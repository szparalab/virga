Contact, Citing
###############

Contact
=======
We welcome your comments and questions! Please contact us at moriah@psu.edu. 

Citing
======
VirGA is current in prep.
    Lance Parsons, Yolanda Tafuri, Jacob Shreve, Christopher Bowen, Mackenzie Shipley, L.W. Enquist, Moriah Szpara (2014). Rapid genome assembly and comparison maps mutations linked to phenotypic change in human herpesviruses. (*In prep, M.Bio*)

