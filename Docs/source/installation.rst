Installation
############

Installation Methods
********************

1. **Linux**

  General installation instructions for linux based systems, using Ubuntu 12.04 as an example.

2. **Virtual Machine (VM)**

  A large file (~4GB) that can be booted with the free software VirtualBox from Windows/Mac/Linux. Download the virtual machine image here: `VirGA_VM_v1-1 <https://www.dropbox.com/sh/szsnelelpbs4qip/AAAohfvHWBGG_HMLL6pJgmTca?dl=0>`_

Linux Installation
******************

This guide assumes the user is installing the VirGA pipeline on a freshly installed Ubuntu 12.04 box with administrative access. For cluster installation without administrative access, please contact your institutions tech support to coordinate the installation of necessary dependencies.

Dependencies
============

Being an all-encompasing pipeline, VirGA has numerous bioinformatics software requirements, and many of these packages have their own requirements. The following shoud be installed in order:

  1. `Java Runtime Environment 1.7.0 <http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html>`_

    a. ``sudo apt-get install default-jre``

  2. `g++ compiler 4.4.7 <ftp://gnu.mirror.iweb.com/gcc/gcc-4.4.7/gcc-4.4.7.tar.gz>`_

    a. ``sudo apt-get install g++``

  3. `perl-doc 5.10.1 <http://search.cpan.org/dist/perl-5.10.1/>`_

    a. ``sudo apt-get install perl-doc``

  4. `biopython 1.64 <http://biopython.org/DIST/biopython-1.64.tar.gz>`_

    a. ``sudo apt-get install python-biopython``

  5. **python-dev**

    a. ``sudo apt-get install python-dev``

  6. **python pip**

    a. ``sudo apt-get install python-pip``

  7. `cython 0.21 <http://cython.org/release/Cython-0.21.2.tar.gz>`_

    a. ``sudo pip install cython``

  8. `paired_sequence_utils <https://bitbucket.org/lance_parsons/paired_sequence_utils>`_

    a. ``sudo pip install paired_sequence_utils``

  9. **bx-python 0.7.1**

    a. Download `bx-python <https://pypi.python.org/packages/source/b/bx-python/bx-python-0.7.1.tar.gz>`_
    b. ``tar -zxvf package_name``
    c. ``sudo python setup.py install``

  10. **pybedtools 0.6**

    a. Download `pybedtools <https://pypi.python.org/packages/source/p/pybedtools/pybedtools-0.6.tar.gz>`_
    b. ``tar -zxvf package_name``
    c. ``sudo python setup.py install``

  11. `bedtools v2.21.0 <https://launchpad.net/ubuntu/+source/bedtools/2.21.0-1>`_

    a. ``sudo apt-get install bedtools``

  12. **Mugsy 2.3**

    a. Download `Mugsy <http://mugsy.sourceforge.net/>`_
    b. Add the installation folder to ``$PATH``

  13. **VAMP 0.9.0**

    a. Download `VAMP <https://bitbucket.org/szparalab/vamp/downloads/VAMP-0.9.0.tar.gz>`_
    b. ``tar -zxvf package_name``
    c. ``sudo python setup.py install``

  14. **Fastx-Toolkit 0.0.13**

    a. Download `Fastx-Toolkit <http://hannonlab.cshl.edu/fastx_toolkit/fastx_toolkit_0.0.13_binaries_Linux_2.6_amd64.tar.bz2>`_
    b. Add the installation folder to ``$PATH``

  15. **FastQC 0.10.1**

    a. Download `FastQC <http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.10.1.zip>`_
    b. ``chmod 755 fastqc``
    c. Add the installation folder to ``$PATH``

  16. **R 3.1.1**

    a. Add the following line to the ``/etc/apt/sources.list`` file:

      * ``deb http://<my.favorite.cran.mirror>/bin/linux/ubuntu trusty/``

    b. ``sudo apt-get update``
    c. ``sudo apt-get install r-base-dev``

  17. **Bowtie2 2.2.2**

    a. Download `Bowtie2 <http://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.2.2/>`_
    b. Add the installation folder to ``$PATH``

  18. **SSAKE 3.8**

    a. Download `SSAKE <http://www.bcgsc.ca/platform/bioinfo/software/ssake/releases/3.8/ssake_v3-8-tar.gz>`_
    b. Add the installation folder to ``$PATH``

  19. **Samtools 1.1**

    a. Download the dependency `zlib library <http://www.zlib.net/>`_

      * ``sudo apt-get install zlibc zlib1g zlib1g-dev``

    b. Download `Samtools <http://sourceforge.net/projects/samtools/files/>`_

      * ``sudo make``
      * Add the installation folder to ``$PATH``
      * Add the subdirectory, bcftools, to ``$PATH`` as well

  20. **Freebayes 0.9.14**

    a. Install the dependency cmake: ``sudo apt-get install cmake``
    b. Download Freebayes: ``git clone --recursive git://github.com/ekg/freebayes.git``

      * ``sudo make``
      * ``sudo make install``

  21. **Celera 8.1**

    a. Download `Celera <http://sourceforge.net/projects/wgs-assembler/files/wgs-assembler/wgs-8.1/>`_
    b. Extract and add the installation folder to ``$PATH``

  22. **Clustalw2 2.1**

    a. Download `clustalw2 <http://www.clustal.org/download/current/>`_
    b. Extract and add the installation folder to ``$PATH``

  23. **GapFiller 1.10**

    a. Obtain a free license and download `GapFiller <http://www.baseclear.com/landingpages/basetools-a-wide-range-of-bioinformatics-solutions/gapfiller/>`_
    b. ``dos2unix gapfiller.pl``
    c. Add the installation folder to ``$PATH``

  24. **VirGA 1.0**

    a. Download `VirGA <https://bitbucket.org/szparalab/virga/downloads/VirGA_v1-0.zip>`_
    b. ``unzip package_name``
    c. Add the VirGA/Pipeline folder to ``$PATH``


Verifying the installation
==========================

VirGA comes with a script that will check for the ability to carry out the specific tasks requested by the user. This means it may not be necessary to have all dependencies installed, such as GapFiller, if the user doesn't plan on using it. To test for proper installation, follow these steps:

  * Execute the following command within the desired working directory, as the VirGA pipeline will create a new file structure repleat with scripts and support files within the current working directory.
  * Initiate the pipeline: ``VirGA_Pipeline_Build.sh``
  * Enter the directory that is created, ``VirGA_Pipeline_Directory``, and edit the ``VirGA_parameters.ini`` file to indicate which steps and substeps are desired, and whether or not to use the PBS/Torque scheduler and the module software system.
  * Execute a script that verifies all relavent dependencies: ``bash x_scripts/check_for_dependencies.sh``
  
Virtual Machine (VM) Installation
*********************************

A barebones installation of VirGA within linux using the Ubuntu 14.04 distribution is available through a VM. The VM itself is a rather large file, approximately 4GB, so please downlaod with a secure and quick connection. Although nearly all of the software dependencies are pre-installed within the VM, licensing issues prevent certain packages from being included. Please follow these instructions for successfully downloading, installing, and running the VM:

  1. Download the software `VirtualBox <https://www.virtualbox.org/>`_, which is free and available for Windows/Mac/Linux.
  2. Download the virtual machine image here: `VirGA_VM_v1-1 <https://www.dropbox.com/sh/szsnelelpbs4qip/AAAohfvHWBGG_HMLL6pJgmTca?dl=0>`_
  3. Open VirtualBox software and load the VirGA-VM_v1-1 image
  4. Configure the options within VirtualBox to designate how many processing cores and how much memory (RAM) to provide to the VM:

    * The more computational resources suppled to the VirGA VM will increase the pipeline's speed but decrease the host computer's speed when the VM is active

  5. Activate the VM
  6. Use the following username and password to login:

    * Username: ``virga-vm``
    * Password: ``HSVSequencing2014``

  7. Change the password for security's sake
  8. Install the dependency **GapFiller**

    a. Obtain a free license and download `GapFiller <http://www.baseclear.com/landingpages/basetools-a-wide-range-of-bioinformatics-solutions/gapfiller/>`_ into the /Download directory
    b. In a terminal window, navigate to the ``/Desktop`` and run the command: ``bash Run_After_Downloading_GapFiller.sh``

  9. Confirm successful VirGA installation

  * In a terminal window, navigate to the ``/Desktop/Dependencies/dependencies_check`` directory
  * Edit the ``VirGA_parameters.ini`` file to indicate which steps and substeps are desired, and whether or not to use the PBS/Torque scheduler and the module software system
  * Execute a script that verifies all relavent dependencies: ``check_for_dependencies.sh``
