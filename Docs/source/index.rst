VirGA - Virus Genome Assembly
#############################

VirGA is a collection of scripts and dependencies that work together to autonomously construct semi *de novo* large DNA virus genomes. It can begin with raw next generation sequencing (NGS) reads and concludes with a fully annotated and evaluated genome assembly. Please thoroughly read VirGA's documentation and contact us with any questions you may have: moriah@psu.edu

Contents:

.. toctree::
    :maxdepth: 2

    introduction
    installation
    usage
    examples
    contact
