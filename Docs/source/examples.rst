Examples
#######

Test case #1: HSV1 assembly using 250,000 paired-end reads
**********************************************************

This scenario uses VirGA's entire pipeline with all steps included. The 250,000 reads are a random sub-set of paired-end reads from a pool of ~20M reads produced when sequencing the straing HSV-F-large. A standard VirGA run would utilize all ~20M reads, but the number of significantly reduced for the purpose of this demonstration. The cells used to culture the virus were from the Green Monkey Kidney line, so the Macaque genome is used to filter contaminating sequence. Since HSV-F-large strain is an HSV1 virus, the common HSV1 reference of HSV1-17 that comes prepackaged with VirGA was used as the reference.

The following steps should be performed in order.

  1. With an empty working directory, execute the command: ``VirGA_Pipeline_Build``
  2. ``cd`` into the newly created directory, ``VirGA_Pipeline_Directory``, and browse its contents: ``ls -la``::

    VirGA_pipeline
    VirGA_parameters.ini
    x_input
    x_reference
    x_contaminants

  3. Copy the reads into the ``x_input`` directory:

    * ``cp /path_to_reads/read_1.fastq x_input``
    * ``cp /path_to_reads/read_2.fastq x_input``

  4. Copy the reference genome and annotation files into the ``x_references`` directory:

    * ``cp /path_to_VirGA/References/HSV-17_trimmed.fa x_references``
    * ``cp /path_to_VirGA/References/HSV-17_trimmed.gff x_references``

  5. Copy the BLAST-indexed contaminants database into the ``x_contaminants`` directory:

    * ``cp /path_to_contaminants/my_BLAST_indexed_database_files* x_contaminants``

Test case #2: Adding wet-bench corrections to assembled genome
**************************************************************

This scenario uses only certain portions of the VirGA pipeline to reannotate the recently assembled genome while included some wet-bench va
