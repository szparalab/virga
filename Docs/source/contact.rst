Contact, Citing
###############

Contact
=======
We welcome your comments and questions! Bioinformatics support is offered through the `Szpara Lab <http://szparalab.psu.edu/>`_, please contact Daniel Renner at dwr19@psu.edu.

Citing
======
VirGA is current in prep.
    Lance Parsons, Yolanda Tafuri, Jacob Shreve, Christopher Bowen, Mackenzie Shipley, L.W. Enquist, Moriah Szpara (2014). Rapid genome assembly and comparison maps mutations linked to phenotypic change in human herpesviruses. (*In prep, M.Bio*)

