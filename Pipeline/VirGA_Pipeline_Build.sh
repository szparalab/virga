#!/usr/bin/env bash

version="v1.0"
description="

 ########################################################################
##########################################################################
#                                                                        #
#                         VirGA_Pipeline_Build.sh                        #
#           By Moriah Szpara, Lance Parsons, and Jacob Shreve            #
#            Szpara lab, The Pennsylvania State University               #
#       https://bitbucket.org/jtshreve/virga-virus-genome-assembly       #
#                                 $version                                   #
#                                                                        #
##########################################################################
 ########################################################################


#  The purpose of this pipeline is to perform the necessary steps prior
#  to a single use of the VirGA pipeline. This script should be run once
#  per sequencing project in order to instantiate file structure, unpack
#  relavant scripts, and guide the user through the process of assembling
#  a viral genome. Execute the script in the desired working directory.

#  Usage:	VirGA_Pipeline_Build.sh

"
### Print the help info if paramenters are passed to the build script
if [ "$#" -ne 0 ]; then
	echo "$description"
	exit 1
fi

### Start building the data for the pipeline, checking for a redundant directory
echo -ne "Unpacking the pipeline...\n"
loc=`pwd`
present=`ls | grep -v "VirGA_Pipeline_Build.sh" | grep -c "VirGA_Pipeline"`
if [ $present == 1 ]; then
	echo "Please run from a directory without an existing VirGA_Pipeline directory"
	exit 1
fi

echo -ne "Creating file structure...\n"
mkdir VirGA_Pipeline_Directory
cd VirGA_Pipeline_Directory
location=`pwd`
mkdir STEP_1--Preprocessing
mkdir STEP_2--Multi_SSAKE_Assembly
mkdir STEP_3--Linearize_and_Annotate
mkdir STEP_4--Assembly_Assessment
mkdir x_input
mkdir x_references
mkdir x_contaminants
mkdir x_scripts
mkdir x_job_files

### Write out the README file
echo -ne "Writing files...\n"
cat>VirGA_README.txt<<EOF
$description
sdf
sdf
sdf
sdf
EOF

### Write out the main pipeline scripts
echo -ne "Writing the pipeline...\n";
cat>VirGA_pipeline<<EOF
#!/usr/bin/env bash
cd $location
. $location/VirGA_parameters.ini

input=\`ls x_input | wc -l\`
if [ \$input -eq 0 ]; then
        echo -ne "\nError - No input files found in the 'x_input' directory\n\n"
        exit 1
fi

names=(\`ls $location/x_input\`)
gff=\`ls $location/x_references | grep ".g"\`
ref=\`ls $location/x_references | grep ".fa"\`
prefix=""
read_loc="$location/x_input"
job_IDs=""
index_IDs=""

if [ \$PBS_use == "yes" ]; then
        script_header="#!/usr/bin/env bash\n#PBS -l walltime=\$PBS_walltime\n#PBS -q \$PBS_queue\n#PBS -l nodes=1:ppn=\$PBS_cores\n"
fi
if [ \$PBS_use != "yes" ]; then
        script_header="#!/usr/bin/env bash\n"
	echo -ne \$script_header > $location/run_manually.sh
	echo -ne "# Execute this script to manually run all pipeline processes in order\n" >> $location/run_manually.sh
	echo -ne "# Alternatively, navigate to the x_job_files directory and run each job individually\n" >> $location/run_manually.sh
	echo -ne "# Beware that running jobs individually requires them to be in the correct order\n" >> $location/run_manually.sh
fi


cd $location/x_job_files


######################################################################################################################################################
#-- Begin UTILITY targeted local reassembly code

if [ \$run_reassembly == "yes" ]; then
	echo -ne "Skipping all VirGA steps, continuing to the targeted local reassembly utility...\n"
	run_step1="no"
	run_step2="no"
	run_step3="no"
	run_step4="no"
	generate_report="no"


	read_R1=\`ls $location/x_input | grep "1.fastq"\`
	read_R2=\`ls $location/x_input | grep "2.fastq"\`
	genome_file=\`ls $location/x_input | grep ".fa$"\`
	sam_file=\`ls $location/x_input | grep ".sam"\`
	mkdir $location/UTILITIES--reassembly

	echo -ne "Beginning UTILITY--reassembly at: `date`\n"
	echo -ne \$script_header > $location/x_job_files/UTILITY-reassembly.sub

	if [ \$use_modules == "yes" ]; then
		if [ \$use_module_wildcard == "yes" ]; then
			echo -ne "\$wildcard_statement\n" >> UTILITY-reassembly.sub
		fi
       		echo -ne "module load \$module_gapfiller\n" >> UTILITY-reassembly.sub
	fi

	echo -ne "echo \"#~# UTILITIES--targeted local reassembly START #~#\"\n" >> UTILITY-reassembly.sub
	sh $location/x_scripts/run_Utility_reassembly.sh $location/UTILITIES--reassembly $location/x_scripts $location/x_input \$sam_file \$read_R1 \$read_R2 \$genome_file \$excise_start \$excise_end \$PBS_cores
	echo -ne "echo \"#~# UTILITIES--targeted local reassembly STOP #~#\"\n" >> UTILITY-reassembly.sub

	if [ \$PBS_use == "yes" ]; then
        	job_IDs="\$(qsub UTILITY-reassembly.sub | grep -Po "^\d+")"
	fi
	if [ \$PBS_use != "yes" ]; then
        	echo -ne "bash x_job_files/UTILITY-reassembly.sub 2>&1 | tee -a run_manually.log\n" >> $location/run_manually.sh
	fi
fi

#######################################################################################################################################################
#-- Begin STEP_1 code
if [ \$run_step1 == "yes" ]; then
	echo -ne "Beginning STEP_1 at: `date`\n"
	echo -ne \$script_header > $location/x_job_files/STEP_1.sub

read_R1=\`ls $location/x_input | grep "1.fastq"\`
read_R2=\`ls $location/x_input | grep "2.fastq"\`

if [ \$before_stats == "yes" ]; then
        mkdir $location/STEP_1--Preprocessing/before_metrics
	if [ \$use_modules == "yes" ]; then
		if [ \$use_module_wildcard == "yes" ]; then
			echo -ne "\$wildcard_statement\n" >> STEP_1.sub
		fi
		echo -ne "module load \$module_fastx\n" >> STEP_1.sub
		echo -ne "module load \$module_fastqc\n" >> STEP_1.sub
	fi
	echo -ne "echo \"#~# before_stats #~#\"\n" >> STEP_1.sub
        sh $location/x_scripts/run_Q30-Histogram_before.sh \$read_R1 $location/STEP_1--Preprocessing/before_metrics $location/x_input/\$read_R1
        sh $location/x_scripts/run_Fastqc_before.sh \$read_R1 $location/STEP_1--Preprocessing/before_metrics $location/x_input/\$read_R1
	if [ \$input_type == "PE" ]; then
		sh $location/x_scripts/run_Q30-Histogram_before.sh \$read_R2 $location/STEP_1--Preprocessing/before_metrics $location/x_input/\$read_R2
        	sh $location/x_scripts/run_Fastqc_before.sh \$read_R2 $location/STEP_1--Preprocessing/before_metrics $location/x_input/\$read_R2
	fi
	echo -ne "echo \"#~# end before_stats #~#\"\n" >> STEP_1.sub
fi

if [ \$clip_adapters == "yes" ]; then
        prefix="clip_\$prefix"
        mkdir $location/STEP_1--Preprocessing/clipped_reads
	if [ \$use_modules == "yes" ]; then
        	if [ \$use_module_wildcard == "yes" ]; then
                	echo -ne "\$wildcard_statement\n" >> STEP_1.sub
        	fi
                echo -ne "module load \$module_fastx\n" >> STEP_1.sub
        fi
	echo -ne "echo \"#~# clip_adapters #~#\"\n" >> STEP_1.sub
        sh $location/x_scripts/run_Clipping.sh \$read_R1 $location/x_input/\$read_R1 $location/STEP_1--Preprocessing/clipped_reads/\${prefix}\$read_R1  \$adapter \$min_clip_keep_length
	if [ \$input_type == "PE" ]; then
		sh $location/x_scripts/run_Clipping.sh \$read_R2 $location/x_input/\$read_R2 $location/STEP_1--Preprocessing/clipped_reads/\${prefix}\$read_R2  \$adapter \$min_clip_keep_length
	fi
	echo -ne "echo \"#~# end clip_adapters #~#\"\n" >> STEP_1.sub
        read_loc="$location/STEP_1--Preprocessing/clipped_reads"
fi


if [ \$trim_reads == "yes" ]; then
        mkdir $location/STEP_1--Preprocessing/trimmed_reads
        if [ \$use_modules == "yes" ]; then
        	if [ \$use_module_wildcard == "yes" ]; then
                	echo -ne "\$wildcard_statement\n" >> STEP_1.sub
                fi
                echo -ne "module load \$module_fastx\n" >> STEP_1.sub
        fi
	echo -ne "echo \"#~# trim_reads #~#\" 1>&2\n" >> STEP_1.sub
	sh $location/x_scripts/run_Trimming.sh $location/x_scripts \$read_loc/\${prefix}\$read_R1 $location/STEP_1--Preprocessing/trimmed_reads/trim_\${prefix}\$read_R1 \$min_qual \$min_trim_keep_length \$window_size
	if [ \$input_type == "PE" ]; then
		sh $location/x_scripts/run_Trimming.sh $location/x_scripts \$read_loc/\${prefix}\$read_R2 $location/STEP_1--Preprocessing/trimmed_reads/trim_\${prefix}\$read_R2 \$min_qual \$min_trim_keep_length \$window_size
	fi
	echo -ne "echo \"#~# end trim_reads #~#\" 1>&2\n" >> STEP_1.sub
	prefix="trim_\$prefix"
        read_loc="$location/STEP_1--Preprocessing/trimmed_reads"
fi

if [ \$filter_artifacts == "yes" ]; then
        mkdir $location/STEP_1--Preprocessing/artifact_filtered_reads
	if [ \$use_modules == "yes" ]; then
        	if [ \$use_module_wildcard == "yes" ]; then
                	echo -ne "\$wildcard_statement\n" >> STEP_1.sub
                fi
               	echo -ne "module load \$module_fastx\n" >> STEP_1.sub
        fi
	echo -ne "echo \"#~# filter_artifacts #~#\"\n" >> STEP_1.sub
	sh $location/x_scripts/run_Artifact.sh \$read_R1 \$read_loc/\${prefix}\$read_R1 $location/STEP_1--Preprocessing/artifact_filtered_reads/artifact_\${prefix}\$read_R1
	if [ \$input_type == "PE" ]; then
		sh $location/x_scripts/run_Artifact.sh \$read_R2 \$read_loc/\${prefix}\$read_R2 $location/STEP_1--Preprocessing/artifact_filtered_reads/artifact_\${prefix}\$read_R2
	fi
	echo -ne "echo \"#~# end filter_artifacts #~#\"\n" >> STEP_1.sub
	prefix="artifact_\$prefix"
        read_loc="$location/STEP_1--Preprocessing/artifact_filtered_reads"
fi

if [ \$filter_contaminants == "yes" ]; then
        mkdir $location/STEP_1--Preprocessing/contaminant_filtered_reads
        if [ \$(ls $location/x_contaminants | grep -c ".bt2") == "6" ]; then
                ref_base=\$(ls $location/x_contaminants | grep ".rev.1.bt2" | sed -r 's/.rev.1.bt2//')
        fi
        if [ \$(ls $location/x_contaminants | grep -c ".bt2") != "6" ]; then
                if [ \$(ls $location/x_contaminants | wc -l) -gt 1 ]; then
                        cat $location/x_contaminants/* > $location/x_contaminants/combined_ref.fa
                        ref_base="combined_ref"
			if [ \$use_modules == "yes" ]; then
                        	if [ \$use_module_wildcard == "yes" ]; then
                                	echo -ne "\$wildcard_statement\n" >> STEP_1.sub
                        	fi
                        	echo -ne "module load \$module_bowtie2\n" >> STEP_1.sub
                	fi
                        sh $location/x_scripts/run_Index.sh $location/x_contaminants/combined_ref.fa $location/x_contaminants/\$ref_base
                fi
                if [ \$(ls $location/x_contaminants | wc -l) == "1" ]; then
                        ref_base=\$(ls $location/x_contaminants)
			if [ \$use_modules == "yes" ]; then
                                if [ \$use_module_wildcard == "yes" ]; then
                                        echo -ne "\$wildcard_statement\n" >> STEP_1.sub
                                fi
                                echo -ne "module load \$module_bowtie2\n" >> STEP_1.sub
                        fi
                        sh $location/x_scripts/run_Index.sh $location/x_contaminants/\$ref_base $location/x_contaminants/\$ref_base
                fi
        fi
	if [ \$use_modules == "yes" ]; then
               	if [ \$use_module_wildcard == "yes" ]; then
                        echo -ne "\$wildcard_statement\n" >> STEP_1.sub
                fi
                echo -ne "module load \$module_bowtie2\n" >> STEP_1.sub
        fi
	echo -ne "echo \"#~# filter_contaminants #~#\" 1>&2\n" >> STEP_1.sub
        sh $location/x_scripts/run_Mapping.sh \$read_R1 $location/x_contaminants/\$ref_base \$read_loc/\${prefix}\$read_R1 $location/STEP_1--Preprocessing/contaminant_filtered_reads/decon_\${prefix}\${read_R1}.sam \$PBS_cores
	echo -ne "perl $location/x_scripts/filter_reads_using_sam.pl $location/STEP_1--Preprocessing/contaminant_filtered_reads/decon_\${prefix}\${read_R1}.sam \$read_loc/\${prefix}\$read_R1 > $location/STEP_1--Preprocessing/contaminant_filtered_reads/decon_\${prefix}\$read_R1\n" >> $location/x_job_files/STEP_1.sub
	if [ \$input_type == "PE" ]; then
		sh $location/x_scripts/run_Mapping.sh \$read_R2 $location/x_contaminants/\$ref_base \$read_loc/\${prefix}\$read_R2 $location/STEP_1--Preprocessing/contaminant_filtered_reads/decon_\${prefix}\${read_R2}.sam \$PBS_cores
		echo -ne "perl $location/x_scripts/filter_reads_using_sam.pl $location/STEP_1--Preprocessing/contaminant_filtered_reads/decon_\${prefix}\${read_R2}.sam \$read_loc/\${prefix}\$read_R2 > $location/STEP_1--Preprocessing/contaminant_filtered_reads/decon_\${prefix}\$read_R2\n" >> $location/x_job_files/STEP_1.sub
	fi
	echo -ne "echo \"#~# end filter_contaminants #~#\" 1>&2\n" >> STEP_1.sub
        prefix="decon_\$prefix"
        read_loc="$location/STEP_1--Preprocessing/contaminant_filtered_reads"
fi

if [ \$only_save_paired == "yes" ] && [ \$input_type == "PE" ]; then
	mkdir $location/STEP_1--Preprocessing/properly_paired_reads
	mkdir $location/STEP_1--Preprocessing/properly_paired_reads/Singletons
	echo -ne "echo \"#~# only_save_paired #~#\" 1>&2\n" >> STEP_1.sub
	echo -ne "perl $location/x_scripts/save_paired_reads.pl \$read_loc/\${prefix}\$read_R1 \$read_loc/\${prefix}\$read_R2 $location/STEP_1--Preprocessing/properly_paired_reads/\n" >> $location/x_job_files/STEP_1.sub
	echo -ne "mv \$read_loc/paired_\${prefix}\$read_R1 $location/STEP_1--Preprocessing/properly_paired_reads\n" >> $location/x_job_files/STEP_1.sub
	echo -ne "mv \$read_loc/paired_\${prefix}\$read_R2 $location/STEP_1--Preprocessing/properly_paired_reads\n" >> $location/x_job_files/STEP_1.sub
	echo -ne "mv $location/STEP_1--Preprocessing/properly_paired_reads/Singletons.fastq $location/STEP_1--Preprocessing/properly_paired_reads/Singletons\n" >> $location/x_job_files/STEP_1.sub
	echo -ne "echo \"#~# end only_save_paired #~#\" 1>&2\n" >> STEP_1.sub
        prefix="paired_\$prefix"
        read_loc="$location/STEP_1--Preprocessing/properly_paired_reads"
fi

if [ \$after_stats == "yes" ]; then
        mkdir $location/STEP_1--Preprocessing/after_metrics
	if [ \$use_modules == "yes" ]; then
        	if [ \$use_module_wildcard == "yes" ]; then
                        echo -ne "\$wildcard_statement\n" >> STEP_1.sub
                fi
                echo -ne "module load \$module_fastx\n" >> STEP_1.sub
                echo -ne "module load \$module_fastqc\n" >> STEP_1.sub
	fi
	echo -ne "echo \"#~# after_stats #~#\"\n" >> STEP_1.sub
        sh $location/x_scripts/run_Q30-Histogram_after.sh \$read_R1 $location/STEP_1--Preprocessing/after_metrics \$read_loc/\${prefix}\$read_R1
        sh $location/x_scripts/run_Fastqc_after.sh \$read_R1 $location/STEP_1--Preprocessing/after_metrics \$read_loc/\${prefix}\$read_R1
	if [ \$input_type == "PE" ]; then
		sh $location/x_scripts/run_Q30-Histogram_after.sh \$read_R2 $location/STEP_1--Preprocessing/after_metrics \$read_loc/\${prefix}\$read_R2
        	sh $location/x_scripts/run_Fastqc_after.sh \$read_R2 $location/STEP_1--Preprocessing/after_metrics \$read_loc/\${prefix}\$read_R2
	fi
	echo -ne "echo \"#~# after_stats #~#\"\n" >> STEP_1.sub
fi

	if [ \$PBS_use == "yes" ]; then
        	job_IDs="\$(qsub STEP_1.sub | grep -Po "^\d+")"
        fi
        if [ \$PBS_use != "yes" ]; then
                echo -ne "bash x_job_files/STEP_1.sub 2>&1 | tee -a run_manually.log\n" >> $location/run_manually.sh
        fi


fi
#######################################################################################################################################################
#-- Begin STEP_2 code
if [ \$run_step2 == "yes" ]; then
	echo -ne "Beginning STEP_2 at: `date`\n"
	echo -ne \$script_header > $location/x_job_files/STEP_2a.sub

	if [ \$run_step1 == "yes" ]; then
		read_loc_for_assembly=\$read_loc
	fi
	if [ \$run_step1 != "yes" ]; then
		read_loc_for_assembly=$location/x_input
		singletons_count=\`ls $location/x_input| grep -c Singletons.fastq\`
		singletons_present=no
		if [ \$singletons_count == 1 ]; then
        		singletons_present=yes
		fi
	fi

	block_kmers="#"
	if [ \$find_SSAKE_kmers == "yes" ]; then
		ssake_parameters=all-0,all-2,all-4
		run_step3=no
		run_step4=no
		use_celera=no
		block_kmers=""
	fi

	if [ \$use_multi_ssake == "yes" ]; then
		mkdir $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE
		echo -ne "cp \$read_loc_for_assembly/*.fastq $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE\n" >> STEP_2a.sub
		echo -ne "ssake_read1=\\\`ls $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE | grep \"1.fastq\"\\\`\n" >> STEP_2a.sub
                echo -ne "ssake_read2=\\\`ls $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE | grep \"2.fastq\"\\\`\n" >> STEP_2a.sub
                echo -ne "perl $location/x_scripts/format_PE_fastq_for_SSAKE.pl $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/\\\$ssake_read1 $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/\\\$ssake_read2 500 > $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/ssake.fasta\n" >> STEP_2a.sub
		if [ \$run_step1 == "yes" ] && [ \$only_save_paired == "yes" ]; then
			echo -ne "cp $location/STEP_1--Preprocessing/properly_paired_reads/Singletons/Singletons.fastq $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE\n" >> STEP_2a.sub
			echo -ne "perl $location/x_scripts/format_Singleton_fastq_for_SSAKE.pl $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/Singletons.fastq > $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/Singletons.fasta\n" >> STEP_2a.sub
		fi
		if [ \$run_step1 != "yes" ] && [ \$singletons_present == "yes" ]; then
			echo -ne "cp $location/x_input/Singletons.fastq $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE\n" >> STEP_2a.sub
                        echo -ne "perl $location/x_scripts/format_Singleton_fastq_for_SSAKE.pl $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/Singletons.fastq > $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/Singletons.fasta\n" >> STEP_2a.sub
		fi

		if [ \$PBS_use == "yes" ]; then
                	new_job_IDs="\$(qsub -W depend=afterok:\$job_IDs STEP_2a.sub | grep -Po "^\d+")"
                	job_IDs=\$new_job_IDs
        	fi
        	if [ \$PBS_use != "yes" ]; then
                	echo -ne "bash x_job_files/STEP_2a.sub 2>&1 | tee -a run_manually.log\n" >> $location/run_manually.sh
        	fi

		IFS=',' read -a pairs <<< "\$ssake_parameters"
		new_job_IDs=""
		for index in "\${!pairs[@]}"
		do
			IFS='-' read -a params <<< "\${pairs[\$index]}"
			if [ \${params[0]} == "all" ]; then
				for i in {16..100}
				do
					if [ \$PBS_use == "yes" ]; then
						echo -ne "#!/usr/bin/env bash\n#PBS -l walltime=\$PBS_walltime\n#PBS -q \$PBS_queue\n#PBS -l nodes=1\n#PBS -l mem=\$ssake_memory\n" > $location/x_job_files/STEP_2b--\${i}-\${params[1]}.sub
					fi
					if [ \$PBS_use != "yes" ]; then
						echo -ne "#!/usr/bin/env bash\n" > $location/x_job_files/STEP_2b--\${i}-\${params[1]}.sub
					fi
	                                if [ \$use_modules == "yes" ]; then
	                                        if [ \$use_module_wildcard == "yes" ]; then
	                                                echo -ne "\$wildcard_statement\n" >> STEP_2b--\${i}-\${params[1]}.sub
	                                        fi
	                                        echo -ne "module load \$module_virga\n" >> STEP_2b--\${i}-\${params[1]}.sub
						echo -ne "module load \$module_ssake\n" >> STEP_2b--\${i}-\${params[1]}.sub
	                                fi
	                                echo -ne "mkdir $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/\${i}-\${params[1]}\n" >> STEP_2b--\${i}-\${params[1]}.sub
	                                echo -ne "cd $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/\${i}-\${params[1]}\n" >> STEP_2b--\${i}-\${params[1]}.sub
	                                echo -ne "ln -s $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/ssake.fasta ssake.fasta\n" >> STEP_2b--\${i}-\${params[1]}.sub
	                                echo -ne "Here we have params: \${pairs[\$index]}, at the index: \$index, \${i}-\${params[1]}\n"
	                                if [ \$run_step1 == "yes" ] && [ \$only_save_paired == "yes" ]; then
	                                        echo -ne "ln -s $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/Singletons.fasta Singletons.fasta\n" >> STEP_2b--\${i}-\${params[1]}.sub
	                                        echo -ne "SSAKE -f ssake.fasta -g Singletons.fasta -p -1 -w 10 -m \${i} -t \${params[1]}\n" >> STEP_2b--\${i}-\${params[1]}.sub
	                                fi
	                                if [ \$run_step1 == "yes" ] && [ \$only_save_paired != "yes" ]; then
	                                        echo -ne "SSAKE -f ssake.fasta -p -1 -w 10 -m \${i} -t \${params[1]}\n" >> STEP_2b--\${i}-\${params[1]}.sub
	                                fi
					if [ \$run_step1 != "yes" ] && [ \$singletons_present != "yes" ]; then
                                                echo -ne "SSAKE -f ssake.fasta -p -1 -w 10 -m \${i} -t \${params[1]}\n" >> STEP_2b--\${i}-\${params[1]}.sub
                                        fi
					if [ \$run_step1 != "yes" ] && [ \$singletons_present == "yes" ]; then
                                        	echo -ne "ln -s $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/Singletons.fasta Singletons.fasta\n" >> STEP_2b--\${i}-\${params[1]}.sub
                                        	echo -ne "SSAKE -f ssake.fasta -g Singletons.fasta -p -1 -w 10 -m \${i} -t \${params[1]}\n" >> STEP_2b--\${i}-\${params[1]}.sub
					fi
	                                if [ \$PBS_use == "yes" ]; then
	                                        new_job_IDs="\${new_job_IDs}\$(qsub -W depend=afterok:\$job_IDs STEP_2b--\${i}-\${params[1]}.sub | grep -Po "^\d+"):"
	                                fi
	                                if [ \$PBS_use != "yes" ]; then
	                                        echo -ne "bash x_job_files/STEP_2b--\${i}-\${params[1]}.sub 2>&1 | tee -a run_manually.log\n" >> $location/run_manually.sh
	                                fi
				done
			fi
			if [ \${params[0]} != "all" ]; then
				if [ \$PBS_use == "yes" ]; then
                                	echo -ne "#!/usr/bin/env bash\n#PBS -l walltime=\$PBS_walltime\n#PBS -q \$PBS_queue\n#PBS -l nodes=1\n#PBS -l mem=\$ssake_memory\n" > $location/x_job_files/STEP_2b--\${pairs[\$index]}.sub
				fi
                                if [ \$PBS_use != "yes" ]; then
                                	echo -ne "#!/usr/bin/env bash\n" > $location/x_job_files/STEP_2b--\${pairs[\$index]}.sub
                                fi	
				if [ \$use_modules == "yes" ]; then
	                        	if [ \$use_module_wildcard == "yes" ]; then
	                                	echo -ne "\$wildcard_statement\n" >> STEP_2b--\${pairs[\$index]}.sub
	                        	fi
	                        	echo -ne "module load \$module_virga\n" >> STEP_2b--\${pairs[\$index]}.sub
					echo -ne "module load \$module_ssake\n" >> STEP_2b--\${pairs[\$index]}.sub
	                	fi
				echo -ne "mkdir $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/\${pairs[\$index]}\n" >> STEP_2b--\${pairs[\$index]}.sub
				echo -ne "cd $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/\${pairs[\$index]}\n" >> STEP_2b--\${pairs[\$index]}.sub
				echo -ne "ln -s $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/ssake.fasta ssake.fasta\n" >> STEP_2b--\${pairs[\$index]}.sub
				echo -ne "Here we have params: \${pairs[\$index]}, at the index: \$index\n"
				if [ \$run_step1 == "yes" ] && [ \$only_save_paired == "yes" ]; then
					echo -ne "ln -s $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/Singletons.fasta Singletons.fasta\n" >> STEP_2b--\${pairs[\$index]}.sub
					echo -ne "SSAKE -f ssake.fasta -g Singletons.fasta -p -1 -w 10 -m \${params[0]} -t \${params[1]}\n" >> STEP_2b--\${pairs[\$index]}.sub
				fi
				if [ \$run_step1 == "yes" ] && [ \$only_save_paired != "yes" ]; then
					echo -ne "SSAKE -f ssake.fasta -p -1 -w 10 -m \${params[0]} -t \${params[1]}\n" >> STEP_2b--\${pairs[\$index]}.sub
				fi
				if [ \$run_step1 != "yes" ] && [ \$singletons_present != "yes" ]; then
                                        echo -ne "SSAKE -f ssake.fasta -p -1 -w 10 -m \${params[0]} -t \${params[1]}\n" >> STEP_2b--\${pairs[\$index]}.sub
                                fi
				if [ \$run_step1 != "yes" ] && [ \$singletons_present == "yes" ]; then
                                        echo -ne "ln -s $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/Singletons.fasta Singletons.fasta\n" >> STEP_2b--\${pairs[\$index]}.sub
					echo -ne "SSAKE -f ssake.fasta -g Singletons.fasta -p -1 -w 10 -m \${params[0]} -t \${params[1]}\n" >> STEP_2b--\${pairs[\$index]}.sub
                                fi
				if [ \$PBS_use == "yes" ]; then
	                		new_job_IDs="\${new_job_IDs}\$(qsub -W depend=afterok:\$job_IDs STEP_2b--\${pairs[\$index]}.sub | grep -Po "^\d+"):"
	        		fi
	        		if [ \$PBS_use != "yes" ]; then
	                		echo -ne "bash x_job_files/STEP_2b--\${pairs[\$index]}.sub 2>&1 | tee -a run_manually.log\n" >> $location/run_manually.sh
	        		fi
			fi
		done
	fi

	echo -ne \$script_header > $location/x_job_files/STEP_2c.sub
	if [ \$use_multi_ssake == "yes" ]; then
		echo -ne "cat $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE/*/*.mergedcontigs > $location/STEP_2--Multi_SSAKE_Assembly/rough_contigs.fa\n" >> STEP_2c.sub
		echo -ne "\$block_kmers" >> STEP_2c.sub
		echo -ne "cd $location/STEP_2--Multi_SSAKE_Assembly/Multi_SSAKE\n" >> STEP_2c.sub
		echo -ne "\$block_kmers" >> STEP_2c.sub
		echo -ne "bash $location/x_scripts/select_ssake_kmers.sh\n" >> STEP_2c.sub
	fi

	if [ \$use_celera == "yes" ] && [ \$use_multi_ssake != "yes" ]; then
		found_ssake_output=\`ls $location/x_input | grep -v ".fastq" | grep -c ".fa"\`
		if [ \$found_ssake_output -ne 1 ]; then
			echo -ne "ERROR: Can't use Celera without the Multi_SSAKE assembly step\n"
			exit 1
		fi
		echo -ne "Using the genome file \$found_ssake_output for assembly with celera...\n"
		found_ssake_output=\`ls $location/x_input | grep -v ".fastq" | grep ".fa"\`
		cp $location/x_input/\$found_ssake_output $location/STEP_2--Multi_SSAKE_Assembly/rough_contigs.fa
		use_multi_ssake="yes"
	fi

	use_gap4="no"
	if [ \$use_gap4 == "yes" ] && [ \$use_multi_ssake != "yes" ]; then
                echo -ne "ERROR: Can't use GAP4 without the Multi_SSAKE assembly step\n"
                exit 1
        fi

	if [ \$use_celera == "yes" ] && [ \$use_multi_ssake == "yes" ]; then
		mkdir $location/STEP_2--Multi_SSAKE_Assembly/Celera
		if [ \$use_modules == "yes" ]; then
                        if [ \$use_module_wildcard == "yes" ]; then
                                echo -ne "\$wildcard_statement\n" >> STEP_2c.sub
                        fi
                        echo -ne "module load \$module_celera\n" >> STEP_2c.sub
                fi
		sh $location/x_scripts/run_Celera.sh $location/STEP_2--Multi_SSAKE_Assembly/Celera $location/x_scripts $location/STEP_2--Multi_SSAKE_Assembly \$min_base_score \$max_base_score \$percent_contig_ends
	fi

	if [ \$use_gap4 == "yes" ] && [ \$use_multi_ssake == "yes" ]; then
		mkdir $location/STEP_2--Multi_SSAKE_Assembly/GAP4
		if [ \$use_modules == "yes" ]; then
                        if [ \$use_module_wildcard == "yes" ]; then
                                echo -ne "\$wildcard_statement\n" >> STEP_2c.sub
                        fi
                        echo -ne "module load \$module_staden\n" >>STEP_2c.sub
                fi
		sh $location/x_scripts/run_Gap4.sh $location/STEP_2--Multi_SSAKE_Assembly/GAP4 $location/x_scripts $location/STEP_2--Multi_SSAKE_Assembly
	fi

	if [ \$PBS_use == "yes" ]; then
		job_IDs="\$(qsub -W depend=afterok:\$new_job_IDs STEP_2c.sub | grep -Po "^\d+")"
	fi
	if [ \$PBS_use != "yes" ]; then
		echo -ne "bash x_job_files/STEP_2c.sub 2>&1 | tee -a run_manually.log\n" >> $location/run_manually.sh
	fi

fi
#######################################################################################################################################################
#-- Begin STEP_3 code
if [ \$run_step3 == "yes" ]; then
	echo -ne "Beginning STEP_3 at: `date`\n"
	echo -ne \$script_header > $location/x_job_files/STEP_3.sub

	if [ \$run_step2 == "yes" ]; then
		prepped_contigs="clean_contigs.fa"
		contig_loc="$location/STEP_2--Multi_SSAKE_Assembly"
	fi

	if [ \$run_step2 != "yes" ]; then
                contig_presence=\`ls $location/x_input | grep -v ".fastq" | grep -c ".fa"\`
                if [ \$contig_presence -ne 1 ]; then
                        echo -ne "ERROR, no contig file ending in .fa found in x_input directory. Quitting...\n"
                        exit 1
                fi
                prepped_contigs=\`ls $location/x_input | grep -v ".fastq" | grep ".fa"\`
		contig_loc="$location/x_input"
        fi

	if [ \$use_mugsy_maf_net == "yes" ]; then
		mkdir $location/STEP_3--Linearize_and_Annotate/MUGSY_and_Maf-Net
		genome_ref_count=\`ls $location/x_references | grep -c ".fa"\`
		if [ \$genome_ref_count -ne 1 ]; then
			echo -ne "ERROR: exactly 1 genome reference file ending in .fa not found in x_references directory. Quitting...\n"
			exit 1
		fi
		genome_ref=\`ls $location/x_references | grep ".fa"\`
		if [ \$use_modules == "yes" ]; then
                        if [ \$use_module_wildcard == "yes" ]; then
                                echo -ne "\$wildcard_statement\n" >> STEP_3.sub
                        fi
                        echo -ne "module load \$module_virga\n" >> STEP_3.sub
			echo -ne "module load \$module_mugsy\n" >> STEP_3.sub
                fi
		sh $location/x_scripts/run_MUGSY_Maf-Net.sh $location/STEP_3--Linearize_and_Annotate/MUGSY_and_Maf-Net $location/x_references \$genome_ref \$contig_loc \$prepped_contigs $location/x_scripts \$use_compare_genomes
	fi

	if [ \$use_compare_genomes == "yes" ] && [ \$use_mugsy_maf_net != "yes" ]; then
		echo -ne "ERROR: Cannot run compare_genomes.py without first runing MUGSY and Maf-Net. Quitting...\n"
		exit 1
	fi

	if [ \$use_compare_genomes == "yes" ]; then
		mkdir $location/STEP_3--Linearize_and_Annotate/Compare_genomes
		genome_ref_annotation_count=\`ls $location/x_references | grep -cP ".g[t|f]f"\`
		if [ \$genome_ref_annotation_count -ne 1 ]; then
			echo -ne "ERROR: exactly one .gff or .gtf annotation file not found in x_references directory. Quitting...\n"
			exit 1
		fi
		genome_ref_annotation=\`ls $location/x_references | grep -P ".g[t|f]f"\`
		if [ \$use_modules == "yes" ]; then
                        if [ \$use_module_wildcard == "yes" ]; then
                                echo -ne "\$wildcard_statement\n" >> STEP_3.sub
                        fi
                        echo -ne "module load \$module_virga\n" >> STEP_3.sub
                fi
		sh $location/x_scripts/run_Compare_Genomes.sh $location/STEP_3--Linearize_and_Annotate/Compare_genomes $location/STEP_3--Linearize_and_Annotate/MUGSY_and_Maf-Net $location/x_references/\${genome_ref_annotation} \$prepped_contigs
	fi
	
	if [ \$use_gapfiller == "yes" ]; then
		mkdir $location/STEP_3--Linearize_and_Annotate/GapFiller
                if [ \$use_modules == "yes" ]; then
                        if [ \$use_module_wildcard == "yes" ]; then
                                echo -ne "\$wildcard_statement\n" >> STEP_3.sub
                        fi
                        echo -ne "module load \$module_gapfiller\n" >> STEP_3.sub
                fi
		echo -ne "echo \"#~# use_gapfiller #~#\"\n" >> STEP_3.sub
                sh $location/x_scripts/run_GapFiller.sh $location/STEP_3--Linearize_and_Annotate/GapFiller $location/STEP_3--Linearize_and_Annotate/ assembled_genome.fa assmebled_genome.gff \$run_step1 \$read_loc $location/x_input $location/x_scripts \$insert_size \$insert_deviation \$PBS_cores
		echo -ne "echo \"#~# end use_gapfiller #~#\"\n" >> STEP_3.sub
	fi


	if [ \$recreate_annotation == "yes" ]; then
                if [ \$use_modules == "yes" ]; then
                        if [ \$use_module_wildcard == "yes" ]; then
                                echo -ne "\$wildcard_statement\n" >> STEP_3.sub
                        fi
                        echo -ne "module load \$module_virga\n" >> STEP_3.sub
                        echo -ne "module load \$module_mugsy\n" >> STEP_3.sub
                fi
                sh $location/x_scripts/run_Recreate_Annotation.sh $location/STEP_3--Linearize_and_Annotate/GapFiller $location/x_scripts $location/STEP_3--Linearize_and_Annotate $location/x_references \$genome_ref $location/x_references/\${genome_ref_annotation}
        fi

	if [ \$PBS_use == "yes" ]; then
                new_job_IDs="\$(qsub -W depend=afterok:\$job_IDs STEP_3.sub | grep -Po "^\d+")"
                job_IDs=\$new_job_IDs
        fi
        if [ \$PBS_use != "yes" ]; then
                echo -ne "bash x_job_files/STEP_3.sub 2>&1 | tee -a run_manually.log\n" >> $location/run_manually.sh
        fi

fi
#######################################################################################################################################################
#-- Begin STEP_4 code
if [ \$run_step4 == "yes" ] && [ \$bowtie2_map == "yes" ]; then
	echo -ne "Beginning STEP_4 at: `date`\n"
	echo -ne \$script_header > $location/x_job_files/STEP_4.sub

	mkdir $location/STEP_4--Assembly_Assessment/Bowtie2

	#~#~#~#~# Move the genome .fa and .gff file into the Bowtie2 directory to begin step 4

	if [ \$run_step3 == "yes" ]; then
		echo "
		cp $location/STEP_3--Linearize_and_Annotate/assembled_genome.fa $location/STEP_4--Assembly_Assessment/Bowtie2
		cp $location/STEP_3--Linearize_and_Annotate/assembled_genome.gff $location/STEP_4--Assembly_Assessment/Bowtie2
		" >> STEP_4.sub
	fi

	if [ \$run_step3 != "yes" ]; then
		echo "
		find_genome=\\\`ls $location/x_input | grep -v \".fastq\" | grep -c \".fa\"\\\`
		if [ \\\$find_genome -ne 1 ]; then
			echo -ne \"ERROR: Single .fa genome file not found in the x_input directory. Quitting...\n\"
			exit 1
		fi
		find_genome=\\\`ls $location/x_input | grep -v \".fastq\" | grep \".fa\"\\\`
		cp $location/x_input/\\\$find_genome $location/STEP_4--Assembly_Assessment/Bowtie2/assembled_genome.fa

		find_genome_gff=\\\`ls $location/x_input | grep -cP \".g(t|f)f\"\\\`
                if [ \\\$find_genome_gff -ne 1 ]; then
                        echo -ne \"ERROR: Single .gff genome annotation file not found in the x_input directory. Quitting...\n\"
                        exit 1
                fi
                find_genome_gff=\\\`ls $location/x_input | grep -P \".g(t|f)f\"\\\`
                cp $location/x_input/\\\$find_genome_gff $location/STEP_4--Assembly_Assessment/Bowtie2/assembled_genome.gff
		" >> STEP_4.sub
	fi

	if [ \$run_step1 != "yes" ]; then
		read_loc="$location/x_input"
	fi

	#~#~#~#~# Rename the genome .fa and .gff files to use the desired final genome name as specified in parameters

	echo "
	echo -ne \">\$final_genome_name\n\" > $location/STEP_4--Assembly_Assessment/Bowtie2/\$final_genome_name.fa
	grep -v \">\" $location/STEP_4--Assembly_Assessment/Bowtie2/assembled_genome.fa >> $location/STEP_4--Assembly_Assessment/Bowtie2/\$final_genome_name.fa
	rm $location/STEP_4--Assembly_Assessment/Bowtie2/assembled_genome.fa
	mv $location/STEP_4--Assembly_Assessment/Bowtie2/assembled_genome.gff $location/STEP_4--Assembly_Assessment/Bowtie2/\$final_genome_name.gff
	old_gff_name=\\\`tail -n 1 $location/STEP_4--Assembly_Assessment/Bowtie2/\${final_genome_name}.gff | cut -f 1\\\`
	sed -ir 's/^'\\\$old_gff_name'/\$final_genome_name/g' $location/STEP_4--Assembly_Assessment/Bowtie2/\$final_genome_name.gff
	rm $location/STEP_4--Assembly_Assessment/Bowtie2/\$final_genome_name.gffr
	" >> STEP_4.sub

	#~#~#~#~# Begin the mapping process

	if [ \$bowtie2_map == "yes" ]; then
		if [ \$use_modules == "yes" ]; then
                        if [ \$use_module_wildcard == "yes" ]; then
                                echo -ne "\$wildcard_statement\n" >> STEP_4.sub
                        fi
                        echo -ne "module load \$module_bowtie2\n" >> STEP_4.sub
			echo -ne "module load \$module_samtools\n" >> STEP_4.sub
                fi
                sh $location/x_scripts/run_Index_Bowtie2.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ $location/STEP_4--Assembly_Assessment/Bowtie2/\${final_genome_name}.fa \$final_genome_name
		echo -ne "echo \"#~# first_mapping #~#\" 1>&2\n" >> STEP_4.sub
		sh $location/x_scripts/run_Bowtie2_mapping_genome.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ \$final_genome_name \$read_loc \$input_type \$PBS_cores
		echo -ne "echo \"#~# end first_mapping #~#\" 1>&2\n" >> STEP_4.sub

		if [ \$create_pileup == "yes" ]; then
			if [ \$use_modules == "yes" ]; then
                        	if [ \$use_module_wildcard == "yes" ]; then
                                	echo -ne "\$wildcard_statement\n" >> STEP_4.sub
                        	fi
                        	echo -ne "module load \$module_samtools\n" >> STEP_4.sub
                	fi
			sh $location/x_scripts/run_Pileup.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ \${final_genome_name} $location/x_scripts

			if [ \$call_variants == "yes" ]; then
				if [ \$use_modules == "yes" ]; then
                                	if [ \$use_module_wildcard == "yes" ]; then
                                        	echo -ne "\$wildcard_statement\n" >> STEP_4.sub
                                	fi
                                	echo -ne "module load \$module_samtools\n" >> STEP_4.sub
					echo -ne "module load \$module_freebayes\n" >> STEP_4.sub
					echo -ne "module load \$module_virga\n" >> STEP_4.sub
					echo -ne "module load \$module_mugsy\n" >> STEP_4.sub
                        	fi
				sh $location/x_scripts/run_Variant_calling.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ \${final_genome_name} yes
			
			if [ \$filter_false_SNPs == "yes" ]; then
				sh $location/x_scripts/run_Remove_false_SNPs.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ \${final_genome_name} $location/x_scripts
				echo -ne "false_lines=\\\`grep -vPc \"^#\" $location/STEP_4--Assembly_Assessment/Bowtie2/false_variants_that_were_corrected.vcf\\\`\n" >> STEP_4.sub
				echo -ne "if [ \\\$false_lines == "0" ]; then\n" >> STEP_4.sub
				echo -ne "echo \"No false SNPs detected, skipping correctional procedures...\"\n" >> STEP_4.sub
				echo -ne "rm $location/STEP_4--Assembly_Assessment/Bowtie2/revised_\${final_genome_name}.fa\nfi\n" >> STEP_4.sub
				echo -ne "if [ \\\$false_lines != "0" ]; then\n" >> STEP_4.sub
				echo -ne "echo \"False Variants have been detected, proceeding with correctional procedures...\"\n" >> STEP_4.sub
				echo -ne "mkdir $location/STEP_4--Assembly_Assessment/Bowtie2/before_genome_revision\n" >> STEP_4.sub
				echo -ne "cp $location/STEP_4--Assembly_Assessment/Bowtie2/revised_\${final_genome_name}.fa $location/STEP_4--Assembly_Assessment/Bowtie2/before_genome_revision\n" >> STEP_4.sub
				echo -ne "mv $location/STEP_4--Assembly_Assessment/Bowtie2/\${final_genome_name}.fa $location/STEP_4--Assembly_Assessment/Bowtie2/old_\${final_genome_name}.fa\n" >> STEP_4.sub
				echo -ne "mv $location/STEP_4--Assembly_Assessment/Bowtie2/\${final_genome_name}.gff $location/STEP_4--Assembly_Assessment/Bowtie2/old_\${final_genome_name}.gff\n" >> STEP_4.sub
				sh $location/x_scripts/run_Genome_Revision.sh $location/STEP_4--Assembly_Assessment/Bowtie2/before_genome_revision $location/x_scripts \${final_genome_name} $location/x_references
				
		                if [ \$use_modules == "yes" ]; then
                	        	if [ \$use_module_wildcard == "yes" ]; then
                        	        	echo -ne "\$wildcard_statement\n" >> STEP_4.sub
                        		fi
                        		echo -ne "module load \$module_virga\n" >> STEP_4.sub
                        		echo -ne "module load \$module_mugsy\n" >> STEP_4.sub
					echo -ne "module load \$module_clustalw2\n" >> STEP_4.sub
					echo -ne "module load \$module_bowtie2\n" >> STEP_4.sub
					echo -ne "module load \$module_samtools\n" >> STEP_4.sub
					echo -ne "module load \$module_freebayes\n" >> STEP_4.sub
                		fi
				
				genome4_name_loc="$location/STEP_4--Assembly_Assessment/Bowtie2"
				genome4_name="\${final_genome_name}.fa"
				genome4_name_gff="\${final_genome_name}.gff"

		                sh $location/x_scripts/run_Index_Bowtie2.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ $location/STEP_4--Assembly_Assessment/Bowtie2/\${final_genome_name}.fa \$final_genome_name
				echo -ne "echo \"#~# second_mapping #~#\" 1>&2\n" >> STEP_4.sub
		                sh $location/x_scripts/run_Bowtie2_mapping_genome.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ \$final_genome_name \$read_loc \$input_type \$PBS_cores
				echo -ne "echo \"#~# end second_mapping #~#\" 1>&2\n" >> STEP_4.sub
	                        sh $location/x_scripts/run_Pileup.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ \${final_genome_name} $location/x_scripts
				sh $location/x_scripts/run_Variant_calling.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ \${final_genome_name} yes	
		fi

			echo -ne "fi\n" >> STEP_4.sub
			sh $location/x_scripts/run_Variant_calling_Freebayes.sh $location/STEP_4--Assembly_Assessment/Bowtie2 \$final_genome_name $location/x_scripts \${final_genome_name}.fa \$freebayes_min_depth \$freebayes_strand_bias \$freebayes_max_homopolymers
			if [ \$analyze_genes == "yes" ]; then
                                        sh $location/x_scripts/run_exon_annotation_comparison.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ $location/x_scripts $location/x_references \$genome4_name_loc/\$genome4_name \$genome4_name_loc/\$genome4_name_gff \$final_genome_name
                                fi

			if [ \$create_low_coverage_gff == "yes" ]; then
                                sh $location/x_scripts/run_Create_Low_GFF.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ $location/x_scripts \$low_coverage_threshold
                        fi

			if [ \$create_no_coverage_gff == "yes" ]; then
				sh $location/x_scripts/run_Create_Gap_GFF.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ $location/x_scripts \${final_genome_name}.fa
			fi

				if [ \$create_coverage_graphic == "yes" ]; then
					if [ \$use_modules == "yes" ]; then
                                        	if [ \$use_module_wildcard == "yes" ]; then
                                                	echo -ne "\$wildcard_statement\n" >> STEP_4.sub
                                        	fi
                                        	echo -ne "module load \$module_R\n" >> STEP_4.sub
						echo -ne "module load \$module_circos\n" >> STEP_4.sub
                                	fi
					sh $location/x_scripts/run_Create_Coverage_Figure.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ $location/x_scripts \${final_genome_name}.gff \${final_genome_name}.fa corrected.pileup low_coverage.gff no_coverage.gff
					if [ \$create_circos_graphic == "yes" ]; then
						sh $location/x_scripts/run_Create_Histograms.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ $location/x_scripts \${final_genome_name}.sam
						sh $location/x_scripts/run_Create_Circos_Figure.sh $location/STEP_4--Assembly_Assessment $location/x_scripts \${final_genome_name}.fa \${final_genome_name}.gff low_coverage.gff no_coverage.gff freebayes_\${final_genome_name}.vcf \${final_genome_name}.vcf read_span_good.txt read_span_bad.txt
					fi
					echo -ne "mv coverage_graphic.png coverage_graphic_\${final_genome_name}.png\n" >> STEP_4.sub
				fi
				if [ \$include_TRL_TRS == "yes" ]; then
                                        if [ \$use_modules == "yes" ]; then
                                                if [ \$use_module_wildcard == "yes" ]; then
                                                        echo -ne "\$wildcard_statement\n" >> STEP_4.sub
                                                fi
                                        fi
                                        sh $location/x_scripts/run_Full_Length_HSV1_Conversion.sh $location/STEP_4--Assembly_Assessment/Bowtie2/ $location/x_scripts \${final_genome_name}.fa \${final_genome_name}.gff
                                fi	
			fi
		fi
	fi

	if [ \$PBS_use == "yes" ]; then
                new_job_IDs="\$(qsub -W depend=afterok:\$job_IDs STEP_4.sub | grep -Po "^\d+")"
                job_IDs=\$new_job_IDs
        fi
        if [ \$PBS_use != "yes" ]; then
                echo -ne "bash x_job_files/STEP_4.sub 2>&1 | tee -a run_manually.log\n" >> $location/run_manually.sh
        fi

fi



#######################################################################################################################################################
#-- Begin post_STEP code

if [ \$generate_report == "yes" ]; then
	echo -ne \$script_header > $location/x_job_files/post_report.sub
	sh $location/x_scripts/run_Post_Report.sh $location \$run_step4 $location/STEP_4--Assembly_Assessment \$final_genome_name
	if [ \$PBS_use == "yes" ]; then
                new_job_IDs="\$(qsub -W depend=afterok:\$job_IDs post_report.sub | grep -Po "^\d+")"
                job_IDs=\$new_job_IDs
        fi
        if [ \$PBS_use != "yes" ]; then
                echo -ne "bash x_job_files/post_report.sub\n" >> $location/run_manually.sh
        fi
fi

EOF
chmod 755 VirGA_pipeline

base_dir=`dirname $0`

# Copy the script files from the repository to the working directory
cp $base_dir/../Scripts/run_Clipping.sh $location/x_scripts
cp $base_dir/../Scripts/run_Trimming.sh $location/x_scripts
cp $base_dir/../Scripts/run_Artifact.sh $location/x_scripts
cp $base_dir/../Scripts/run_Index.sh $location/x_scripts
cp $base_dir/../Scripts/run_Mapping.sh $location/x_scripts
cp $base_dir/../Scripts/filter_reads_using_sam.pl $location/x_scripts
cp $base_dir/../Scripts/run_Fastqc_after.sh $location/x_scripts
cp $base_dir/../Scripts/run_Q30-Histogram_after.sh $location/x_scripts
cp $base_dir/../Scripts/run_Fastqc_before.sh $location/x_scripts
cp $base_dir/../Scripts/run_Q30-Histogram_before.sh $location/x_scripts
cp $base_dir/../Scripts/save_paired_reads.pl $location/x_scripts
cp $base_dir/../Scripts/trimmomatic-0.32.jar $location/x_scripts

cp $base_dir/../Scripts/run_Celera.sh $location/x_scripts
cp $base_dir/../Scripts/create_weighted_qual_file_for_celera.pl $location/x_scripts
cp $base_dir/../Scripts/run_Gap4.sh $location/x_scripts
cp $base_dir/../Scripts/pregap4.config $location/x_scripts
cp $base_dir/../Scripts/select_ssake_kmers.sh $location/x_scripts
cp $base_dir/../Scripts/check_ssake_quality_metrics.pl $location/x_scripts
cp $base_dir/../Scripts/find_ssake_kmers.pl $location/x_scripts

cp $base_dir/../Scripts/clean_multi_fasta.pl $location/x_scripts
cp $base_dir/../Scripts/run_MUGSY_Maf-Net.sh $location/x_scripts
cp $base_dir/../Scripts/convert_fasta_to_multiple_lines.pl $location/x_scripts
cp $base_dir/../Scripts/run_Compare_Genomes.sh $location/x_scripts
cp $base_dir/../Scripts/run_GapFiller.sh $location/x_scripts
cp $base_dir/../Scripts/remove_flanking_Ns_from_genome.fa $location/x_scripts
cp $base_dir/../Scripts/run_Recreate_Annotation.sh $location/x_scripts

cp $base_dir/../Scripts/create_gff_of_low_coverage_from_pileup.pl $location/x_scripts
cp $base_dir/../Scripts/run_Create_Low_GFF.sh $location/x_scripts
cp $base_dir/../Scripts/run_Remove_false_SNPs.sh $location/x_scripts
cp $base_dir/../Scripts/filter_normal_vcf_for_assembly_anomalies.pl $location/x_scripts
cp $base_dir/../Scripts/create_corrected_pileup_including_Ns.pl $location/x_scripts
cp $base_dir/../Scripts/run_Index_Bowtie2.sh $location/x_scripts
cp $base_dir/../Scripts/run_Bowtie2_mapping_genome.sh $location/x_scripts
cp $base_dir/../Scripts/run_Pileup.sh $location/x_scripts
cp $base_dir/../Scripts/run_Variant_calling.sh $location/x_scripts
cp $base_dir/../Scripts/filter_freebayes_vcf.pl $location/x_scripts
cp $base_dir/../Scripts/run_Variant_calling_Freebayes.sh $location/x_scripts

cp $base_dir/../Scripts/combine_and_sort_multi_exon_files.pl $location/x_scripts
cp $base_dir/../Scripts/extract_genes_and_transcripts_from_fasta.pl $location/x_scripts
cp $base_dir/../Scripts/scanning_exons_for_aberrations.pl $location/x_scripts
cp $base_dir/../Scripts/translate_fasta_exon_to_protein.pl $location/x_scripts
cp $base_dir/../Scripts/create_alignments_from_fastas.sh $location/x_scripts
cp $base_dir/../Scripts/run_exon_annotation_comparison.sh $location/x_scripts
cp $base_dir/../Scripts/filter_missassemblies.pl $location/x_scripts
cp $base_dir/../Scripts/run_Genome_Revision.sh $location/x_scripts
cp $base_dir/../Scripts/format_PE_fastq_for_SSAKE.pl $location/x_scripts
cp $base_dir/../Scripts/format_Singleton_fastq_for_SSAKE.pl $location/x_scripts
cp $base_dir/../Scripts/create_gff_of_Ns_from_genome.pl $location/x_scripts
cp $base_dir/../Scripts/run_Create_Gap_GFF.sh $location/x_scripts
cp $base_dir/../Scripts/create_gff_graphic_in_R.pl $location/x_scripts
cp $base_dir/../Scripts/run_Create_Coverage_Figure.sh $location/x_scripts
cp $base_dir/../Scripts/circos_conversion.pl $location/x_scripts
cp $base_dir/../Scripts/histogram_per-base_generator.pl $location/x_scripts
cp $base_dir/../Scripts/histogram_read-length_generator.pl $location/x_scripts
cp $base_dir/../Scripts/run_Create_Circos_Figure.sh $location/x_scripts
cp $base_dir/../Scripts/run_Create_Histograms.sh $location/x_scripts

cp $base_dir/../Scripts/generate_report.pl $location/x_scripts
cp $base_dir/../Scripts/run_Post_Report.sh $location/x_scripts
cp $base_dir/../Scripts/check_assembly_for_quality_metrics.pl $location/x_scripts
cp $base_dir/../Scripts/convert_trimmed_genome_to_full_genome.pl $location/x_scripts
cp $base_dir/../Scripts/run_Full_Length_HSV1_Conversion.sh $location/x_scripts
cp $base_dir/../Scripts/check_for_dependencies.sh $location/x_scripts
cp $base_dir/../Scripts/run_Utility_reassembly.sh $location/x_scripts
cp $base_dir/../Scripts/excise_target_genomic_area.pl $location/x_scripts
cp $base_dir/../Scripts/bands.conf $location/x_scripts
cp $base_dir/../Scripts/circos_good-qual.conf $location/x_scripts
cp $base_dir/../Scripts/circos_bad-qual.conf $location/x_scripts
cp $base_dir/../Scripts/ideogram.conf $location/x_scripts
cp $base_dir/../Scripts/ideogram.label.conf $location/x_scripts
cp $base_dir/../Scripts/ideogram.position.conf $location/x_scripts
cp $base_dir/../Scripts/ticks.conf $location/x_scripts

### Write out the parameters.ini file
cat>VirGA_parameters.ini<<EOF


 ########################################################################
##########################################################################
#                                                                        #
#                           VirGA_parameters.ini                         #
#           By Moriah Szpara, Lance Parsons, and Jacob Shreve            #
#            Szpara lab, The Pennsylvania State University               #
#     Contact: moriah@psu.edu,  https://bitbucket.org/lparsons/VirGA     #
#                                 $version                                   #
#                                                                        #
##########################################################################
 ########################################################################


#  Configure the lines below prior to running the VirGA_pipeline. 


#  ==================================================================
#  Required parameters for all jobs

#-- Run the pipeline using submitted jobs through PBS/Torque scheduler
PBS_use="yes"                     #-- (yes/no)

	#-- If no, the pipeline with generate a bash script (.sh) file to be run manually
	#-- If yes, please define the following PBS parameters
	PBS_queue="biostar"         #-- (default="batch")
	PBS_walltime="30:00:00"    #-- (default="8:00:00)

#-- Identify the type of input file: Single-end reads (SE), paired-end reads (PE), or genome (GE)
input_type="PE"                   #-- (SE/PE/GE)

#-- How many cores to use for multi-threaded steps? This effects both cluster and manual usage.
PBS_cores="6"			  #-- (default="1")

#-- Define the name for the resulting assembled genome
final_genome_name="HSV-Test"

#-- Generate a report after pipeline completion?
generate_report="yes"

#  ==================================================================
#  STEP_1--Preprocessing

  ### Requirements for STEP_1 ###
  #   Either single-end (SE) reads or paired-end (PE) reads in .fastq format

#-- Conduct STEP_1, which is used to preprocess raw sequencing reads in .fastq files
run_step1="yes"			  #-- (yes/no)

	#-- Create infographics of the raw reads?
	before_stats="yes"                #-- (yes/no)

	#-- Create infographics of the processed reads?
	after_stats="yes"                 #-- (yes/no)

	#-- Clip adapters from the reads?
	clip_adapters="yes"               #-- (yes/no)

		adapter="AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT"   #-- (adapter to be clipped)
		# TruSeq universal adapter: AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT

		min_clip_keep_length="30"   #-- (Required minumum length to save read)

	#-- Trim poor quality bases from the 3' end of the reads?
	trim_reads="yes"                  #-- (yes/no)

 		min_qual="30"               #-- (Required minimum PHRED score to save base)
 		min_trim_keep_length="30"   #-- (Required minimum length to save read)
		window_size="15"	    #-- (Window size used for quality scoring)

	#-- Remove artifacts from the reads?
	filter_artifacts="yes"            #-- (yes/no)

	#-- Remove reads that match to a contaminate reference?
	filter_contaminants="yes"         #-- (yes/no)

	#-- Only save processed reads that are paired (only applicable to PE reads)
	only_save_paired="yes"		  #-- (yes/no)


#  ==================================================================
#  STEP_2--Multi_SSAKE_Assembly

  ### Requirements for STEP_2 ###
  #   Either single-end (SE) reads or paired-end (PE) reads in .fastq format
  #   A reference genome for identifying the best assembled contigs, such as HSV-1 JN555585.fa

#-- Conduct STEP_2, which is used to assemble sequencing reads in .fastq files into a flattened genome
run_step2="yes"			    #-- (yes/no)

	#-- Conduct Multi_SSAKE assembly of reads into high quality contigs?
	use_multi_ssake="yes"	    #-- (yes/no)

		#-- In a comma-delimited list, define the overlap kmer and contigs trimming for SSAKE to use
		ssake_parameters="16-0,19-0,20-0,29-0,16-4,19-4,20-4,29-4"
		
		#-- Memory reservation for SSAKE jobs (if using a cluster scheduler)
		ssake_memory="60gb"

		#-- Identify the best kmers for assembly (very computationally expensive)
		find_SSAKE_kmers="no"

	#-- Combine Multi_SSAKE contigs using the automatic Celera method?
	use_celera="yes"	    #-- (yes/no)

		#-- Define contig edge error modelling (advanced usage)
		min_base_score="10"
		max_base_score="40"
		percent_contig_ends="0.10"


#  ==================================================================
#  STEP_3--Annotation

  ### Requirements for STEP_3 ###
  #   A sample genome or contig file, such as the output of STEP_2
  #   A reference genome for identifying the best assembled contigs, such as HSV-1 JN555585.fa
  #   A .gff/.gtf annotate file for the reference genome, such as HSV-1 JN555585.gff

#-- Conduct STEP_3, which is used to annotate a flattened genome by aligning to a reference genome
run_step3="yes"                     #-- (yes/no)

	#-- Align/linearize the sample genome with MUGSY and maf_net.py?
	use_mugsy_maf_net="yes"     #-- (yes/no)

	#-- Annotate the sample from a reference using compare_genomes.py (requires mugsy/maf_net output)?
	use_compare_genomes="yes"   #-- (yes/no)

	#-- Use GapFiller to attempt to close gaps within the newly assembled genome (only for PE-reads)?
                use_gapfiller="yes"        #-- (yes/no)

			#-- Define the expected insert size for PE-reads
			insert_size="500"

			#-- Define allowed insert size deviation
			insert_deviation="0.2" 

                #-- Use MUGSY/Maf-Net/Compare_genomes to rebuild .GFF annotation file for gapfilled genome?
                recreate_annotation="yes"  #-- (yes/no)


#  ==================================================================
#  STEP_4--Assembly_Assessment

#-- Conduct STEP_4, which is used to assemble sequencing reads in .fastq files into a flattened genome
run_step4="yes"                     #-- (yes/no)

  ### Requirements for STEP_4 ###
  #   An assembled genome fasta file, such as the output of STEP_2
  #   An assembled genome annotation file (optional but recommended), such as the output of STEP_2
  #   Original reads used to assemble the genome, such as the output from STEP_1

	#-- Map the reads with Bowtie2 (required for all downstream processes)?
	bowtie2_map="yes"          #-- (yes/no)

	#-- Create a pileup file? (required for all downstream processes)?
	create_pileup="yes"	   #-- (yes/no)

	#-- Call variants to the reference for each input?
        call_variants="yes"        #-- (yes/no)

        #-- Revise the assembly by filtering out false-positive Variants due to assembly errors?
        filter_false_SNPs="yes"    #-- (yes/no)

	#-- Determine the differences between reference and sample gene features?
	analyze_genes="yes"	   #-- (yes/no)

	#-- Set what is considered low coverage for image and GFF creation
	low_coverage_threshold="15"

		#-- Create a GFF identifying areas of the genome that recieved low coverage?
		create_low_coverage_gff="yes"

		#-- Create a GFF identifying areas of the genome that are assembly gaps?
		create_no_coverage_gff="yes"

		#-- Create a coverage figure using the genome, annotation, and pileup files?
                create_coverage_graphic="yes"

		#-- Create a circos figure using the genome, annotation, and pileup files?
                create_circos_graphic="yes"
	
	#-- Configure the freebayes SNP filtering parameters

		#-- Specify the minimum read depth needed to qualify as a SNP
		freebayes_min_depth="100"

		#-- Specify the allowed percentage of strand bias
		freebayes_strand_bias="0.80"

		#-- Specify the maximum allowed number of homopolymer nucleotides flanking the SNP site
		freebayes_max_homopolymers="5"

	#-- Create non-trimmed genome with both IRL/IRS and TRL/TRS features? (HSV1 genomes only!)
	#-- Also requires the reference genome having "IRL", "TRL", and "a prime" features.
		include_TRL_TRS="yes"


#  ==================================================================
#  UTILITY--Targeted local reassembly

#-- Conducts reassembly, which is used to assemble through difficult genomic locations
run_reassembly="yes"                     #-- (yes/no)

  ### Requirements for Targeted local reassembly ###
  #   All previous steps are ignored
  #   Required files: read_1.fastq read_2.fastq mapping.sam genome.fa
  #   Input files should be placed in the "x_input" diretory
  #   Output will be placed in the "UTILITY_reassembly" directory

        #-- Define the start position of the genome segment to excise
        excise_start=""

        #-- Define the end position of the genome segment to excise
        excise_end=""


#  ==================================================================
#  Software compatibility

  #   Clusters often load software through a module system. Before use, a software
  #   package needs to be loaded using a command, such as "module load sample_software".
  #   Because different system admins may name module differently, the following
  #   software/module pairs need be accurate for the system on which VirGA is being used.

#-- Use a module-based software loading system
use_modules="yes"		   #-- (yes/no)
         
	#-- Add a wildcard line before each module usage statement (such as "module load bioinformatics-modules")
	use_module_wildcard="yes"  #-- (yes/no)

		#-- Define the wildcard statement
		wildcard_statement="module load R"	#-- Depends on the specific unix environment

	#-- If using modules, please check that each software package is calling the correct module name

	#-- FastX-Toolkit
	module_fastx="fastx"

	#-- FastQC
	module_fastqc="fastqc"

	#-- R statistical language
	module_R="R"

	#-- Bowtie2
	module_bowtie2="bowtie"

	#-- SAMTools
	module_samtools="samtools/0.1.19"

	#-- STADEN (gap4) package
	module_staden="gap4"

	#-- MUGSY multiple genome aligner
	module_mugsy="mugsy"

	#-- Virus Genome Assembly (VirGA) package
	module_virga="virga"

	#-- Freebayes
	module_freebayes="freebayes"

	#-- Celera assembler
	module_celera="celera"
	
	#-- GapFiller
	module_gapfiller="gapfiller"

	#-- ClustalW
	module_clustalw2="clustalw2"

	#-- SSAKE
	module_ssake="ssake"

	#-- Circos
	module_circos="circos"
EOF

