#!/usr/bin/perl -w

open(FASTA,$ARGV[2]) || die("Can't open the sample genome\n");
while(<FASTA>)
{
	$_ =~ s/\n//;
	if($_ =~ /^>/)
	{
		$genome_name = $_;
	}
	if($_ !~ /^>/)
	{
		$genome_seq .= $_;
	}
}
close(FASTA);

open(Samtools,$ARGV[0]) || die("Can't open the Samtools file\n");
while(<Samtools>)
{
	if($_ =~ /^#/){ next; }
	$_ =~ s/\n//;
	@splitter = split(/\t/,$_);
	if($splitter[4] =~ /,/){ next; }
	$splitter[7] =~ m/DP4=(\d+),(\d+),(\d+),(\d+)/;
	$SRF = $1;
	$SRR = $2;
	$SAF = $3;
	$SAR = $4;
	$depth = ($splitter[7] =~ m/DP=(\d+)/)[0];
	if((($SAF+$SAR) > ($SRF+$SRR)) and $depth >= 100)
        {
                #print "******** Mis-assembly ************ \n";
                #print "$_\nDepth:$depth\nSAF: $SAF\nSAR: $SAR\nSRF: $SRF\nSRR: $SRR\n\n";
        	$REPLACE{$splitter[1]} = $_;
		if(length($splitter[4]) >= length($splitter[3]))
		{
			for($i=0; $i<length($splitter[4]); $i++)
			{
				$HASH{$splitter[1]+$i} = "1";
				#print "added ".($splitter[1]+$i)." to the HASH table\n";
			}
		}
		if(length($splitter[4]) < length($splitter[3]))
                {
                        for($i=0; $i<length($splitter[3]); $i++)
                        {
                                $HASH{$splitter[1]+$i} = "1";
                                #print "added ".($splitter[1]+$i)." to the HASH table\n";
                        }
		}
	}
	
}
close(Samtools);

#print "\n\n***********************************\nBreak\n\n\n";

open(Freebayes,$ARGV[1]) || die("Can't open the Freebayes file\n");
while(<Freebayes>)
{
	if($_ =~ /^#/){ next; }
	$_ =~ s/\n//;
	@splitter = split(/\t/,$_);
	if($splitter[4] =~ /,/){ next; }
	$SAF = ($splitter[7] =~ m/SAF=(\d+)/)[0];
	$SAR = ($splitter[7] =~ m/SAR=(\d+)/)[0];
	$SRF = ($splitter[7] =~ m/SRF=(\d+)/)[0];
	$SRR = ($splitter[7] =~ m/SRR=(\d+)/)[0];
	$depth = ($splitter[7] =~ m/DP=(\d+)/)[0];
	if((($SAF+$SAR) > ($SRF+$SRR)) and $depth >= 100)
	{
		$skip = "no";
		#foreach (sort { $a <=> $b } keys(%HASH))
		#{
		#	print STDERR "Keys: $_\n";
		#}
		for($i=0; $i<length($splitter[4]); $i++)
		{
			#while(($key,$value) = each %HASH)
			#{
			#	print "Key: $key, Value: $value\n";
			#}
			#print "$splitter[1] + $i!\n";
			if(exists $HASH{$splitter[1]+$i})
			{
				print STDERR "Skipping the following VCF line, because the coordinates coincide with output in the Samtools output:\n$_\n\n";
				$skip = "yes";
				last;
			}
		}
		if($skip eq "no")
		{
			#print "******** Mis-assembly ************ \n";
			#print "$_\nDepth: $depth\nSAF: $SAF\nSAR: $SAR\nSRF: $SRF\nSRR: $SRR\n\n";
			if(exists $REPLACE{$splitter[1]})
			{
				print STDERR "ERROR: freebayes overwriting samtools variant\n";
				exit 1;
			}
			$REPLACE{$splitter[1]} = $_;
		}
	}
}
close(Freebayes);

open(FSNPs,">>false_variants_that_were_corrected.vcf") || die("Can't open the output variant file for record keeping\n");
foreach (sort { $b <=> $a } keys(%REPLACE))
{
	@splitter = split(/\t/,$REPLACE{$_});
	#print "$REPLACE{$_}\n";
	$confirm = substr($genome_seq,($splitter[1]-1),length($splitter[3]));
	#print "From genome: $confirm, from VCF: $splitter[3]\n";
	if($confirm ne $splitter[3])
	{
		print STDERR "ERROR: substring of genome doesn't match VCF\n$REPLACE{$_}\nFrom genome: $confirm\nFrom VCF: $splitter[3]\n";
		exit 1;
	}
	substr($genome_seq,($splitter[1]-1),length($splitter[3]),$splitter[4]);
	$reconfirm = substr($genome_seq,($splitter[1]-1),length($splitter[4]));
	#print "from genome (reconfirm): $reconfirm, from VCF; $splitter[4]\n";
	if($reconfirm ne $splitter[4])
	{
		print STDERR "ERROR reconfirmation of replaced vcf substring into genome failed to match\n";
		exit 1;
	}
	print FSNPs "$REPLACE{$_}\n";
}

print "$genome_name\n$genome_seq\n";
