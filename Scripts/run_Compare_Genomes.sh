cat>>STEP_3.sub<<eof
cd $1
ref_name=\`head -n 1 $2/reference.fa | sed 's/>//'\`
#sample_base=\`echo $4 | sed -r 's/\..*//'\`
sample_base="assembled"
compare_genomes.py --reference=reference.\${ref_name} --gff_feature_types=exon,gene,genome_segment,miRNA,misc_feature,repeat_region,stem_loop,CDS --gff_attributes=ID,Parent,gene,function $2/mugsyOutput.net.afa $3
cp compare_genomes_output/contigs.\${ref_name}.gff ../contigs.gff

sed -ir "s/^contigs.\${ref_name}\t/\${sample_base}_genome\t/" ../contigs.gff
rm ../contigs.gffr
mv ../contigs.gff ../\${sample_base}_genome.gff
echo ">\${sample_base}_genome" > ../\${sample_base}_genome.fa
grep -v ">" ../contigs.fa >> ../\${sample_base}_genome.fa
rm ../contigs.fa
eof
