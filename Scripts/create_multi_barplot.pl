#!/usr/bin/perl -w
# User provides $1=mapping.pileup, $2=mapping.gff, $3=reference.fasta, $4=low_coverage_threshold
$low_coverage_threshold = $ARGV[3];

open(REF,$ARGV[2])||die("Can't open the reference\n");
while(<REF>)
{
	if($_ =~ /^>/)
	{
		next;
	}
	if($_ !~ /^>/)
	{
		$_ =~ s/\n//g;
		$ref_string .= $_;
	}
}
close(REF);


$first = 0;
$pos = -1;
$master_blank ="";
$real_position = 0;
open(PILEUP,$ARGV[0]) || die("Can't open the mpileup file\n");
while(<PILEUP>)
{
	$real_position++;
	while($real_position != ($_ =~ m/^.*?\t(.*?)\t/)[0])
        {
		$master_blank .= "1";
		$pos++;
		$depth[$pos] = 0;
		$real_position++;
		next;
        }
	$master_blank .= "1";
        $pos++;
        $_ =~ s/\n//;
        if($first == 0)
        {
                $first = 1;
                @files = split(/\t/,$_);
                $samples = ($#files-2)/3;
        }
        @splitter = split(/\t/,$_);

	if($splitter[2] ne substr($ref_string,$splitter[1]-1,1))
	{
		if(($splitter[2] ne "N") and ((substr($ref_string,$splitter[1]-1,1)) ne "N"))
		{
			print "ERROR: pileup ref base doesn't match the reference!\n";
			print "splitter: $splitter[2], ".(substr($ref_string,$splitter[1]-1,1))."\n";
			exit 1;
		}
	}

        $current_sample = 1;
        $depth_col = 0;
        while($current_sample <= $samples)
        {
                $depth_col += 3;
                $depth[$pos] += $splitter[$depth_col];
                $current_sample++;
        }
}
close(PILEUP);

while($real_position ne length($ref_string))
{
	$master_blank .= "1";
	$pos++;
	$depth[$pos] = 0;
	$real_position++;
}

$gene_track = $master_blank;
$LAT_track = $master_blank;
$reit_track = $master_blank;
print "Length: ".(length($master_blank))."\n";
open(GFF,$ARGV[1]) || die("Can't open the gff\n");
while(<GFF>)
{
	$_ =~ s/\n//;
	@splitter = split(/\t/,$_);
	$start = $splitter[3];
        $stop = $splitter[4];
	if(($splitter[2] eq "CDS" or $splitter[2] eq "gene") and ($_ !~ /ID=LAT/))
	{
		substr($gene_track, $start-1, (($stop-$start+1))) = "2"x(($stop-$start)+1);
	}
	if($_ =~ /ID=LAT/)
	{
		substr($LAT_track, $start-1, (($stop-$start+1))) = "5"x(($stop-$start)+1);
	}
	if($splitter[2] eq "repeat_region" or $splitter[2] eq "step_loop")
	{
		substr($reit_track, $start-1, (($stop-$start+1))) = "3"x(($stop-$start)+1);
	}	
}
close(GFF);
@split_gene = split(//,$gene_track);
$total_genes = join(",",@split_gene);
@split_LAT = split(//,$LAT_track);
$total_LAT = join(",",@split_LAT);
@split_reit = split(//,$reit_track);
$total_repeats = join(",",@split_reit);
print "Length of Depth_array = ".($#depth)."Length after: ".(length($gene_track))." and ".(length($LAT_track))." and ".(length($reit_track))." \n";

$x_count = 0;
$x_total = "";
$low_total = "";
foreach $DEEP (@depth)
{
	$x_count++;
	$x_total .= "$x_count,";
	if($DEEP <= $low_coverage_threshold)
	{
		$low_total .= "10,";
	}
	if($DEEP > $low_coverage_threshold)
	{
		$low_total .= "1,";
	}
	$total_value .= "$DEEP,";
}

$total_value =~ s/,$//;
$x_total =~ s/,$//;
$low_total =~ s/,$//;
$location = `pwd`;
$location =~ s/\n//;

$comma1 =()= $total_value =~ /,/gi;
$comma2 =()= $low_total =~ /,/gi;
print "Here: $comma1\t$comma2\n";

open(FILE_OUT,">>coverage_plot.r") || die("Can't open the outfile for writing");
print FILE_OUT "setwd(\"$location\")\n";
print FILE_OUT "library(ggplot2)\n";
print FILE_OUT "bulk = data.frame(x_pos=c($x_total),depth=c($total_value),low_depth=c($low_total),repeats=c($total_repeats),genes=c($total_genes),lat=c($total_LAT))\n";
print FILE_OUT "png(filename=\"coverage_graphic_with_GFF_info.png\", width=4000, height=800)\n";
print FILE_OUT "ggplot(bulk, aes(x=x_pos), aes(alpha=0.2)) +\n";
print FILE_OUT "  geom_bar(aes(y=low_depth), color=\"red\", stat=\"identity\") +\n";
print FILE_OUT "  geom_bar(aes(y=lat), color=\"black\", stat=\"identity\") +\n";
print FILE_OUT "  geom_bar(aes(y=repeats), color=\"green\", stat=\"identity\") +\n";
print FILE_OUT "  geom_bar(aes(y=genes), color=\"blue\", stat=\"identity\") +\n";
print FILE_OUT "  geom_line(aes(y=depth), color=\"black\", size=1) +\n";
print FILE_OUT "  scale_y_log10()\n";

#ggplot(bulk, aes(x=x_pos)) + 
#  geom_bar(aes(y=y_bar), stat="identity") +
#  geom_line(aes(y=y_line), color="red", size=2)


print FILE_OUT "dev.off()\n";
close(FILE_OUT);
system("R --vanilla < coverage_plot.r > output_from_R");
system("rm coverage_plot.r");
system("rm output_from_R");
