cat>>STEP_3.sub<<eof
genome_name="assembled_genome.fa"
if [ $7 ]; then
	genome_name="$7"
fi
cd $1
perl $2/clean_multi_fasta.pl $4/$5 0 Reference > reference.fa
perl $2/clean_multi_fasta.pl $3/\$genome_name 0 Contigs > contigs.fa

ref_name=\`head -n 1 reference.fa | sed 's/>//'\`
mugsy --directory . --prefix mugsyOutput reference.fa contigs.fa
maf_net.py -r reference -s contigs,reference -c \$ref_name --consensus_sequence mugsyOutput.maf
perl $2/convert_fasta_to_multiple_lines.pl contigs.\${ref_name}.consensus.fasta > maf-net_gapfilled_output.fa
compare_genomes.py --reference=reference.\${ref_name} --gff_feature_types=exon,gene,genome_segment,miRNA,misc_feature,repeat_region,stem_loop --gff_attributes=ID,Parent,gene,function mugsyOutput.net.afa $6
cp compare_genomes_output/contigs.\${ref_name}.gff maf-net_gapfilled_output.gff

sed -ir "s/^contigs.\${ref_name}\t/assembled_genome\t/" maf-net_gapfilled_output.gff
rm maf-net_gapfilled_output.gffr
mv maf-net_gapfilled_output.gff assembled_genome.gff
echo ">assembled_genome" > assembled_genome.fa
grep -v ">" maf-net_gapfilled_output.fa >> assembled_genome.fa
cp assembled_genome.fa ../
cp assembled_genome.gff ../
eof
