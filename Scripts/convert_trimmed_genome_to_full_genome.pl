#!/usr/bin/perl -w

#  Usage:     perl convert_trimmed_genome_to_full_genome.pl /
#	      genome.fa /
#  	      genome.gff /
#
#  Requires:  Must have three particular GFF entries, namely
#             ID=IRL
#             ID=IRS
#             IS=a prime

### Load the full genome into a variable
open(FASTA, $ARGV[0]) || die("Can't open the FASTA file\n");
while(<FASTA>)
{
	if($_ =~ /^>/)
	{
		$_ =~ s/\n//;
		$genome_name = $_;
		next;
	}
	$_ =~ s/\n//;
	$full_genome .= $_;
}
close(FASTA);

### Grab just the IRL, IRS, and a_prime locations from the GFF file
open(GFF, $ARGV[1]) || die("Can't open the GFF file\n");
while(<GFF>)
{
	if($_ =~ /repeat_region.*=IRL(;|$)/)
	{
		@split_IRL = split(/\t/,$_);
		$IRL_start = $split_IRL[3];
		$IRL_stop = $split_IRL[4];
	}
	if($_ =~ /repeat_region.*=IRS(;|$)/)
	{
		@split_IRS = split(/\t/,$_);
		$IRS_start = $split_IRS[3];
		$IRS_stop = $split_IRS[4];
	}
	if($_ =~ /repeat_region.*=a prime(;|$)/)
	{
		#@split_a = split(/\t/,$_);
		#$a_start = $split_a[3];
		#$a_stop = $split_a[4];
	}
}
close(GFF);

### Grab the IRL, IRS, and a-prime sequences from the genome using the GFF coordinates
$IRL_seq = &grab_seq($full_genome,$IRL_start,$IRL_stop);
$IRS_seq = &grab_seq($full_genome,$IRS_start,$IRS_stop);
#$a_seq = &grab_seq($full_genome,$a_start,$a_stop);
$length_IRL = length($IRL_seq);
$length_IRS = length($IRS_seq);
#$length_a = length($a_seq);

$part1 = reverse($IRL_seq);
$part1 =~ tr/ATCG/TAGC/;

$part2 = reverse($IRS_seq);
$part2 =~ tr/ATCG/TAGC/;

### Create the full-length genome and TRL, TRS, and alpha GFF coordinates
$lengthened_genome = "${part1}${full_genome}${part2}";
$output_base_fa = $ARGV[0];
if($ARGV[0] =~ /\//)
{
	$output_base_fa = ($ARGV[0] =~ m/\/([^\/]+)$/)[0];
}
open(OUTPUT_GENOME,">full-length_${output_base_fa}") || die("Can't open the output genome file for writing...\n");
print OUTPUT_GENOME "$genome_name\n";
print OUTPUT_GENOME "$lengthened_genome\n";
close(OUTPUT_GENOME);

$output_base_gtf = $ARGV[1];
if($ARGV[1] =~ /\//)
{
	$output_base_gtf = ($ARGV[1] =~ m/\/([^\/]+)$/)[0];
}
open(OUTPUT_GFF,">full-length_${output_base_gtf}") || die("Can't open the output GFF file for writing...\n");
open(GFF, $ARGV[1]) || die("Can't open the GFF file\n");
while(<GFF>)
{
	if($_ =~ /^#/){ next; }
	@split_GFF = split(/\t/,$_);
	$split_GFF[3] += $length_IRL;
	$split_GFF[4] += $length_IRL;
	$genome_name = $split_GFF[0];
	$output_GFF = join("\t",@split_GFF);
	print OUTPUT_GFF $output_GFF;
}
close(GFF);
print OUTPUT_GFF "$genome_name\t-\trepeat_region\t1\t$length_IRL\t.\t+\t.\tID=TRL;\n";
$TRS_start_location = length($part1) + length($full_genome) + 1;
$TRS_stop_location = $TRS_start_location + $length_IRS - 1;
print OUTPUT_GFF "$genome_name\t-\trepeat_region\t$TRS_start_location\t$TRS_stop_location\t.\t-\t.\tID=TRS;\n";

### Read through the GFF and capture features fully encapsulated by the IRL/IRS boundaries
open(GFF, $ARGV[1]) || die("Can't open the GFF file\n");
while(<GFF>)
{
	if($_ =~ /^#/){ next; }
	@split_GFF = split(/\t/,$_);
	if(($split_GFF[3] >= $IRL_start) and ($split_GFF[4] <= $IRL_stop))
	{
		if($_ =~ m/ID=IRL(;|$)/){ next; }
		$feature_length = $split_GFF[4] - $split_GFF[3];
		$split_GFF[3] = $IRL_stop - $split_GFF[4] + 1; # Setting the distance from 1 that the feature starts in TRL
		$split_GFF[4] = $split_GFF[3] + $feature_length;
		$strand = $split_GFF[6];
		if($strand eq "-") { $split_GFF[6] = "+"; }
		if($strand eq "+") { $split_GFF[6] = "-"; }
		$gene_name = ($split_GFF[8] =~ m/ID=(.*?)(;|$)/)[0];
		$gene_name_leftover = ($split_GFF[8] =~ m/ID=$gene_name(.*?)/)[0];
		$gene_name .= "_TRL";
		$split_GFF[8] = "ID=${gene_name}${gene_name_leftover}\n";
		if($split_GFF[8] =~ /ID=a prime/)
		{
			$split_GFF[8] =~ s/a prime/a/g;
		}
		$output_GFF = join("\t",@split_GFF);
		print OUTPUT_GFF $output_GFF;
	}
	@split_GFF = split(/\t/,$_);
	if(($split_GFF[3] >= $IRS_start) and ($split_GFF[4] <= $IRS_stop))
	{
		if($_ =~ m/ID=IRS(;|$)/){ next; }
		$feature_length = $split_GFF[4] - $split_GFF[3];
		$split_GFF[3] = $IRS_stop - $split_GFF[4] + $TRS_start_location;
		$split_GFF[4] = $split_GFF[3] + $feature_length;
		$strand = $split_GFF[6];
                if($strand eq "-") { $split_GFF[6] = "+"; }
                if($strand eq "+") { $split_GFF[6] = "-"; }
		$gene_name = ($split_GFF[8] =~ m/ID=(.*?)(;|$)/)[0];
                $gene_name_leftover = ($split_GFF[8] =~ m/ID=$gene_name(.*?)/)[0];
                $gene_name .= "_TRS";
                $split_GFF[8] = "ID=${gene_name}${gene_name_leftover}\n";
                if($split_GFF[8] =~ /ID=a prime/)
                {
                        $split_GFF[8] =~ s/a prime/a/g;
                }
                $output_GFF = join("\t",@split_GFF);
                print OUTPUT_GFF $output_GFF;
	}
}
close(GFF);
close(OUTPUT_GFF);

sub grab_seq()
{
	$start = $_[1] - 1;
	$end = $_[2] - 1;
	$length = ($end - $start) + 1;
        $fragment = substr $_[0], $start, $length;
	return $fragment;
}
