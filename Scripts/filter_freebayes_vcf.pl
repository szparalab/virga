#!/usr/bin/perl -w

$save_snp = "yes";		#Report SNPs		(yes|no)
$save_ins = "no";		#Report insertions	(yes|no)
$save_del = "no";		#Report deletions	(yes|no)
$save_mnp = "no";		#Report loci with multiple polymorphisms	(yes|no)
$save_complex = "no";		#Report loci with complex 

$minimum_read_depth = $ARGV[2];	#Set minimum read depth cut-off
$maximum_bias = $ARGV[3];		#Set the maximum strand bias allowed at a given locus
$maximum_homopolymer = $ARGV[4];       #Set the maximum number of homopolymer bases allowed leading up to a SNP

open(FASTA,$ARGV[1]) || die("Failed to open the references FASTA file\n");
while(<FASTA>)
{
	$_ =~ s/\n//;
	if($_ =~ /^>/)
	{
		$header = ($_ =~ m/^>(.*?)( |$)/)[0];
		next;
	}
	if($_ !~ /^>/)
	{
		$HASH{$header} .= $_;
	}
}
close(FASTA);

open(VCF,$ARGV[0]) || die("Failed to open the VCF file\n");
while(<VCF>)
{
	$_ =~ s/\n//;
	if($_ =~ /^##/ or $_ =~ /^#CHROM/)
	{
		next;
	}
	@splitter = split(/\t/,$_);
        $total_depth = ($_ =~ m/DP=(\d+)/)[0];      #Total depth at a position (DP)
        $for_ref_count = ($_ =~ m/SRF=(\d+)/)[0];   #Number of reference observations on the forward strand
        $rev_ref_count = ($_ =~ m/SRR=(\d+)/)[0];   #Number of reference observations on the reverse strand
        $for_alt_count = ($_ =~ m/SAF=(\d+)/)[0];   #Number of alternate observations on the forward strand
        $rev_alt_count = ($_ =~ m/SAR=(\d+)/)[0];   #Number of alternate observations on the reverse strand
#	$variant_type = ($_ =~ m/TYPE=([a-zA-Z]+)/)[0];
	if(($save_snp eq "no") and ($splitter[7] =~ /TYPE=snp/)){ push(@VARIANT_filter,$_); next; }
	if(($save_ins eq "no") and ($splitter[7] =~ /TYPE=ins/)){ push(@VARIANT_filter,$_); next; }
	if(($save_del eq "no") and ($splitter[7] =~ /TYPE=del/)){ push(@VARIANT_filter,$_); next; }
	if(($save_mnp eq "no") and ($splitter[7] =~ /TYPE=mnp/)){ push(@VARIANT_filter,$_); next; }
	
	################# Adding a line to force-filter out any variant that involves multiple bases, since freebayes was reporting 3 base SNPs that were killing homopolymer SNP reporting
	if(length($splitter[3]) != 1){ push(@VARIANT_filter,$_); next; }	

	if(($save_complex eq "no") and ($splitter[7] =~ /TYPE=complex/)){ push(@VARIANT_filter,$_); next; }
	if($total_depth < $minimum_read_depth){ push(@DP_filter,$_); next; }
	if(((($for_ref_count + $for_alt_count)/($rev_ref_count + $rev_alt_count + $for_ref_count + $for_alt_count)) >= $maximum_bias) or ((($rev_ref_count + $rev_alt_count)/($for_ref_count + $for_alt_count + $rev_ref_count + $rev_alt_count)) >= $maximum_bias)){ push(@BIAS_filter,$_); next; }
	
	$up_sub = substr($HASH{$splitter[0]}, $splitter[1] - ($maximum_homopolymer+1), $maximum_homopolymer+1);
	$down_sub = substr($HASH{$splitter[0]}, $splitter[1]+1, $maximum_homopolymer+1);
	$target_sub = substr($HASH{$splitter[0]}, $splitter[1] - 1, 1);
	if($target_sub ne $splitter[3])
	{
		print STDERR "\n\nERROR: homopolymer parsing finding wrong target nucleotide\n";
		print STDERR "$_\n";
	}
#	$large_sub = substr($HASH{$splitter[0]}, $splitter[1] - 10, 20);

	$A_up_matches =()= $up_sub =~ /A/gi;
	$T_up_matches =()= $up_sub =~ /T/gi;
	$C_up_matches =()= $up_sub =~ /C/gi;
	$G_up_matches =()= $up_sub =~ /G/gi;
        $A_down_matches =()= $down_sub =~ /A/gi;
        $T_down_matches =()= $down_sub =~ /T/gi;
        $C_down_matches =()= $down_sub =~ /C/gi;
        $G_down_matches =()= $down_sub =~ /G/gi;
	if(($A_up_matches > $maximum_homopolymer) or ($T_up_matches > $maximum_homopolymer) or ($C_up_matches > $maximum_homopolymer) or ($G_up_matches > $maximum_homopolymer) or ($A_down_matches > $maximum_homopolymer) or ($T_down_matches > $maximum_homopolymer) or ($C_down_matches > $maximum_homopolymer) or ($G_down_matches > $maximum_homopolymer)){ push(@HOMOPOLYMER_filter,$_); next; }

	print "$_\n";
}
close(VCF);



print "\n=============================================================================\nVARIANT filtered\n";
foreach $LINE (@VARIANT_filter)
{
	print "$LINE\n";
}

print "\n=============================================================================\nDP filtered\n";
foreach $LINE (@DP_filter)
{
        print "$LINE\n";
}

print "\n=============================================================================\nBIAS filtered\n";
foreach $LINE (@BIAS_filter)
{
        print "$LINE\n";
        $for_ref_count = ($LINE =~ m/SRF=(\d+)/)[0];   #Number of reference observations on the forward strand
        $rev_ref_count = ($LINE =~ m/SRR=(\d+)/)[0];   #Number of reference observations on the reverse strand
        $for_alt_count = ($LINE =~ m/SAF=(\d+)/)[0];   #Number of alternate observations on the forward strand
        $rev_alt_count = ($LINE =~ m/SAR=(\d+)/)[0];   #Number of alternate observations on the reverse strand
	print "for_ref + for_alt = ".($for_ref_count + $for_alt_count)."\n";
	print "rev_ref + ref_alt = ".($rev_ref_count + $rev_alt_count)."\n";
	print "Total forward / All reads = ".(($for_ref_count + $for_alt_count)/($rev_ref_count + $rev_alt_count + $for_ref_count + $for_alt_count))."\n";
	print "Total reverse / All reads = ".(($rev_ref_count + $rev_alt_count)/($rev_ref_count + $rev_alt_count + $for_ref_count + $for_alt_count))."\n\n";
}

print "\n=============================================================================\nHOMOPOLYMER filtered\n";
foreach $LINE (@HOMOPOLYMER_filter)
{
        print "$LINE\n";
}

