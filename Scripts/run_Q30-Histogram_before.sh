cat>>STEP_1.sub<<eof
fastx_quality_stats -Q33 -i $3 -o $2/$1.stats
fastq_quality_boxplot_graph.sh -i $2/$1.stats -o $2/$1_boxplot.png
eof
