#!/usr/bin/perl -w

### Grab the important columns from the kmer file

open(KMERS,$ARGV[0]) || die("Can't open the kmers file\n");
while(<KMERS>)
{
	$_ =~ s/\n//;
	if($_ !~ /^m/){ next; }
	@splitter = split(/\t/,$_);
	$longest{$splitter[0]} = $splitter[3];
	$n80{$splitter[0]} = $splitter[7];
	$total_bases{$splitter[0]} = $splitter[9];
	#print "$splitter[0]\t$splitter[2]\t$splitter[5]\t$splitter[9]\n";
}
close(KMERS);

### Make hashes from the top 30 (15%) results for the three categories

$counter = -1;
foreach $key (sort { $longest {$b} <=> $longest {$a}} keys %longest )
{
	$counter++;
	if($counter < 30)
	{
		$longest_top{$key} = $longest{$key};
	}
}

$counter = -1;
foreach $key (sort { $n80 {$b} <=> $n80 {$a}} keys %n80 )
{
        $counter++;
        if($counter < 30)
        {
		#print "$key\t$n80{$key}\n";
                $n80_top{$key} = $n80{$key};
        }
}

$counter = -1;
foreach $key (sort { $total_bases {$b} <=> $total_bases {$a}} keys %total_bases )
{
        $counter++;
        if($counter < 30)
        {
		#print "$key\t$total_bases{$key}\n";
                $total_bases_top{$key} = $total_bases{$key};
        }
}

### Find 9 kmers, 3 from each category, that overlap in some meaningful way

$list .= ">n80:total_bases\n";
foreach $key (sort { $n80_top {$b} <=> $n80_top {$a}} keys %n80_top )
{
	if(exists $total_bases_top{$key})
	{
		$temp .= "$key\t$n80_top{$key}\t$total_bases_top{$key}\n"; 
	}
}
open(OUT,">kmer_temp.txt") || die("Can't create a temporary file during kmer selection\n");
print OUT "$temp";
close(OUT);
$results_sorted = `sort -k2,2nr -k3,3nr kmer_temp.txt`;
$list .= $results_sorted;

$list .= ">longest:total_bases\n";
foreach $key (sort { $longest_top {$b} <=> $longest_top {$a}} keys %longest_top )
{
        if(exists $total_bases_top{$key})
        {
                $list .= "$key\t$longest_top{$key}\t$total_bases_top{$key}\n";
        }
}

$list .= ">longest:n80\n";
foreach $key (sort { $longest_top {$b} <=> $longest_top {$a}} keys %longest_top )
{
        if(exists $n80_top{$key})
        {
                $list .= "$key\t$longest_top{$key}\t$n80_top{$key}\n";
        }
}

$list .= ">longest\n";
$top9 = 0;
foreach $key (sort { $longest_top {$b} <=> $longest_top {$a}} keys %longest_top )
{
	$top9++;
	if($top9 <= 9)
	{
		$list .= "$key\t$longest_top{$key}\n";
	}
}

$list .= ">n80\n";
$top9 = 0;
foreach $key (sort { $n80_top {$b} <=> $n80_top {$a}} keys %n80_top )
{
        $top9++;
        if($top9 <= 9)
        {
                $list .= "$key\t$n80_top{$key}\n";
        }
}

$list .= ">total_bases\n";
$top9 = 0;
foreach $key (sort { $total_bases_top {$b} <=> $total_bases_top {$a}} keys %total_bases_top )
{
        $top9++;
        if($top9 <= 9)
        {
                $list .= "$key\t$total_bases_top{$key}\n";
        }
}

open(OUT,">kmer_temp.txt") || die("Can't create a temporary file during kmer selection\n");
print OUT "$list";
close(OUT);
system("cut -f 1 kmer_temp.txt > kmer_temp2.txt");
system("rm kmer_temp.txt");
system("rm kmer_temp2.txt");

@splitty = split(/>/,$list);

$first = 0;
@part1 = split(/\n/,$splitty[1]);
foreach $LINE (@part1)
{
	if($LINE !~ /^m/){ next; }
	#print "$LINE\n";
	$m_para = ($LINE =~ m/^m(.*?)t/)[0];
	$t_para = ($LINE =~ m/^m.*?t(.*?)\t/)[0];
	if($first == 0)
	{
		$first = 1;
		$m_save = $m_para;
		$t_save = $t_para;
		next;
	}
	if($m_para eq $m_save){ next; }
	if($m_para ne $m_save)
	{
		$part1 .= "$m_save\t$t_save\n";
		$m_save = $m_para;
		$t_save = $t_para;
		next;
	}
}
if($m_save =~ m/^\d/)
{
	$part1 .= "$m_save\t$t_save\n";
}

$first = 0;
$m_save = "";
$t_save = "";
@part2 = split(/\n/,$splitty[2]);
foreach $LINE (@part2)
{
        if($LINE !~ /^m/){ next; }
        $m_para = ($LINE =~ m/^m(.*?)t/)[0];
        $t_para = ($LINE =~ m/^m.*?t(.*?)\t/)[0];
        if($first == 0)
        {
                $first = 1;
                $m_save = $m_para;
                $t_save = $t_para;
                next;
        }
        if($m_para eq $m_save){ next; }
        if($m_para ne $m_save)
        {
                $part2 .= "$m_save\t$t_save\n";
                $m_save = $m_para;
                $t_save = $t_para;
                next;
        }
}
if($m_save =~ m/^\d/)
{
        $part2 .= "$m_save\t$t_save\n";
}

$first = 0;
$m_save = "";
$t_save = "";
@part3 = split(/\n/,$splitty[3]);
foreach $LINE (@part3)
{
        if($LINE !~ /^m/){ next; }
        $m_para = ($LINE =~ m/^m(.*?)t/)[0];
        $t_para = ($LINE =~ m/^m.*?t(.*?)\t/)[0];
        if($first == 0)
        {
                $first = 1;
                $m_save = $m_para;
                $t_save = $t_para;
                next;
        }
        if($m_para eq $m_save){ next; }
        if($m_para ne $m_save)
        {
                $part3 .= "$m_save\t$t_save\n";
                $m_save = $m_para;
                $t_save = $t_para;
                next;
        }
}
if($m_save =~ m/^\d/)
{
        $part3 .= "$m_save\t$t_save\n";
}

$first = 0;
$m_save = "";
$t_save = "";
@part4 = split(/\n/,$splitty[4]);
foreach $LINE (@part4)
{
        if($LINE !~ /^m/){ next; }
        $m_para = ($LINE =~ m/^m(.*?)t/)[0];
        $t_para = ($LINE =~ m/^m.*?t(.*?)\t/)[0];
        if($first == 0)
        {
                $first = 1;
                $m_save = $m_para;
                $t_save = $t_para;
                next;
        }
        if($m_para eq $m_save){ next; }
        if($m_para ne $m_save)
        {
                $part4 .= "$m_save\t$t_save\n";
                $m_save = $m_para;
                $t_save = $t_para;
                next;
        }
}
if($m_save =~ m/^\d/)
{
        $part4 .= "$m_save\t$t_save\n";
}

$first = 0;
$m_save = "";
$t_save = "";
@part5 = split(/\n/,$splitty[5]);
foreach $LINE (@part5)
{
        if($LINE !~ /^m/){ next; }
        $m_para = ($LINE =~ m/^m(.*?)t/)[0];
        $t_para = ($LINE =~ m/^m.*?t(.*?)\t/)[0];
        if($first == 0)
        {
                $first = 1;
                $m_save = $m_para;
                $t_save = $t_para;
                next;
        }
        if($m_para eq $m_save){ next; }
        if($m_para ne $m_save)
        {
                $part5 .= "$m_save\t$t_save\n";
                $m_save = $m_para;
                $t_save = $t_para;
                next;
        }
}
if($m_save =~ m/^\d/)
{
        $part5 .= "$m_save\t$t_save\n";
}

$first = 0;
$m_save = "";
$t_save = "";
@part6 = split(/\n/,$splitty[6]);
foreach $LINE (@part6)
{
        if($LINE !~ /^m/){ next; }
        $m_para = ($LINE =~ m/^m(.*?)t/)[0];
        $t_para = ($LINE =~ m/^m.*?t(.*?)\t/)[0];
        if($first == 0)
        {
                $first = 1;
                $m_save = $m_para;
                $t_save = $t_para;
                next;
        }
        if($m_para eq $m_save){ next; }
        if($m_para ne $m_save)
        {
                $part6 .= "$m_save\t$t_save\n";
                $m_save = $m_para;
                $t_save = $t_para;
                next;
        }
}
if($m_save =~ m/^\d/)
{
        $part6 .= "$m_save\t$t_save\n";
}

#print "$part1\n\n$part2\n\n$part3\n\n$part4\n\n$part5\n\n$part6\n\n";
#print "\n\n$list\n";


$output .= (split /\n/, $part1)[0];
$output .= "\n";
$output .= (split /\n/, $part1)[1];
$output .= "\n";
$output .= (split /\n/, $part2)[0];
$output .= "\n";
$output .= (split /\n/, $part2)[1];
$output .= "\n";
$output .= (split /\n/, $part3)[0];
$output .= "\n";
$output .= (split /\n/, $part3)[1];

open(OUT,">kmer_temp3.txt") || die("Can't make a temporary kmer file\n");
print OUT $output;
close(OUT);
system("sed -i '/^\$/d' kmer_temp3.txt");
$sorted_output = `sort kmer_temp3.txt | uniq`;
#print "$sorted_output";
$kmer_count = () = $sorted_output =~ /\n/gi;
#print "$kmer_count\n\n";

$shifter = -1;
$number_of_kmers = $ARGV[1];
while($kmer_count <= $number_of_kmers)
{
	$shifter++;
	$selection = (split /\n/, $part4)[$shifter];
	if($sorted_output !~ m/$selection/)
	{
		$sorted_output .= (split /\n/, $part4)[$shifter];
		$kmer_count++;
		if($kmer_count >= $number_of_kmers ){ last; }
		$sorted_output .= "\n";
	}
	$selection = (split /\n/, $part5)[$shifter];
        if($sorted_output !~ m/$selection/)
        {
                $sorted_output .= (split /\n/, $part5)[$shifter];
                $kmer_count++;
                if($kmer_count >= $number_of_kmers ){ last; }
                $sorted_output .= "\n";
        }
	$selection = (split /\n/, $part6)[$shifter];
        if($sorted_output !~ m/$selection/)
        {
                $sorted_output .= (split /\n/, $part6)[$shifter];
                $kmer_count++;
                if($kmer_count >= $number_of_kmers ){ last; }
                $sorted_output .= "\n";
        }
}
system("rm kmer_temp3.txt");
$sorted_output =~ s/\t/-/g;
$sorted_output =~ s/\n/,/g;
$sorted_output .= "\n";
print STDOUT "The following were the best kmer/trimming combinations found for use with Multi-SSAKE within VirGA\n\n";
print STDOUT "Number of kmer/trimming combinations requested: $number_of_kmers\n";
print STDOUT "$sorted_output\n\n";
open(OUT,">ssake_kmer_selection.txt") || die("Can't write the kmer output\n");
print OUT "The following were the best kmer/trimming combinations found for use with Multi-SSAKE within VirGA\n\n";
print OUT "Number of kmer/trimming combinations requested: $number_of_kmers\n";
print OUT "$sorted_output\n\n";
close(OUT);
