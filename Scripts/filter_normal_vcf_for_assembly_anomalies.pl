#!/usr/bin/perl -w

if($ARGV[0] =~ /\//){ $vcf_base = ($ARGV[0] =~ m/.*\/(.*)/)[0]; }
else{ $vcf_base = $ARGV[0]; }

open(OUTPUT,">>false_SNPS_that_were_corrected.vcf") || die("Can't open the file for writing...\n");
open(VCF,$ARGV[0]) || die("Can't open the VCF file\n");
while(<VCF>)
{
	$_ =~ s/\n//;
	if($_ =~ /^#/){ next; }
	if($_ =~ /INDEL/){ next; }
	@splitter = split(/\t/,$_);
	if($splitter[4] =~ /,/){ next; }
	$DP4_section = ($splitter[7] =~ m/DP4=(.*?);/)[0];
	#print STDERR "$DP4_section\n";
	@DP4 = split(/,/,$DP4_section);
	if((($DP4[2] + $DP4[3]) / ($DP4[0] + $DP4[1] + $DP4[2] + $DP4[3])) > 0.5)
	{
		#print STDERR "False SNP!\n\n";
		print OUTPUT "$_\n";
		push(@ERRORS,$_);
	}
	else
	{
		#print STDERR "SNP\n\n";
	}
}
close(VCF);
close(OUTPUT);

open(FASTA,$ARGV[1]) || die("Can't open the reference genome\n");
while(<FASTA>)
{
	$_ =~ s/\n//;
	if($_ =~ /^>/)
	{
		$header = $_;
		next;
	}
	if($_ !~ /^>/)
	{
		$value .= $_;
	}
}
close(FASTA);

foreach $LINE (@ERRORS)
{
	#print "HERE: $LINE\n";
	@splitty = split(/\t/,$LINE);
	$pos = $splitty[1];
	$ref = $splitty[3];
	$alt = $splitty[4];
	$sub = substr($value,$pos-1,1);
	if($sub ne $ref)
	{
		print STDERR "ERROR: ref bases don't match\n";
		exit 1;
	}
	#print "Before: Substring from ref: $sub\n";
	substr($value,$pos-1,1,$alt);
	$sub = substr($value,$pos-1,1);
	if($sub ne $alt)
	{
		print STDERR "ERROR: new ref base doesn't match the alt base\n";
		exit 1;
	}
	#print "After:  Substring from ref: $sub\n\n";
}
if($ARGV[1] =~ /\//){ $out_base = ($ARGV[1] =~ m/.*\/(.*)/)[0]; }
else{ $out_base = $ARGV[1]; }
open(RESULT,">>SNP_error_corrected_$out_base") || die("Can't open the output genome file for writing\n");
print RESULT "$header\n$value\n";
close(RESULT);
