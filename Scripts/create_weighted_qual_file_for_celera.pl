#!/usr/bin/perl -w

$min = 10;
if(exists $ARGV[1]){ $min = $ARGV[1]; }

$max = 40;
if(exists $ARGV[2]){ $max = $ARGV[2]; }

$percent_trim = 0.10;
if(exists $ARGV[3]){ $percent_trim = $ARGV[3]; }


$diff = $max - $min;
$index = -1;
open(FASTA,$ARGV[0]) || die("Can't open the fasta file\n");
while(<FASTA>)
{
	$_ =~ s/\n//;
	if($_ =~ /^>/)
	{
		$index++;
		$names[$index] = $_;
	}
	if($_ !~ /^>/)
	{
		$seqs[$index] .= $_;
	}
}
close(FASTA);

$index = -1;
foreach $value (@seqs)
{
	$index++;
	$temp = "";
	$total_length = length($value);
	$start_section = $total_length*$percent_trim;
	$end_section = $total_length - ($total_length*$percent_trim);
	$fresh_min = $min;
	$fresh_max = $max;
	$end_count = 1;
        for($i=0; $i<length($value); $i++)
        {
		
		if($i <= $start_section)
		{
			$increase = int(($i/$start_section)*$diff);
			$fresh_min = $min + $increase;
			$temp .= "$fresh_min ";
		}

		if($i > $start_section and $i < $end_section)
		{
			$temp .= "$max ";
			$end_count = 0;
		}

		if($i >= $end_section)
		{
			$end_count++;
			$decrease = int(($end_count/($start_section-1))*$diff);
			$fresh_max = $max - $decrease;
			if($fresh_max < $min){ $fresh_max = $min; };
			$temp .= "$fresh_max ";
		}

        }


        $temp =~ s/ $//;
        print "$names[$index]\n$temp\n";
}
