#!/usr/bin/perl -w

$folder_date = `date +%Y-%m-%d`;
$folder_date =~ s/\n//;
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}");
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Alignments");
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Alignments/ORF");
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Alignments/Gene");
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Alignments/AA");
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Alignments/Other");
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Reference");
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome");
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Variants");
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC");
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/Before");
system("mkdir $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/After");
system("cp $ARGV[1]/VirGA_parameters.ini $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/VirGA_parameters.txt");
system("cp $ARGV[1]/STEP_4--Assembly_Assessment/*.png $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome");
system("cp $ARGV[1]/STEP_4--Assembly_Assessment/Bowtie2/gene_temp_files/AA/*.aln $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Alignments/AA");
system("cp $ARGV[1]/STEP_4--Assembly_Assessment/Bowtie2/gene_temp_files/ORF/*.aln $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Alignments/ORF");
system("cp $ARGV[1]/STEP_4--Assembly_Assessment/Bowtie2/gene_temp_files/Gene/*.aln $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Alignments/Gene");
system("cp $ARGV[1]/STEP_4--Assembly_Assessment/Bowtie2/gene_temp_files/Other/*.aln $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Alignments/Other");
system("cp $ARGV[1]/x_references/* $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Reference");
system("cp $ARGV[1]/STEP_4--Assembly_Assessment/*.fa $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome");
system("cp $ARGV[1]/STEP_4--Assembly_Assessment/*.fa.fai $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome");
system("cp $ARGV[1]/STEP_4--Assembly_Assessment/*.gff $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome");
system("cp $ARGV[1]/STEP_4--Assembly_Assessment/*.vcf $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Variants");
system("cp -r $ARGV[1]/STEP_1--Preprocessing/before_metrics/*before/* $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/Before");
system("rm $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/Before/*.zip");
system("cp -r $ARGV[1]/STEP_1--Preprocessing/after_metrics/*after/* $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/After");
system("rm $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/After/*.zip");
system("cp -r $ARGV[1]/STEP_1--Preprocessing/before_metrics/*.png $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/Before");
system("cp -r $ARGV[1]/STEP_1--Preprocessing/after_metrics/*.png $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/After");
system("cp $ARGV[1]/STEP_4--Assembly_Assessment/low_coverage.gff $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome");

$ref_gtf=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Reference | grep -P ".g(t|f)f"`;
$ref_gtf =~ s/\n//;
$ref_fa=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Reference | grep ".fa"`;
$ref_fa =~ s/\n//;
#$ref_base = ( $ref_fa =~ m/^(.*?)\.fa/)[0];
#system("perl $ARGV[1]/x_scripts/extract_CDS_features_from_fasta.pl $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Reference/$ref_fa $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Reference/$ref_gtf > $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Reference/$ref_base.nucl");
#system("perl $ARGV[1]/x_scripts/translate_fasta_CDS_to_protein.pl $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Reference/$ref_base.nucl > $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Reference/$ref_base.prot");

$sample_gtf=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome | grep -v "low_coverage.gff" | grep -v "no_coverage.gff" | grep -v "full-length" | grep -P ".g(t|f)f"`;
$sample_gtf =~ s/\n//;
$sample_fa=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome | grep -Pv ".fa.fai" | grep -v "full-length" | grep ".fa"`;
$sample_fa =~ s/\n//;
#$sample_base = ( $sample_fa =~ m/^(.*?)\.fa/)[0];
#system("perl $ARGV[1]/x_scripts/create_gff_of_Ns_from_genome.pl $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome/${sample_fa} > $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome/no_coverage.gff");
#system("perl $ARGV[1]/x_scripts/extract_CDS_features_from_fasta.pl $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome/$sample_fa $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome/$sample_gtf > $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome/$sample_base.nucl");
#system("perl $ARGV[1]/x_scripts/translate_fasta_CDS_to_protein.pl $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome/$sample_base.nucl > $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome/$sample_base.prot");


open(REPORT,">>$ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/VirGA_Report.html") || die("Can't open the output report for writing\n");
system("ln -s $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/VirGA_Report.html $ARGV[1]/VirGA_Report.html");
#exit 0;
open(VAR,$ARGV[0]) || die("Can't open the parameters file\n");
while(<VAR>)
{
	$var .= $_;
}
close(VAR);

$the_date=`date`;

#if($var =~ /include_TRL_TRS="yes"/)
#{
#	system("perl $ARGV[1]/x_scripts/convert_trimmed_genome_to_full_genome.pl $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome/$sample_fa $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome/$sample_gtf");
#	system("mv full-length* $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome");
#}

$genome_name = ($var =~ m/final_genome_name="(.*?)"/)[0];

print REPORT "<!DOCTYPE html>\n
<html>\n
<head>\n
<title>VirGA Report</title>\n
</head>\n
<body>\n
\n
<font size=6><b>===VirGA ${genome_name} Report Summary===</b></font><br>\n
\n
<font size=3>Date generated: $the_date</font><br><br>\n";

print REPORT "<table border=1>";
print REPORT "<tr><td>Genome name </td><td><a href=\"Assembled_genome/$sample_fa\">$sample_fa</a></tr>\n";
print REPORT "<tr><td>Genome annotation </td><td><a href=\"Assembled_genome/$sample_gtf\">$sample_gtf</a></tr>\n";
$genome_length = `grep -v ">" $ARGV[1]/STEP_4--Assembly_Assessment/$genome_name.fa`;
$genome_length =~ s/\n//g;
print REPORT "<tr><td>Genome length</td><td> ".(length($genome_length))." bp</td></tr>\n";

if($var =~ /include_TRL_TRS="yes"/)
{
	$long_genome_name = ($var =~ m/final_genome_name="(.*?)"/)[0];
	$long_genome_name = "full-length_${long_genome_name}";
	print REPORT "<tr><td>Genome name (with TRL/TRS)</td><td><a href=\"Assembled_genome/full-length_${sample_fa}\">full-length_${sample_fa}</a></tr>\n";
	print REPORT "<tr><td>Genome annotation (with TRL/TRS)</td><td><a href=\"Assembled_genome/full-length_${sample_gtf}\">full-length_${sample_gtf}</a></tr>\n";
	$long_genome_length = `grep -v ">" $ARGV[1]/STEP_4--Assembly_Assessment/full-length_${genome_name}.fa`;
	$long_genome_length =~ s/\n//g;
	print REPORT "<tr><td>Genome length (with TRL/TRS)</td><td> ".(length($long_genome_length))." bp</td></tr>\n";
}

$pileup_100 = `cut -f 4 $ARGV[1]/STEP_4--Assembly_Assessment/Bowtie2/corrected.pileup | sort -nr | grep -P "^\\d\\d\\d" | wc -l`;
$pileup_total = `cut -f 4 $ARGV[1]/STEP_4--Assembly_Assessment/Bowtie2/corrected.pileup | wc -l`;
$pileup_percent_temp = 100*($pileup_100/$pileup_total);
$pileup_percent = ($pileup_percent_temp =~ m/^(\d+\.\d\d)/)[0];
if($pileup_100 == 0){ $pileup_percent = 0; }
print REPORT "<tr><td>Number of bases with >=100 depth support&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td> $pileup_100 ($pileup_percent%)</td></tr>\n";
$genome_length_Ns =()= $genome_length =~ /N/gi;
$genome_length_gaps =()= $genome_length =~ /N+/gi;
print REPORT "<tr><td>Number of gaps</td><td> $genome_length_gaps</td></tr><tr><td>Number of Ns</td><td> $genome_length_Ns</td></tr>\n";

if($var =~ /analyze_genes="yes"/)
{
        $exon_report=`cat $ARGV[1]/STEP_4--Assembly_Assessment/feature_report.txt`;
        $exon_miss=`grep -vc "No errors" $ARGV[1]/STEP_4--Assembly_Assessment/feature_report.txt`;
        $exon_hits=`grep -c "No errors" $ARGV[1]/STEP_4--Assembly_Assessment/feature_report.txt`;
        print REPORT "<tr><td>Complete protein annotations</td><td> $exon_hits</td></tr><tr><td>Incomplete protein annotations</td><td> $exon_miss</td></tr>\n";
}
print REPORT "<tr><td>Reference name </td><td><a href=\"Reference/$ref_fa\">$ref_fa</a></tr>\n";
print REPORT "<tr><td>Reference annotation </td><td><a href=\"Reference/$ref_gtf\">$ref_gtf</a></tr>\n";
print REPORT "</table>";

if($var =~/create_coverage_graphic="yes"/)
{
	print REPORT "<br><br><b>Coverage plot demonstrating read depth across the new genome</b><br>\n";
	print REPORT "<font size=2>All annotations from the .gff file have been added as separate tracks, as well as low coverage and gap regions. (click to enlarge)</font><br>\n";
	print REPORT "<a href=\"Assembled_genome/coverage_graphic_${genome_name}.png\">";
	print REPORT "<img border=\"0\" src=\"Assembled_genome/coverage_graphic_${genome_name}.png\" alt=\"Coverage Graphic\" width=\"720\" height=\"130\"></a><br>\n";
}

if($var =~ /analyze_genes="yes"/)
        {
                print REPORT "<br><b><font size=3>The following proteins were correctly assembled and annotated from the $genome_name genome</font></b><br>\n";
		print REPORT "<font size=2>(Click on the protein names to see their alignment to the reference genome)</font><br>\n";
                $exon_report=`cat $ARGV[1]/STEP_4--Assembly_Assessment/feature_report.txt`;
                $exon_miss=`grep -vc "No problems detected" $ARGV[1]/STEP_4--Assembly_Assessment/feature_report.txt`;
                $exon_hits=`grep -c "No problems detected" $ARGV[1]/STEP_4--Assembly_Assessment/feature_report.txt`;
                $exon_report =~ s/\n/<br>/g;
                open(EXONTEXT,"$ARGV[1]/STEP_4--Assembly_Assessment/feature_report.txt") || die("Can't open the feature_report.txt\n");
		$counter = -1;
		print REPORT "<table border=0><tr>";
                while(<EXONTEXT>)
                {
			$counter++;
			if($counter == 3)
			{
				print REPORT "</tr><tr>";
				$counter = 0;
			}
                        if($_ =~ /No errors/)
                        {
                                $exon_current_name = ($_ =~ m/^(.*?):/)[0];
                                $exon_current_status = ($_ =~ m/^(.*?):(.*)/)[1];
                                print REPORT "<td><font size=2>${exon_current_name}: <a href=\"Alignments/Gene/${exon_current_name}.aln\">Gene</a>|<a href=\"Alignments/ORF/${exon_current_name}.aln\">ORF</a>|<a href=\"Alignments/AA/${exon_current_name}.aln\">AA</a></font></td><td><font color=\"green\" size=2>&nbsp;&nbsp;$exon_current_status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color=\"black\"></td>";
                        }
                        if($_ !~ /No errors/)
                        {
                                $exon_current_name = ($_ =~ m/^(.*?):/)[0];
                                $exon_current_status = ($_ =~ m/^(.*?):(.*)/)[1];
                                $exon_failed .= "<tr><td><font size=2>${exon_current_name}: <a href=\"Alignments/Gene/${exon_current_name}.aln\">Gene</a>|<a href=\"Alignments/ORF/${exon_current_name}.aln\">ORF</a>|<a href=\"Alignments/AA/${exon_current_name}.aln\">AA</a></font></td><td><font color=\"red\" size=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$exon_current_status</font></td></tr>";
				$counter--;
                        }
			$exon_list{$exon_current_name} = 1;
                }
                close(EXONTEXT);
		print REPORT "</tr></table>";
		print REPORT "<br><font size=3><b>Proteins requiring review due to potential assembly defects or substantial biological variation:</b></font><br>\n";
		print REPORT "<table border=0>$exon_failed</table>\n";
                #print REPORT "Complete exon annotations: $exon_hits<br>Incomplete exon annotations: $exon_miss<br>\n";
                #print REPORT "<br><font size=\"2\">$exon_report_formatted</font><br>\n";
                print REPORT "<br><b><font size=3>The following genome features are non-protein coding and were aligned to the reference for visual comparison</font></b><br>\n";
		print REPORT "<font size=2>(Click the feature names to see their alignment to the reference. Only features <= 5kb were aligned.)</font><br>\n";
		print REPORT "<table border=0><tr>";
		
		open(SLIST,"$ARGV[1]/STEP_4--Assembly_Assessment/Bowtie2/gene_temp_files/Other/genome.gff") || die("Can't open the genome.gff file to find short genes\n");
		while(<SLIST>)
		{
			$_ =~ s/\n//;
			@short_split = split(/\t/,$_);
			$short_name = ($_ =~ m/ID=(.*)/)[0];
			if(($short_split[4] - $short_split[3]) <= 5000)
			{
				$short_LIST{$short_name} = 1;
			}
		}
		close(SLIST);

		$new_gff=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/Assembled_genome | grep -v "low_coverage.gff" | grep -v "no_coverage.gff" | grep -v "full-length" | grep -P ".g(t|f)f"`;
		open(NEWGFF,"$ARGV[1]/STEP_4--Assembly_Assessment/$new_gff") || die("Can't open the gff file for 2nd and 3rd table construction\n");
		$counter = -1;
		while(<NEWGFF>)
		{
			$counter++;
			if($counter == 3)
			{
				print REPORT "</tr><tr>";
				$counter = 0;
			}
			$_ =~ s/\n//;
			@split_newgff = split(/\t/,$_);
			if($split_newgff[2] eq "exon"){ $counter--; next; }
			$current_newgff_name = ($split_newgff[8] =~ m/ID=(.*)/)[0];
			if(exists $exon_list{$current_newgff_name}){ $counter--; next; }
			if(exists $short_LIST{$current_newgff_name})
			{
				print REPORT "<td><font size=2>${current_newgff_name}: <a href=\"Alignments/Other/${current_newgff_name}.aln\">DNA</a></font></td>";
			}
			if(not exists $short_LIST{$current_newgff_name})
			{
				print REPORT "<td><font size=2>${current_newgff_name}: None</font></td>";
			}
		}
		close(NEWGFF);
		print REPORT "</tr></table>\n";
        }




print REPORT "<br><br><font size=6><b>===VirGA Detailed Report===</b></font><br>";

print REPORT "<font color=\"black\">";
print REPORT "<h4>VirGA steps completed:<h4>\n<font size=\"2\">\n";

if($var =~ /run_step1="yes"/)
{
	print REPORT "&nbsp&nbsp&nbsp- STEP_1:Preprocessing<br>\n";
}
if($var =~ /run_step2="yes"/)
{
        print REPORT "&nbsp&nbsp&nbsp- STEP_2:Multi_SSAKE_Assembly<br>\n";
}
if($var =~ /run_step3="yes"/)
{
        print REPORT "&nbsp&nbsp&nbsp- STEP_3:Annotation<br>\n";
}
if($var =~ /run_step4="yes"/)
{
        print REPORT "&nbsp&nbsp&nbsp- STEP_4:Assembly_Assessment<br>\n";
}

print REPORT "</font>\n";

if($var =~ /run_step1="yes"/)
{
	$STEP_1e=`cat $ARGV[1]/x_job_files/STEP_1.sub.e*`;
	$STEP_1o=`cat $ARGV[1]/x_job_files/STEP_1.sub.o*`;
	if($var =~ /PBS_use="no"/)
	{
		$STEP_1e=`cat $ARGV[1]/run_manually.log`;
		$STEP_1o=`cat $ARGV[1]/run_manually.log`;
	}
	$STEP_1e =~ s/\n/<br>/g;
	$STEP_1o =~ s/\n/<br>/g;
	$raw_read_1=`ls $ARGV[1]/x_input | grep "1.fastq"`;
	$raw_read_2=`ls $ARGV[1]/x_input | grep "2.fastq"`;
	$raw_read_1_num_temp=`wc -l $ARGV[1]/x_input/$raw_read_1`;
	$raw_read_2_num_temp=`wc -l $ARGV[1]/x_input/$raw_read_2`;
	$raw_read_1_num_temp2 = ($raw_read_1_num_temp =~ m/^(.*?) /)[0];
	$raw_read_2_num_temp2 = ($raw_read_2_num_temp =~ m/^(.*?) /)[0];
	$raw_read_1_num = $raw_read_1_num_temp2/4;
	$raw_read_2_num = $raw_read_2_num_temp2/4;
        print REPORT "<br><h2>STEP_1:Preprocessing Output</h2><br>\n";
	print REPORT "Raw reads within sample1 ($raw_read_1): $raw_read_1_num<br>\n";
	print REPORT "Raw reads within sample2 ($raw_read_2): $raw_read_2_num<br>\n"; 
	
	if($var =~ /before_stats="yes"/)
	{
		print REPORT "<br><b>Procedure: 'before_stats'</b>\n";
		print REPORT "<br><b>Purpose: Create histograms of raw read quality and other metrics</b><br>\n";
		$before_pic_1=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/Before | grep "1.fastq_boxplot.png"`;
		$before_pic_2=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/Before | grep "2.fastq_boxplot.png"`;
		$before_fastq_1=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/Before/*1_fastqc | grep \".html\"`;
		$before_fastq_1_path=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/Before | grep \"1_fastqc\"`;
		$before_fastq_2=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/Before/*2_fastqc | grep \".html\"`;
		$before_fastq_2_path=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/Before | grep \"2_fastqc\"`;
		print REPORT "<a href=\"QC/Before/$before_pic_1\">";
		print REPORT "<img border=\"0\" src=\"QC/Before/$before_pic_1\" alt=\"Raw_reads_1\" width=\"720\" height=\"130\"></a>\n";
		print REPORT "<br><a href=\"QC/Before/$before_pic_2\">";
		print REPORT "<img border=\"0\" src=\"QC/Before/$before_pic_2\" alt=\"Raw_reads_1\" width=\"720\" height=\"130\"></a>\n";
		print REPORT "<br>FastQC Results for $raw_read_1: <a href=\"QC/Before/$before_fastq_1_path/$before_fastq_1\">FastQC-Report</a>\n";
		print REPORT "<br>FastQC Results for $raw_read_2: <a href=\"QC/Before/$before_fastq_2_path/$before_fastq_2\">FastQC-Report</a>\n";
	}

	if($var =~ /clip_adapters="yes"/)
	{
		print REPORT "<br><br><b>Procedure: 'fastx_clip_adapters'</b>\n";
		print REPORT "<br><b>Purpose: Clip leftover adapters from the raw sequencing reads</b><br>\n";
		$clip = ($STEP_1o =~ m/#~# clip_adapters #~#(.*?)#~# end clip_adapters #~#/)[0];
		print REPORT "<br>$clip<br>\n";
	}

	if($var =~ /trim_reads="yes"/)
        {
                print REPORT "<br><br><b>Procedure: 'fastx_trim_qual'</b>\n";
                print REPORT "<br><b>Purpose: Trim low quality bases from sequencing reads</b><br>\n";
		$trim = ($STEP_1e =~ m/#~# trim_reads #~#(.*?)#~# end trim_reads #~#/)[0];
                print REPORT "<br>$trim<br>\n";
        }

	if($var =~ /filter_artifacts="yes"/)
        {
                print REPORT "<br><br><b>Procedure: 'fastx_remove_artifacts'</b>\n";
                print REPORT "<br><b>Purpose: Remove sequencing artifacts from the read pool</b><br>\n";
		$artifacts = ($STEP_1o =~ m/#~# filter_artifacts #~#(.*?)#~# end filter_artifacts #~#/)[0];
                print REPORT "<br>$artifacts<br>\n";
        }

	if($var =~ /filter_contaminants="yes"/)
        {
                print REPORT "<br><br><b>Procedure: 'filter_contaminants'</b>\n";
                print REPORT "<br><b>Purpose: Remove reads that are derived from a contaminating source, e.g. host cells</b><br>\n";
		$contaminants = ($STEP_1e =~ m/#~# filter_contaminants #~#(.*?)#~# end filter_contaminants #~#/)[0];
                print REPORT "<br>$contaminants<br>\n";
}

	if($var =~ /only_save_paired="yes"/)
        {
                print REPORT "<br><br><b>Procedure: 'only_save_paired'</b>\n";
                print REPORT "<br><b>Purpose: Only save reads that are properly paired</b><br>\n";
		$paired = ($STEP_1e =~ m/#~# only_save_paired #~#(.*?)#~# end only_save_paired #~#/)[0];
                print REPORT "<br>$paired<br>\n";
        }

	if($var =~ /after_stats="yes"/)
        {
                print REPORT "<br><b>Procedure: 'after_stats'</b>\n";
                print REPORT "<br><b>Purpose: Create histograms of raw read quality and other metrics</b><br>\n";
                $after_pic_1=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/After | grep "1.fastq_boxplot.png"`;
                $after_pic_2=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/After | grep "2.fastq_boxplot.png"`;
                $after_fastq_1=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/After/*1_fastqc | grep \".html\"`;
                $after_fastq_1_path=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/After | grep \"1_fastqc\"`;
                $after_fastq_2=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/After/*2_fastqc | grep \".html\"`;
                $after_fastq_2_path=`ls $ARGV[1]/VirGA_Report_${ARGV[2]}_${folder_date}/QC/After | grep \"2_fastqc\"`;
                print REPORT "<a href=\"QC/After/$after_pic_1\">";
                print REPORT "<img border=\"0\" src=\"QC/After/$after_pic_1\" alt=\"Raw_reads_1\" width=\"720\" height=\"130\"></a>\n";
                print REPORT "<br><a href=\"QC/After/$after_pic_2\">";
                print REPORT "<img border=\"0\" src=\"QC/After/$after_pic_2\" alt=\"Raw_reads_1\" width=\"720\" height=\"130\"></a>\n";
                print REPORT "<br>FastQC Results for $raw_read_1: <a href=\"QC/After/$after_fastq_1_path/$after_fastq_1\">FastQC-Report</a>\n";
                print REPORT "<br>FastQC Results for $raw_read_2: <a href=\"QC/After/$after_fastq_2_path/$after_fastq_2\">FastQC-Report</a>\n";



		#$after_pic_1=`ls $ARGV[1]/STEP_1--Preprocessing/after_metrics/ | grep "1.fastq_boxplot.png"`;
                #$after_pic_2=`ls $ARGV[1]/STEP_1--Preprocessing/after_metrics/ | grep "2.fastq_boxplot.png"`;
                #$raw_read_1_base = ($raw_read_1 =~ m/^(.*?)\.fastq/)[0];
                #$raw_read_2_base = ($raw_read_2 =~ m/^(.*?)\.fastq/)[0];
		#$fastqc_after_1=`find $ARGV[1] -name "*.html*" | grep "1_fastqc/fastqc_report.html" | grep "after"`;
		#$fastqc_after_2=`find $ARGV[1] -name "*.html*" | grep "2_fastqc/fastqc_report.html" | grep "after"`;
                #print REPORT "<a href=\"$ARGV[1]/STEP_1--Preprocessing/after_metrics/$after_pic_1\">";
                #print REPORT "<img border=\"0\" src=\"$ARGV[1]/STEP_1--Preprocessing/after_metrics/$after_pic_1\" alt=\"Raw_reads_1\" width=\"720\" height=\"130\"></a>\n";
                #print REPORT "<br><a href=\"$ARGV[1]/STEP_1--Preprocessing/after_metrics/$after_pic_2\">";
                #print REPORT "<img border=\"0\" src=\"$ARGV[1]/STEP_1--Preprocessing/after_metrics/$after_pic_2\" alt=\"Raw_reads_1\" width=\"720\" height=\"130\"></a>\n";
                #print REPORT "<br>FastQC Results for $raw_read_1: <a href=\"$fastqc_after_1\">FastQC-Report</a>\n";
                #print REPORT "<br>FastQC Results for $raw_read_2: <a href=\"$fastqc_after_2\">FastQC-Report</a>\n";
        }
	
#Properly paired reads: 


}

if($var =~ /run_step2="yes"/)
{
        print REPORT "<br><br><h2>STEP_2:Multi_SSAKE_Assembly</h2><br>\n";

	if($var =~ /use_multi_ssake="yes"/)
        {
                print REPORT "<b>Procedure: 'multi_ssake'</b>\n";
                print REPORT "<br><b>Purpose: Iteratively run SSAKE de bruijn graph generations to assemble rough contigs</b><br>\n";
                $rough_contigs_report=`perl $ARGV[1]/x_scripts/check_assembly_for_quality_metrics.pl $ARGV[1]/STEP_2--Multi_SSAKE_Assembly/rough_contigs.fa`;
		$rough_contigs_report =~ s/\n/<br>/g;
		print REPORT "<br>$rough_contigs_report\n";
        }

	if($var =~ /use_celera="yes"/)
        {
                print REPORT "<br><br><b>Procedure: 'use_celera'</b>\n";
                print REPORT "<br><b>Purpose: Use celera to assemble rough contigs into high quality contigs</b><br>\n";
                $clean_contigs_report=`perl $ARGV[1]/x_scripts/check_assembly_for_quality_metrics.pl $ARGV[1]/STEP_2--Multi_SSAKE_Assembly/clean_contigs.fa`;
                $clean_contigs_report =~ s/\n/<br>/g;
                print REPORT "<br>$clean_contigs_report\n";
        }

	if($var =~ /use_gap4="yes"/)
        {
                print REPORT "<br><br><b>Procedure: 'use_gap4'</b>\n";
                print REPORT "<br><b>Purpose: Use gap4 to assemble rough contigs into high quality contigs</b><br>\n";
                $clean_contigs_report=`perl $ARGV[1]/x_scripts/check_assembly_for_quality_metrics.pl $ARGV[1]/STEP_2--Multi_SSAKE_Assembly/clean_contigs.fa`;
                $clean_contigs_report =~ s/\n/<br>/g;
                print REPORT "<br>$clean_contigs_report\n";
        }
}

if($var =~ /run_step3="yes"/)
{
        print REPORT "<br><h2>STEP_3:Annotation</h2><br>\n";
	$STEP_3e=`cat $ARGV[1]/x_job_files/STEP_3.sub.e*`;
        $STEP_3o=`cat $ARGV[1]/x_job_files/STEP_3.sub.o*`;
        if($var =~ /PBS_use="no"/)
        {
                $STEP_3e=`cat $ARGV[1]/run_manually.log`;
                $STEP_3o=`cat $ARGV[1]/run_manually.log`;
        }
	$STEP_3e =~ s/\n/<br>/g;
        $STEP_3o =~ s/\n/<br>/g;

	if($var =~ /use_gapfiller="no"/)
	{
		print REPORT "<b>Procedure: 'mugsy_maf-net_compare_genome'</b>\n";
                print REPORT "<br><b>Purpose: Align quality contigs with reference to produce linearized genome assembly</b><br>\n";
                $linear_genome_stats=`grep -v ">" $ARGV[1]/STEP_3--Linearize_and_Annotate/assembled_genome.fa`;
		$linear_genome_stats =~ s/\n//g;
		$linear_genome_Ns =()= $linear_genome_stats =~ /N/gi;
                print REPORT "<br>Assembled_genome contains $linear_genome_Ns Ns\n";
		print REPORT "<br>Assembled_genome contains ".(length($linear_genome_stats))." total bases\n";
	}

	if($var =~ /use_gapfiller="yes"/)
        {
                print REPORT "<b>Procedure: 'mugsy_maf-net_compare_genome'</b>\n";
                print REPORT "<br><b>Purpose: Align quality contigs with reference to produce linearized genome assembly</b><br>\n";
		$linear_genome_stats=`grep -v ">" $ARGV[1]/STEP_3--Linearize_and_Annotate/assembled_genome_before_gapfiller.fa`;
                $linear_genome_stats =~ s/\n//g;
                $linear_genome_Ns =()= $linear_genome_stats =~ /N/gi;
                print REPORT "<br>Assembled_genome contains $linear_genome_Ns Ns\n";
                print REPORT "<br>Assembled_genome contains ".(length($linear_genome_stats))." total bases\n";


		print REPORT "<br><br><b>Procedure: 'use_gapfiller'</b>\n";
                print REPORT "<br><b>Purpose: Fill gaps in the assembled genome using paired-end read data</b><br>\n";
		$gapfiller_output = `grep "Closed" $ARGV[1]/x_job_files/STEP_3.sub.o*`;
		$gapfiller_output =~ s/\n/<br>/g;
		print REPORT "<br>$gapfiller_output<br>\n";
                

                $linear_genome_stats=`grep -v ">" $ARGV[1]/STEP_3--Linearize_and_Annotate/assembled_genome.fa`;
                $linear_genome_stats =~ s/\n//g;
                $linear_genome_Ns =()= $linear_genome_stats =~ /N/gi;
		print REPORT "<br>Assembled_genome after GapFiller contains $linear_genome_Ns Ns\n";
                print REPORT "<br>Assembled_genome after GapFiller contains ".(length($linear_genome_stats))." total bases<br>\n";
        }



}

if($var =~ /run_step4="yes"/)
{
        print REPORT "<br><h2>STEP_4:Assembly_Assessment</h2><br>\n";
	$STEP_4e=`cat $ARGV[1]/x_job_files/STEP_4.sub.e*`;
        $STEP_4o=`cat $ARGV[1]/x_job_files/STEP_4.sub.o*`;
        if($var =~ /PBS_use="no"/)
        {
                $STEP_4e=`cat $ARGV[1]/run_manually.log`;
                $STEP_4o=`cat $ARGV[1]/run_manually.log`;
        }
	$STEP_4e =~ s/\n/<br>/g;
        $STEP_4o =~ s/\n/<br>/g;

	if($var =~ /bowtie2_map="yes"/)
        {
                print REPORT "<b>Procedure: 'Bowtie2_map'</b>\n";
                print REPORT "<br><b>Purpose: Align reads back to the new genome assembly using Bowtie2</b><br>\n";
                $bowtie_map_output = ($STEP_4e =~ m/#~# first_mapping #~#(.*)#~# end first_mapping #~#/)[0];
		print REPORT "<br>$bowtie_map_output\n";
        }

	if($var =~ /use_corrected_genome="yes"/)
	{
		print REPORT "<b>Procedure: 'Bowtie2_remapping'</b>\n";
                print REPORT "<br><b>Purpose: Align reads back to the SNP genome assembly using Bowtie2</b><br>\n";
                $bowtie_map_output = ($STEP_4e =~ m/#~# second_mapping #~#(.*)#~# end second_mapping #~#/)[0];
                print REPORT "<br>$bowtie_map_output\n";
	}

	if($var =~ /call_variants="yes"/)
        {
                print REPORT "<br><b>Procedure: 'call_variants'</b>\n";
                print REPORT "<br><b>Purpose: Find the variants (INDELS/SNPs) in the newly assembled genomes</b><br>\n";
                $variant_output_INDEL=`cat $ARGV[1]/STEP_4--Assembly_Assessment/Samtools_SNPs.vcf | grep -vP "^##" | grep -c "INDEL"`;
		$variant_output_SNP=`cat $ARGV[1]/STEP_4--Assembly_Assessment/Samtools_SNPs.vcf | grep -vP "^##" | grep -cv "INDEL"`;
                print REPORT "<br>SAMTools indels called: $variant_output_INDEL\n";
		print REPORT "<br>SAMTools SNPs called: $variant_output_SNP\n";
		$free_output_INDEL=`cat $ARGV[1]/STEP_4--Assembly_Assessment/Freebayes_SNPs.vcf | grep -vP "^##" | grep -vc "TYPE=snp"`;
                $free_output_SNP=`cat $ARGV[1]/STEP_4--Assembly_Assessment/Freebayes_SNPs.vcf | grep -vP "^##" | grep -cv "INDEL"`;
                print REPORT "<br>Freebayes indels called: $free_output_INDEL\n";
                print REPORT "<br>Freebayes SNPs called: $free_output_SNP\n";

        }

	if($var =~ /filter_false_SNPs="yes"/)
        {
                print REPORT "<br><br><b>Procedure: 'filter_false_SNPs'</b>\n";
                print REPORT "<br><b>Purpose: Replace misassembled bases from the genome that are found as false SNPs</b><br>\n";
                $number_falses_temp=`wc -l $ARGV[1]/STEP_4--Assembly_Assessment/false_variants_that_were_corrected.vcf`;
		$number_falses = ($number_falses_temp =~ m/^(.*?) /)[0];
                print REPORT "<br>Number of false SNPs corrected in the genome: $number_falses<br>\n";
        }

	if($var =~ /create_low_coverage_gff="yes"/)
        {
                print REPORT "<br><br><b>Procedure: 'create_low_coverage_gff'</b>\n";
                print REPORT "<br><b>Purpose: Generate a .gff file that reports areas of the genome with coverage lower than a threshold</b><br>\n";
		print REPORT "<a href=\"Assembled_genome/low_coverage.gff\">low_coverage.gff</a><br>\n";
		$low_threshold = ($var =~ m/low_coverage_threshold="(.*?)"/)[0];
                print REPORT "<br>Threshold for determining low coverage:  $low_threshold<br>\n";
        }

	if($var =~ /create_no_coverage_gff="yes"/)
        {
                print REPORT "<br><br><b>Procedure: 'create_no_coverage_gff'</b>\n";
                print REPORT "<br><b>Purpose: Generate a .gff file that reports areas of the genome that contain gaps</b><br>\n";
                print REPORT "<a href=\"Assembled_genome/no_coverage.gff\">no_coverage.gff</a><br>\n";
        }
	
}

print REPORT "<br><h1><font color=\"black\">===End Report===</h1><br>\n";
print REPORT "</body></html>";
close(REPORT);
system("zip -rq VirGA_Report_${ARGV[2]}_${folder_date}.zip VirGA_Report_${ARGV[2]}_${folder_date}");
