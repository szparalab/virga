#!/usr/bin/perl -w

open(INPUT,$ARGV[0]) || die("Can't open the input");
while(<INPUT>)
{
	$_ =~ s/\n//;
	@splitter = split(/\t/,$_);
	$HASH{$splitter[0]} = 0;
}
close(INPUT);

$position = 0;
$read = "";
$counter = 0;
open(FASTQ,$ARGV[1]) || die("Can't open the fastq file");
while(<FASTQ>)
{
	$_ =~ s/\n//;
	$position++;
	$read .= "$_\n";
	if($position == 4)
	{
		@check = split(/\n/,$read);
		$check[0] =~ s/^@//;
		$read_ID = ($check[0] =~ m/^(.*?)\s/)[0];
		if(not exists $HASH{$read_ID})
		{
			print "$read";
		}
		else
		{
			$counter++;
		}
		$read = "";
		$position = 0;
	}
}
close(FASTQ);
print STDERR "\nRemoved $counter contaminate reads\n";
