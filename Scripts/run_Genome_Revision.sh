cat>>STEP_4.sub<<eof
cd $1

grab_ref=\`ls $4 | grep -v ".fastq" | grep -c ".fa"\`
if [ \$grab_ref -ne 1 ]; then
	echo -ne "ERROR: single reference file not found, Quitting...\n"
fi
grab_ref=\`ls $4 | grep -v ".fastq" | grep ".fa"\`

grab_ref_gff=\`ls $4 | grep -cP ".g(g|f)f"\`
if [ \$grab_ref_gff -ne 1 ]; then
        echo -ne "ERROR: single reference annotation file not found, Quitting...\n"
fi
grab_ref_gff=\`ls $4 | grep -P ".g(g|f)f"\`
cp $4/\$grab_ref_gff $1

perl $2/clean_multi_fasta.pl $4/\$grab_ref 0 Reference > reference.fa
perl $2/clean_multi_fasta.pl $1/revised_${3}.fa 0 Contigs > contigs.fa
sample_base="$3"
ref_name=\`head -n 1 reference.fa | sed 's/>//'\`
mugsy --directory . --prefix mugsyOutput reference.fa contigs.fa
maf_net.py -r reference -s contigs,reference -c \$ref_name --consensus_sequence mugsyOutput.maf
compare_genomes.py --reference=reference.\${ref_name} --gff_feature_types=exon,gene,genome_segment,miRNA,misc_feature,repeat_region,stem_loop --gff_attributes=ID,Parent,gene,function mugsyOutput.net.afa \$grab_ref_gff
cp contigs.\${ref_name}.consensus.fasta new_contigs.fa
cp compare_genomes_output/contigs.\${ref_name}.gff new_contigs.gff

mkdir Mugsy_MafNet_CompareGenomes
mv * Mugsy_MafNet_CompareGenomes
mv Mugsy_MafNet_CompareGenomes/new_contigs.fa .
mv Mugsy_MafNet_CompareGenomes/new_contigs.gff .


sed -ir "s/^contigs.\${ref_name}\t/\${sample_base}\t/" new_contigs.gff
rm new_contigs.gffr
mv new_contigs.gff \${sample_base}.gff
echo ">\${sample_base}" > \${sample_base}.fa
grep -v ">" new_contigs.fa >> \${sample_base}.fa
rm new_contigs.fa
mv ../* .
mv false_variants_that_were_corrected.vcf ../
mv \${sample_base}.fa ../
mv \${sample_base}.gff ../
eof
