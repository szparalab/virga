cat>>STEP_2c.sub<<EOF
cd $1
cat>specfile.txt<<E2OF
#  The mer overlapper is designed to allow more homopolymer errors.
# overlapper    = mer

#  The bog unitigger is designed to allow more contained reads.
unitigger     = bog

#  Especially important for Titanium reads, allow a bit more error in the overlaps
#  used for forming unitigs.
utgErrorRate  = 0.06
E2OF
cp $3/rough_contigs.fa ssake.fa
perl $2/create_weighted_qual_file_for_celera.pl ssake.fa $4 $5 $6 > ssake.qual
fastaToCA -l SSAKE -s ssake.fa -q ssake.qual > ssake.frg
runCA -d TEMP -p SSAKE -s specfile.txt ssake.frg
cp $1/TEMP/9-terminator/SSAKE.utg.fasta ../clean_contigs.fa
EOF
