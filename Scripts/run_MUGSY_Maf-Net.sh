cat>>STEP_3.sub<<eof
cd $1
perl $6/clean_multi_fasta.pl $2/$3 0 Reference > reference.fa
perl $6/clean_multi_fasta.pl $4/$5 0 Contigs > contigs.fa
sample_base=\`echo $5 | sed -r 's/\..*//'\`
ref_name=\`head -n 1 reference.fa | sed 's/>//'\`
mugsy --directory . --prefix mugsyOutput reference.fa contigs.fa
maf_net.py -r reference -s contigs,reference -c \$ref_name --consensus_sequence mugsyOutput.maf
perl $6/convert_fasta_to_multiple_lines.pl contigs.\${ref_name}.consensus.fasta > ../contigs.fa
if [ $7 != "yes" ]; then
	echo ">\${sample_base}_genome" > ../\${sample_base}_genome.fa
	grep -v ">" ../contigs.fa >> ../\${sample_base}_genome.fa
	rm ../contigs.fa
fi
eof
