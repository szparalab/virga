#!/usr/bin/perl -w

open(FASTA,$ARGV[0])||die("Can't open the input fasta file\n");
while(<FASTA>)
{
	$_ =~ s/\n//;
	if($_ =~ /^>/)
	{
		$header = $_;
	}
	if($_ !~ /^>/)
	{
		$HASH{$header} .= $_;
	}
}
close(FASTA);

while(($key,$value) = each %HASH)
{
	print "$key\n";
	while($value ne "")
	{
		if(length($value) >= 100)
		{
			print substr($value,0,100)."\n";
			substr($value,0,100) = "";
		}
		else
		{
			print substr($value,0,length($value))."\n";
			substr($value,0,length($value)) = "";
		}
	}
}
