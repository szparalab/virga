cat>>STEP_3.sub<<eof
cd $1
if [ $5 == "yes" ]; then
	read_location="$6"
fi
if [ $5 != "yes" ]; then
	read_location="$7"
fi

read_1=\`ls \$read_location | grep "1.fastq"\`
read_2=\`ls \$read_location | grep "2.fastq"\`

cat>>libraries.txt<<e2of
Reads bwa \${read_location}/\$read_1 \${read_location}/\$read_2 $9 ${10} FR
e2of

mv $2/assembled_genome.fa $2/assembled_genome_before_gapfiller.fa
mv $2/assembled_genome.gff $2/assembled_genome_before_gapfiller.gff
perl $8/remove_flanking_Ns_from_genome.fa $2/assembled_genome_before_gapfiller.fa > formatted_input.fa
GapFiller.pl -l libraries.txt -s formatted_input.fa -d 2000 -T ${11}
cp standard_output/standard_output.gapfilled.final.fa $2/assembled_genome.fa
eof
