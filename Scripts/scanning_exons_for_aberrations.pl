#!/usr/bin/perl -w

open(LIST,$ARGV[0]) || die("Can't open the list of exon names\n");
while(<LIST>)
{
	push(@aLIST,$_);
}
close(LIST);

open(REPORT,">>feature_report.txt") || die("Can't open the exon_report.txt for writing\n");
foreach $exon (@aLIST)
{
	$clean = 0;
	$exon =~ s/\n//;
	$sample = "";
	$reference = "";
	$sample = `grep -A 1 "$ARGV[1]" ${exon}.fasta | grep -vP "^>"`;
	$reference_temp = `grep ">" ${exon}.fasta | grep -v "$ARGV[1]"`;
	$reference_temp =~ s/\n//;
	$reference = `grep -A 1 "$reference_temp" ${exon}.fasta | grep -vP "^>"`;
	$sample =~ s/\n//;
	$reference =~ s/\n//;
	$alignment_score_temp = `grep "Sequences (1:2) Aligned. Score:" ${exon}.fasta.txt`;
	$alignment_score = ($alignment_score_temp =~ m/Score: *(\d+)/)[0];

	$ref_align="";
	$sample_align="";
	$score_align="";
	open(ALIGNMENT,"${exon}.aln") || print "Can't open the alignment file because no sequence was annotated for this ORF\n";
	while(<ALIGNMENT>)
	{
		if($_ =~/multiple sequence alignment/){ next; }
		if($_ =~ /^\n/){ next; }
		if($_ =~ /$exon/ and $_ =~ /$ARGV[1]/)
		{
			$sample_align .= ($_ =~ m/^(\S+)\s+(\S+)/)[1];
			$current_align = ($_ =~ m/^(\S+)\s+(\S+)/)[1];
			next;
		}
		if($_ =~ /$exon/ and $_ !~ /$ARGV[1]/)
		{
			$ref_align .= ($_ =~ m/^(\S+)\s+(\S+)/)[1];
			$current_align = ($_ =~ m/^(\S+)\s+(\S+)/)[1];
			next;
		}
		$score_align_temp = ($_ =~ m/ +(.*?)\n/)[0];
		while(length($score_align_temp) < length($current_align))
		{
			$score_align_temp = " $score_align_temp";
		}
		$score_align .= $score_align_temp;
	}
	close(ALIGNMENT);
#	print "Ref:\t$ref_align\nSample:\t$sample_align\nScores:\t$score_align\n";
	$last_ten = substr($score_align,(length($score_align)-10),10);
#	print "Last 10:\t$last_ten\n";
	$last_ten_matches =()= $last_ten =~ / /gi;
#	print "Last 10 mathes:\t$last_ten_matches\n";

#	print "\n$exon\nScore: $alignment_score\nRef:\t$reference\nSample:\t$sample\n";

	print REPORT "$exon:\t";
	
	########## Detect alignment score below 80% identity
	if($alignment_score < 80)
	{
		print REPORT "(Deviation of percent identity is >= 20%) ";
		$clean = 1;
	}


	########## Detect lack of start ATG
	$methionine = ($sample =~ m/^(.)/)[0];
	$ref_meth = ($reference =~ m/^(.)/)[0];
	if(($methionine ne "m") and ($methionine ne "M") and (($ref_meth eq "M") or ($ref_meth eq "m")))
	{
		print REPORT "(Methionine start codon missing) ";
		$clean = 1;
	}

	########## Detect presence of a gap
        if($sample =~ /X/i)
        {
                print REPORT "(Gaps found) ";
                $clean = 1;
        }
	
	########## Detect size descrepencies
	if((length($sample)/length($reference))	> 1.1)
	{
		print REPORT "(Sample is more than 10% larger than the reference) ";
		$clean = 1;
	}
	if((length($sample)/length($reference)) < 0.9)
        {
                print REPORT "(Sample is more than 10% shorter than the reference) ";
                $clean = 1;
        }

	########## Detect lack of stop codon
	if($sample !~ /\*$/ and $reference  =~ /\*$/)
	{
		print REPORT "(Stop codon missing) ";
		$clean = 1;
	}
	
	########## Detect early stop codon
	if(($last_ten_matches > 3) and (length($sample) < length($reference)) and ($sample =~ /\*$/))
        {
		print REPORT "(Early stop codon found) ";
                $clean = 1;
        }

	if($clean == 0)
	{
		print REPORT "\tNo errors\n";
	}
	if($clean != 0)
	{
		print REPORT "\n";
	}
}
