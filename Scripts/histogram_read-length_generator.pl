#!/usr/bin/perl -w

$counter = -1;
$stops = -1;
@inserts = "";
open(INPUT, $ARGV[0]) || die("Can't open the sam file\n");
while(<INPUT>)
{
	if($_ =~ /^\@/){ next; }
	@splitter = split(/\t/,$_);
	$flag = $splitter[1];
	if((field_2($flag) ? "YES" : "NO") eq "NO"){ next; }
	$match = $splitter[8];
	if($match < 0){ $match = $match*-1; }
	$counter++;
	$inserts[$counter] = $match;
        $stops++;
        if($stops == 100000)
        {
                print STDOUT "Processing line: $counter\r";
                $stops = 0;
        }
}
close(INPUT);
print STDOUT "Processing line: $counter\n";

$total = 0;
$entries = 0;
foreach $ENTRY (@inserts)
{
	$entries++;
	$total += $ENTRY;
}
$mean = $total / $entries;

$variance = 0;
$squared = 0;
foreach $ENTRY (@inserts)
{
	$squared = $mean - $ENTRY;
#	print "\n$squared\n";
	$squared = $squared * $squared;
	$variance += $squared;
#	print "\n$squared\n";
}

$stddev = $variance / ($entries-1);
$stddev = sqrt($stddev);
print STDOUT "Total:\t$total\nEntries:\t$entries\nMean:\t$mean\nStd Dev:\t$stddev\n";

$counter = -1;
$stops = -1;
open(INPUT, $ARGV[0]) || die("Can't open the .sam file\n");
while(<INPUT>)
{
	if($_ =~ /^\@/){ next; }
	@splitter = split(/\t/,$_);
	if($splitter[5] eq "*")
	{
		next;
	}
	if(((field_2($splitter[1]) ? "YES" : "NO") eq "YES") and (($splitter[8] >= ($mean - $stddev)) and ($splitter[8] <= ($mean + $stddev))))
	{
		$HASH_good{$splitter[0]} = $_;
	}
	if(((field_2($splitter[1]) ? "YES" : "NO") eq "NO") or (($splitter[8] < ($mean - $stddev)) or ($splitter[8] > ($mean + $stddev))))
        {
                $HASH_bad{$splitter[0]} = $_;
        }
        $counter++;
        $stops++;
        if($stops == 100000)
        {
                print STDOUT "Processing line: $counter\r";
                $stops = 0;
        }
}
close(INPUT);
print STDOUT "Processing line: $counter\n";


while(($key,$value) = each %HASH_good)
{
	@splitty = split(/\t/,$value);
	$read_pos = $splitty[3];
	$read_cigar = $splitty[5];
	$read_cigar =~ s/M/M /g;
	$read_cigar =~ s/I/I /g;
	$read_cigar =~ s/N/N /g;
	$read_cigar =~ s/D/D /g;
	$read_cigar =~ s/S/S /g;
	$read_cigar =~ s/H/H /g;
	$read_cigar =~ s/P/P /g;
	@sub_split = split(/ /,$read_cigar);
	$read_span = 0;
	foreach $SECTION (@sub_split)
	{
		#print "$SECTION\n";
		if($SECTION =~ /M/)
		{
			$SECTION =~ s/M//;
			$read_span += $SECTION;
		}
		if($SECTION =~ /D/)
		{
			$SECTION =~ s/D//;
			$read_span += $SECTION;
		}
		if($SECTION =~ /I/)
		{
			$SECTION =~ s/I//;
			$read_span -= $SECTION;
		}
	}
	#print "$read_span\n\n";
	$hash_counter = ($read_pos-1);
	while($read_span != 0)
	{
		$hash_counter++;
		if(exists $FINAL_good{$hash_counter})
		{
			$FINAL_good{$hash_counter} += 1;
		}
		if(not exists $FINAL_good{$hash_counter})
		{
			$FINAL_good{$hash_counter} = 1;
		}
		$read_span--;
	}
	#print "Good:\t$read_pos\t$read_cigar\t$value\n";
}

while(($key,$value) = each %HASH_bad)
{
        @splitty = split(/\t/,$value);
        $read_pos = $splitty[3];
        $read_cigar = $splitty[5];
        $read_cigar =~ s/M/M /g;
        $read_cigar =~ s/I/I /g;
        $read_cigar =~ s/N/N /g;
        $read_cigar =~ s/D/D /g;
        $read_cigar =~ s/S/S /g;
        $read_cigar =~ s/H/H /g;
        $read_cigar =~ s/P/P /g;
        @sub_split = split(/ /,$read_cigar);
        $read_span = 0;
        foreach $SECTION (@sub_split)
        {
                #print "$SECTION\n";
                if($SECTION =~ /M/)
                {
                        $SECTION =~ s/M//;
                        $read_span += $SECTION;
                }
                if($SECTION =~ /D/)
                {
                        $SECTION =~ s/D//;
                        $read_span += $SECTION;
                }
                if($SECTION =~ /I/)
                {
                        $SECTION =~ s/I//;
                        $read_span -= $SECTION;
                }
        }
        #print "$read_span\n\n";
        $hash_counter = ($read_pos-1);
        while($read_span != 0)
        {
                $hash_counter++;
                if(exists $FINAL_bad{$hash_counter})
                {
                        $FINAL_bad{$hash_counter} += 1;
                }
                if(not exists $FINAL_bad{$hash_counter})
                {
                        $FINAL_bad{$hash_counter} = 1;
                }
                $read_span--;
        }
        #print "Good:\t$read_pos\t$read_cigar\t$value\n";
}

open(OUT1, ">read_span_good.txt") || die("Can't open file for writing\n");
foreach (sort { $a <=> $b } keys(%FINAL_good) )
{
	print OUT1 "$_ $FINAL_good{$_}\n";
}
close(OUT1);

open(OUT2, ">read_span_bad.txt") || die("Can't open file for writing\n");
foreach (sort { $a <=> $b } keys(%FINAL_bad) )
{
        print OUT2 "$_ $FINAL_bad{$_}\n";
}
close(OUT2);

sub dec2bin {
        my $str = unpack("B32", pack("N", shift));
        $str =~ s/^0+(?=\d)//;   # otherwise you'll get leading zeros
	return $str;
}

sub bin2dec{
	my $query = oct("0b".shift);
        return $query;
}

sub field_1 { $_[0] & 0x0001 } # the read is paired in sequencing
sub field_2 { $_[0] & 0x0002 } # the read is mapped in a proper pair
sub field_3 { $_[0] & 0x0004 } # the query sequence itself is unmapped
sub field_4 { $_[0] & 0x0008 } # the mate is unmapped
sub field_5 { $_[0] & 0x0010 } # strand of the query (0 = forward, 1 = reverse)
sub field_6 { $_[0] & 0x0020 } # strand of the mate (0 - forward, 1 = reverse)
sub field_7 { $_[0] & 0x0040 } # the read is the first read in a pair
sub field_8 { $_[0] & 0x0080 } # the read is the second read in a pair
sub field_9 { $_[0] & 0x0100 } # the alginment is not primary
sub field_10 { $_[0] & 0x0200 } # the read fails platform/vender QC checks
sub field_11 { $_[0] & 0x0400 } # the read is either PCR or optical duplicate
