#!/usr/bin/perl -w

#-------------------------------------------------------------#
#     extract_gtf_features_from_fasta.pl - By Jacob Shreve    #
#        Szpara lab, The Pennsylvania State University        #
#                jtshreve@psu.edu     v1.0.0                  #
#-------------------------------------------------------------#

#  The purpose of this script is to extract all features listed
#  in a GTF file from its corresponding fasta file, and in the
#  correct orientation (5' -> 3').

#  Usage:     perl extract_gtf_features_from_fasta.pl /
#             input.fasta input.gtf 1/2
#
#             Use "1" for genes, "2" for transcripts

open(FASTA, $ARGV[0]) || die("Can't open the FASTA file");
while(<FASTA>)
{
	if($_ =~ /^>/)
	{
		$_ =~ s/\n//;
		$header = ($_ =~ m/^>(.*?)(\s|$)/)[0];
	}
	if($_ !~ /^>/)
	{
		$_ =~ s/\n//;
		$HASH{$header} .= $_;
	}
}
close(FASTA);

if($ARGV[2] eq "1")
{
open(GTF, $ARGV[1]) || die("Can't open the GTF file");
while(<GTF>)
{
		$_ =~ s/\n//;
		@splitter = split(/\t/,$_);
		if($splitter[2] ne "gene"){ next; }
		$chromo = $splitter[0];
		$start = $splitter[3] - 1;
		$end = $splitter[4] - 1;
		$length = ($end - $start) + 1;
                $fragment = substr $HASH{$chromo}, $start, $length;
		$name = ($splitter[8] =~ m/ID=(.*?)(;|$)/)[0];
		$feature = $splitter[2];
		if($splitter[6] eq "+")
		{
			print ">${name} Sequence for $chromo, Feature=$feature, from $start-$end, $splitter[6], from $ARGV[0]:\n";
		}
		else
		{
			$fragment = reverse($fragment);
			$fragment =~ tr/ATCG/TAGC/;
			print ">${name} Sequence for $chromo, Feature=$feature, from $start-$end, $splitter[6], from $ARGV[0]:\n";
		}
		print "$fragment\n";
}
close(GTF);
}

if($ARGV[2] eq "3")
{
open(GTF, $ARGV[1]) || die("Can't open the GTF file");
while(<GTF>)
{
                $_ =~ s/\n//;
                @splitter = split(/\t/,$_);
                $chromo = $splitter[0];
                $start = $splitter[3] - 1;
                $end = $splitter[4] - 1;
                $length = ($end - $start) + 1;
                $fragment = substr $HASH{$chromo}, $start, $length;
                $name = ($splitter[8] =~ m/ID=(.*?)(;|$)/)[0];
                $feature = $splitter[2];
                if($splitter[6] eq "+")
                {
                        print ">${name} Sequence for $chromo, Feature=$feature, from $start-$end, $splitter[6], from $ARGV[0]:\n";
                }
                else
                {
                        $fragment = reverse($fragment);
                        $fragment =~ tr/ATCG/TAGC/;
                        print ">${name} Sequence for $chromo, Feature=$feature, from $start-$end, $splitter[6], from $ARGV[0]:\n";
                }
                print "$fragment\n";
}
close(GTF);
}

if($ARGV[2] eq "2")
{

open(GTF, $ARGV[1]) || die("Can't open the GTF file");
while(<GTF>)
{
	if($_ =~ /^#/){ next; }
	if($_ !~ /\texon\t/){ next; }
	@splitter = split(/\t/,$_);
	$parent_name = ($splitter[8] =~ m/Parent=(.*?)(;|$)/)[0];
	$PARENT{$parent_name} .= $_;
}
close(GTF);

while(($key,$value) = each %PARENT)
{
	$key =~ s/\n//;
	@splitter = split(/\n/,$value);
	if($#splitter != 0)
	{
		@sub_exon="";
		$count = -1;
		foreach $LINE (@splitter)
		{
			$count++;
			@temp_split = split(/\t/,$LINE);
			$sub_exon[$count][0] = $temp_split[3];
			$sub_exon[$count][1] = $temp_split[4];
			$sub_exon[$count][2] = $temp_split[6];
			$sub_exon[$count][3] = $temp_split[0];
			$sub_exon[$count][4] = ($temp_split[8] =~ m/Parent=(.*?)(;|$)/)[0];
		}
		#print "XX\n$sub_exon[0][0]\nXX\n";
		@sorted_sub_exon = sort { $a->[0] <=> $b->[0] } @sub_exon;
		if($temp_split[6] eq "-")
		{
			@rev_hold = reverse(@sorted_sub_exon);
			@sorted_sub_exon = @rev_hold;
		}
		$exon_name = ">";
		$exon_storage = "";
		for($i=0; $i<=$#sorted_sub_exon; $i++)
		{
			#print "$sorted_sub_exon[$i][0]\t$sorted_sub_exon[$i][1]\t$sorted_sub_exon[$i][2]\t$sorted_sub_exon[$i][3]\t$sorted_sub_exon[$i][4]\n";
			$chromo = $sorted_sub_exon[$i][3];
	                $start = $sorted_sub_exon[$i][0] - 1;
	                $end = $sorted_sub_exon[$i][1] - 1;
	                $length = ($end - $start) + 1;
	                $fragment = substr $HASH{$chromo}, $start, $length;
	                $name = $sorted_sub_exon[$i][4];
	                $feature = "exon";
			$strand = $sorted_sub_exon[$i][2];
	                if($strand eq "+")
	                {
	                        $exon_name .= "${name} Sequence for $chromo, Feature=$feature, from $start-$end, $strand, from $ARGV[0]\t";
	                }
	                else
	                {
	                        $fragment = reverse($fragment);
	                        $fragment =~ tr/ATCG/TAGC/;
	                        $exon_name .= "${name} Sequence for $chromo, Feature=$feature, from $start-$end, $strand, from $ARGV[0]\t";
	                }
	                $exon_storage .= $fragment;
		}
		print "$exon_name\n$exon_storage\n";
	}
	else
	{
		@splittery = split(/\t/,$splitter[0]);
		$chromo = $splittery[0];
                $start = $splittery[3] - 1;
                $end = $splittery[4] - 1;
                $length = ($end - $start) + 1;
                $fragment = substr $HASH{$chromo}, $start, $length;
                $name = ($splittery[8] =~ m/Parent=(.*?)(;|$)/)[0];
                $feature = $splittery[2];
                if($splittery[6] eq "+")
                {
                        print ">${name} Sequence for $chromo, Feature=$feature, from $start-$end, $splittery[6], from $ARGV[0]:\n";
                }
                else
                {
                        $fragment = reverse($fragment);
                        $fragment =~ tr/ATCG/TAGC/;
                        print ">${name} Sequence for $chromo, Feature=$feature, from $start-$end, $splittery[6], from $ARGV[0]:\n";
                }
                print "$fragment\n";
	}
}

}
