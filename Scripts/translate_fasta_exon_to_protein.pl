#!/usr/bin/perl -w
$input = $ARGV[0];
my $argc;
$argc = @ARGV;

open (SOURCE, $input) || die("Could not open file...");
while (<SOURCE>)
{
        $_ =~ s/\n//;
	if($_ =~ /^>/)
	{
		$identifier = $_;
	}
	else
	{
		$HASH{$identifier} .= $_;
	}
}
close(SOURCE);

while(($key, $value) = each %HASH)
{
	$temp_trans = &translate_DNA_string($value);
	$temp_trans =~ s/\*.*/\*/;
	print "$key\n",$temp_trans,"\n";
}

sub translate_DNA_string
{
	$query = $_[0];
	$AA = "";
	$peptide = "";
	$len = length($query);
	for($i=0; $i<$len; $i=$i+3)
	{
		$codon = substr $query, $i, 3;
		$codon =~ tr/a-z/A-Z/;
		if($codon =~ /N/i)
		{
			$AA = "X";
			$peptide .= $AA;
			next;
		}
		if($codon eq "TTT" or $codon eq "TTC")
		{
			$AA = "F";
		}
		if($codon eq "TTA" or $codon eq "TTG" or $codon eq "CTT" or $codon eq "CTC" or $codon eq "CTA" or $codon eq "CTG")
		{
			$AA = "L";
		}
		if($codon eq "ATT" or $codon eq "ATC" or $codon eq "ATA")
		{
			$AA = "I";
		}
		if($codon eq "ATG")
		{
			$AA = "M";
		}
		if($codon eq "GTT" or $codon eq "GTC" or $codon eq "GTA" or $codon eq "GTG")
		{
			$AA = "V";
		}
		if($codon eq "TCT" or $codon eq "TCC" or $codon eq "TCA" or $codon eq "TCG" or $codon eq "AGT" or $codon eq "AGC")
		{
			$AA = "S";
		}
		if($codon eq "CCT" or $codon eq "CCC" or $codon eq "CCA" or $codon eq "CCG")
		{
			$AA = "P";
		}
		if($codon eq "ACT" or $codon eq "ACC" or $codon eq "ACA" or $codon eq "ACG")
		{
			$AA = "T";
		}
		if($codon eq "GCT" or $codon eq "GCC" or $codon eq "GCA" or $codon eq "GCG")
		{
			$AA = "A";
		}
		if($codon eq "TAT" or $codon eq "TAC")
		{
			$AA = "Y";
		}
		if($codon eq "TAA" or $codon eq "TAG" or $codon eq "TGA")
		{
			$AA = "*";
		}
		if($codon eq "CAT" or $codon eq "CAC")
		{
			$AA = "H";
		}
		if($codon eq "CAA" or $codon eq "CAG")
		{
			$AA = "Q";
		}
		if($codon eq "AAT" or $codon eq "AAC")
		{
			$AA = "N";
		}
		if($codon eq "AAA" or $codon eq "AAG")
		{
			$AA = "K";
		}
		if($codon eq "GAT" or $codon eq "GAC")
		{
			$AA = "D";
		}
		if($codon eq "GAA" or $codon eq "GAG")
		{
			$AA = "E";
		}
		if($codon eq "TGT" or $codon eq "TGC")
		{
			$AA = "C";
		}
		if($codon eq "TGG")
		{
			$AA = "W";
		}
		if($codon eq "CGT" or $codon eq "CGC" or $codon eq "CGA" or $codon eq "CGG")
		{
			$AA = "R";
		}
		if($codon eq "AGA" or $codon eq "AGG")
		{
			$AA = "R";
		}
		if($codon eq "GGT" or $codon eq "GGC" or $codon eq "GGA" or $codon eq "GGG")
		{
			$AA = "G";
		}
		$peptide .= $AA;
	}
	return $peptide;
}
