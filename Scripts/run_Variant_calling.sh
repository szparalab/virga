cat>>STEP_4.sub<<eof
cd $1
samtools mpileup -d 150000 -f $1/${2}.fa ${2}_sorted.bam -u | bcftools view -bcvg - > ${2}.bcf
bcftools view ${2}.bcf | vcfutils.pl varFilter -D 150000 > ${2}.vcf
if [ $3 == "yes" ]; then
	freebayes -p 1 --fasta-reference $1/${2}.fa ${2}_sorted.bam > freebayes_${2}.vcf
fi
eof
