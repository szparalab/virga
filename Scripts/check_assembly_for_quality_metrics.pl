#!/usr/bin/perl -w

#-------------------------------------------------------------#
#   check_assembly_for_quality_metrics.pl - By Jacob Shreve   #
#        Szpara lab, The Pennsylvania State University        #
#                jtshreve@psu.edu     v1.0.0                  #
#-------------------------------------------------------------#

#  The purpose of the script is to calculate the common quality
#  metrics associated with assemblies. Just provide the fasta
#  file of the assembly to obtain the statistics.

#  Usage:     perl check_assembly_for_quality_metrics.pl /
#	      assembly_fasta_file.fa

$total_length = 0;
open(FASTA, $ARGV[0]) || die("Can't open the FASTA file");
while(<FASTA>)
{
	if($_ =~ /^>/)
	{
		$_ =~ s/\n//;
		$header = $_;
	}
	if($_ !~ /^>/)
	{
		$_ =~ s/\n//;
		$HASH{$header} += length($_);
		$total_length += length($_);
	}
}
close(FASTA);

$N50_target = $total_length / 2;
$N20_target = $total_length*0.2;
$N80_target = $total_length*0.8;
$running_total = 0;
$above_N50 = 0;
foreach $key (sort { $HASH {$b} <=> $HASH {$a}} keys %HASH )
{
	$running_total += $HASH{$key};
	$above_N50++;
	if($running_total >= $N50_target and not defined ($N50))
	{
		$N50 = $HASH{$key};
		$N50_above = $above_N50;
	}
	if($running_total >= $N80_target and not defined ($N80))
        {
                $N80 = $HASH{$key};
        }
        if($running_total >= $N20_target and not defined ($N20))
        {
                $N20 = $HASH{$key};
        }
}

$average = $total_length / (keys %HASH);
$largest = (sort { $HASH {$b} <=> $HASH {$a}} keys %HASH )[0];
$shortest = (sort { $HASH {$b} <=> $HASH {$a}} keys %HASH )[-1];


print "Total # of contigs:\t\t".(keys %HASH)."\n";
print "Average contig length:\t\t".int($average)."\n";
print "Longest contig:\t\t\t".$HASH{$largest}."\n";
print "Shortest contig:\t\t".$HASH{$shortest}."\n";
print "N20:\t\t\t\t$N20\n";
print "N50:\t\t\t\t$N50\n";
print "N80:\t\t\t\t$N80\n";
print "Number of contigs >= N50:\t$N50_above\n";
print "Total # of bases:\t\t$total_length\n";
