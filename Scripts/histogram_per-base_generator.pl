#!/usr/bin/perl -w

##########################################################

# The first step is to grab the insert sizes of all mapped
# reads as long as the flag string indicates that the read
# is properly paired and mapped.
#
# Second step is to calculate teh std dev for all insert
# sizes.

##########################################################

$counter = -1;
$stops = -1;
@inserts = "";
open(INPUT, $ARGV[0]) || die("Can't open the sam file\n");
while(<INPUT>)
{
	if($_ =~ /^\@/){ next; }
	@splitter = split(/\t/,$_);
	$flag = $splitter[1];
	if((field_2($flag) ? "YES" : "NO") eq "NO"){ next; }
	$match = $splitter[8];
	if($match < 0){ $match = $match*-1; }
	$counter++;
	$inserts[$counter] = $match;
        $stops++;
        if($stops == 100000)
        {
                print STDOUT "Processing line: $counter\n";
                $stops = 0;
        }
}
close(INPUT);

$total = 0;
$entries = 0;
foreach $ENTRY (@inserts)
{
	$entries++;
	$total += $ENTRY;
}
$mean = $total / $entries;

$variance = 0;
$squared = 0;
foreach $ENTRY (@inserts)
{
	$squared = $mean - $ENTRY;
#	print "\n$squared\n";
	$squared = $squared * $squared;
	$variance += $squared;
#	print "\n$squared\n";
}

$stddev = $variance / ($entries-1);
$stddev = sqrt($stddev);
print STDOUT "Total:\t$total\nEntries:\t$entries\nMean:\t$mean\nStd Dev:\t$stddev\n";

$counter = -1;
$stops = -1;
$position = 0;
$mapped = 0;
open(INPUT, $ARGV[0]) || die("Can't open the .sam file\n");
while(<INPUT>)
{
	if($_ =~ /^\@/){ next; }
	$position++;
	@splitter = split(/\t/,$_);
	if($splitter[8] <0)
	{
		$splitter[8] = $splitter[8] * -1;
	}
	if(((field_2($splitter[1]) ? "YES" : "NO") eq "YES") and (($splitter[8] >= ($mean - $stddev)) and ($splitter[8] <= ($mean + $stddev))))
	{
		$HASH_good{$splitter[3]} += 1;
		$mapped++;
	}
	if(((field_2($splitter[1]) ? "YES" : "NO") eq "NO") or (($splitter[8] < ($mean - $stddev)) or ($splitter[8] > ($mean + $stddev))))
        {
                $HASH_bad{$splitter[3]} += 1;
		$mapped++;
        }
        $counter++;
        $stops++;
        if($stops == 100000)
        {
                print STDOUT "Processing line: $counter\n";
                $stops = 0;
        }
}
close(INPUT);

print "Total reads mapped in .sam file: $position\nTotal reads scored per position: $mapped\n";

open(OUT, ">>histogram_per-base_positive.txt") || die("Can't write the histogram\n");
foreach (sort { $a <=> $b } keys(%HASH_good))
{
	print OUT "chr1 $_ $_ $HASH_good{$_}\n";
}
close(OUT);

open(OUT, ">>histogram_per-base_negative.txt") || die("Can't write the histogram\n");
foreach (sort { $a <=> $b } keys(%HASH_bad))
{
        print OUT "chr1 $_ $_ $HASH_bad{$_}\n";
}
close(OUT);

$all_good = 0;
$max_good = 0;
foreach (sort { $a <=> $b } keys(%HASH_good))
{
        $all_good += $HASH_good{$_};
        if($HASH_good{$_} > $max_good)
        {
                $max_good = $HASH_good{$_};
        }
}

open(OUT, ">>histogram_per-base_positive_scaled.txt") || die("Can't write out the histogram\n");
foreach (sort { $a <=> $b } keys(%HASH_good))
{
        $temp = $HASH_good{$_} / $max_good;
        print OUT "chr1 $_ $_ $temp\n";
}
close(OUT);

delete $HASH_bad{0};
$all_bad = 0;
$max_bad = 0;
foreach (sort { $a <=> $b } keys(%HASH_bad))
{
	$all_bad += $HASH_bad{$_};
	if($HASH_bad{$_} > $max_bad)
	{
		$max_bad = $HASH_bad{$_};
	}
}

open(OUT, ">>histogram_per-base_negative_scaled.txt") || die("Can't write out the histogram\n");
foreach (sort { $a <=> $b } keys(%HASH_bad))
{
	$temp = $HASH_bad{$_} / $max_bad;
	print OUT "chr1 $_ $_ $temp\n";
}
close(OUT);

sub dec2bin {
        my $str = unpack("B32", pack("N", shift));
        $str =~ s/^0+(?=\d)//;   # otherwise you'll get leading zeros
	return $str;
}

sub bin2dec{
	my $query = oct("0b".shift);
        return $query;
}

sub field_1 { $_[0] & 0x0001 } # the read is paired in sequencing
sub field_2 { $_[0] & 0x0002 } # the read is mapped in a proper pair
sub field_3 { $_[0] & 0x0004 } # the query sequence itself is unmapped
sub field_4 { $_[0] & 0x0008 } # the mate is unmapped
sub field_5 { $_[0] & 0x0010 } # strand of the query (0 = forward, 1 = reverse)
sub field_6 { $_[0] & 0x0020 } # strand of the mate (0 - forward, 1 = reverse)
sub field_7 { $_[0] & 0x0040 } # the read is the first read in a pair
sub field_8 { $_[0] & 0x0080 } # the read is the second read in a pair
sub field_9 { $_[0] & 0x0100 } # the alginment is not primary
sub field_10 { $_[0] & 0x0200 } # the read fails platform/vender QC checks
sub field_11 { $_[0] & 0x0400 } # the read is either PCR or optical duplicate
