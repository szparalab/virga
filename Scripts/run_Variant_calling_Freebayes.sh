cat>>STEP_4.sub<<eof
cd $1
mv freebayes_${2}.vcf temp.vcf
perl $3/filter_freebayes_vcf.pl temp.vcf $4 $5 $6 $7 > freebayes_${2}.vcf
eof
