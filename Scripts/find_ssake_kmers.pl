#!/usr/bin/perl -w

### Define scoring parameters ###

$least_contigs_score = 1;
$largest_average_contig_score = 1;
$largest_single_contig_score = 1;
$best_N20 = 1;
$best_N50 = 1;
$best_N80 = 1;
$largest_total_assembly = 1;

###


### Grab the important columns from the kmer file

$number_of_kmers = $ARGV[1];

open(KMERS,$ARGV[0]) || die("Can't open the kmers file\n");
while(<KMERS>)
{
	$_ =~ s/\n//;
	if($_ !~ /^m/){ next; }
	@splitter = split(/\t/,$_);
	$HASH_contigs{$splitter[0]} = $splitter[1];
	$HASH_average{$splitter[0]} = $splitter[2];
	$HASH_longest{$splitter[0]} = $splitter[3];
	$HASH_N20{$splitter[0]} = $splitter[5];
	$HASH_N50{$splitter[0]} = $splitter[6];
	$HASH_N80{$splitter[0]} = $splitter[7];
	$HASH_total{$splitter[0]} = $splitter[9]
}
close(KMERS);

# Least number of contigs
$counter = 0;
$old = "";
foreach $key (sort { $HASH_contigs {$a} <=> $HASH_contigs {$b}} keys %HASH_contigs )
{
	if($counter == 0)
	{
		$store = $HASH_contigs{$key};
	}
	if($counter != 0)
	{
		$old = $store;
		$store = $HASH_contigs{$key};
		if($old == $store)
		{
			next;
		}
	}
	if($counter <= 15)
	{
		while(($key2,$value2) = each %HASH_contigs)
		{
			if($value2 == $HASH_contigs{$key})
			{
				$HASH_rank{$key2} += $least_contigs_score;
				$counter++;
			}
		}
	}
	if($counter >= 15)
	{
		last;
	}
}

# Longest average contig
$counter = 0;
$old = "";
foreach $key (sort { $HASH_average {$b} <=> $HASH_average {$a}} keys %HASH_average )
{
        if($counter == 0)
        {
                $store = $HASH_average{$key};
        }
        if($counter != 0)
        {
                $old = $store;
                $store = $HASH_average{$key};
                if($old == $store)
                {
                        next;
                }
        }
        if($counter <= 15)
        {
                while(($key2,$value2) = each %HASH_average)
                {
                        if($value2 == $HASH_average{$key})
                        {
                                $HASH_rank{$key2} += $largest_average_contig_score;
                                $counter++;
                        }
                }
        }
        if($counter >= 15)
        {
                last;
        }
}

# Longest single contig
$counter = 0;
$old = "";
foreach $key (sort { $HASH_longest {$b} <=> $HASH_longest {$a}} keys %HASH_longest )
{
        if($counter == 0)
        {
                $store = $HASH_longest{$key};
        }
        if($counter != 0)
        {
                $old = $store;
                $store = $HASH_longest{$key};
                if($old == $store)
                {
                        next;
                }
        }
        if($counter <= 15)
        {
                while(($key2,$value2) = each %HASH_longest)
                {
                        if($value2 == $HASH_longest{$key})
                        {
                                $HASH_rank{$key2} += $largest_single_contig_score;
                                $counter++;
                        }
                }
        }
        if($counter >= 15)
        {
                last;
        }
}

#Largest N20
$counter = 0;
$old = "";
foreach $key (sort { $HASH_N20 {$b} <=> $HASH_N20 {$a}} keys %HASH_N20 )
{
        if($counter == 0)
        {
                $store = $HASH_N20{$key};
        }
        if($counter != 0)
        {
                $old = $store;
                $store = $HASH_N20{$key};
                if($old == $store)
                {
                        next;
                }
        }
        if($counter <= 15)
        {
                while(($key2,$value2) = each %HASH_N20)
                {
                        if($value2 == $HASH_N20{$key})
                        {
                                $HASH_rank{$key2} += $best_N20;
                                $counter++;
                        }
                }
        }
        if($counter >= 15)
        {
                last;
        }
}

#Largest N50
$counter = 0;
$old = "";
foreach $key (sort { $HASH_N50 {$b} <=> $HASH_N50 {$a}} keys %HASH_N50 )
{
        if($counter == 0)
        {
                $store = $HASH_N50{$key};
        }
        if($counter != 0)
        {
                $old = $store;
                $store = $HASH_N50{$key};
                if($old == $store)
                {
                        next;
                }
        }
        if($counter <= 15)
        {
                while(($key2,$value2) = each %HASH_N50)
                {
                        if($value2 == $HASH_N50{$key})
                        {
                                $HASH_rank{$key2} += $best_N50;
                                $counter++;
                        }
                }
        }
        if($counter >= 15)
        {
                last;
        }
}

#Largest N80
$counter = 0;
$old = "";
foreach $key (sort { $HASH_N80 {$b} <=> $HASH_N80 {$a}} keys %HASH_N80 )
{
        if($counter == 0)
        {
                $store = $HASH_N80{$key};
        }
        if($counter != 0)
        {
                $old = $store;
                $store = $HASH_N80{$key};
                if($old == $store)
                {
                        next;
                }
        }
        if($counter <= 15)
        {
                while(($key2,$value2) = each %HASH_N80)
                {
                        if($value2 == $HASH_N80{$key})
                        {
                                $HASH_rank{$key2} += $best_N80;
                                $counter++;
                        }
                }
        }
        if($counter >= 15)
        {
                last;
        }
}

# Largest total number of bases in assembly
$counter = 0;
$old = "";
foreach $key (sort { $HASH_total {$b} <=> $HASH_total {$a}} keys %HASH_total )
{
        if($counter == 0)
        {
                $store = $HASH_total{$key};
        }
        if($counter != 0)
        {
                $old = $store;
                $store = $HASH_total{$key};
                if($old == $store)
                {
                        next;
                }
        }
        if($counter <= 15)
        {
                while(($key2,$value2) = each %HASH_total)
                {
                        if($value2 == $HASH_total{$key})
                        {
                                $HASH_rank{$key2} += $largest_total_assembly;
                                $counter++;
                        }
                }
        }
        if($counter >= 15)
        {
                last;
        }
}

$counter = 0;
$kmer_output = "";
foreach $key (sort { $HASH_rank {$b} <=> $HASH_rank {$a}} keys %HASH_rank )
{
	$key_temp = $key;
	$key_temp =~ s/t.*//;
	$key_temp =~ s/m//;
	print "$key = $HASH_rank{$key}\n";
	if($counter == 0)
	{
		$store = $HASH_rank{$key};
	}
	if($counter != 0)
	{
		$old = $store;
		$store = $HASH_rank{$key};
	}
	$counter++;
	if($counter <= $number_of_kmers)
	{
		if(exists $HASH_kmer{$key_temp}){ $counter--; next; }
		$kmer_output .= "$key,";
                $HASH_kmer{$key_temp} = 1;
	}
	if($counter == ($number_of_kmers+1) and $store == $old)
	{
		if(exists $HASH_kmer{$key_temp}){ $counter--; next; }
		$kmer_output .= "$key,";
		$HASH_kmer{$key_temp} = 1;
		$counter--;
	}
}
$kmer_output =~ s/,$//;
$kmer_output =~ s/m//g;
$kmer_output =~ s/t/-/g;

$counter = 0;
$kmer_output2 = "";
foreach $key (sort { $HASH_rank {$b} <=> $HASH_rank {$a}} keys %HASH_rank )
{
        $key_temp = $key;
        $key_temp =~ s/t.*//;
        $key_temp =~ s/m//;
        print "$key = $HASH_rank{$key}\n";
        if($counter == 0)
        {
                $store = $HASH_rank{$key};
        }
        if($counter != 0)
        {
                $old = $store;
                $store = $HASH_rank{$key};
        }
        $counter++;
        if($counter <= $number_of_kmers)
        {
                #if(exists $HASH_kmer{$key_temp}){ $counter--; next; }
                $kmer_output2 .= "$key,";
                $HASH_kmer{$key_temp} = 1;
        }
        if($counter == ($number_of_kmers+1) and $store == $old)
        {
                #if(exists $HASH_kmer{$key_temp}){ $counter--; next; }
                $kmer_output2 .= "$key,";
                $HASH_kmer{$key_temp} = 1;
                $counter--;
        }
}
$kmer_output2 =~ s/,$//;
$kmer_output2 =~ s/m//g;
$kmer_output2 =~ s/t/-/g;



print STDOUT "\nThe following were the best kmer/trimming combinations found for use with Multi-SSAKE within VirGA\n";
print STDOUT "Number of kmer/trimming combinations requested: $number_of_kmers\n";
print STDOUT "Be aware that more kmers than requested may be reported if numerous kmers report that same quality score\n";
print STDOUT "Without overlapping kmers with different trimming parameters:\n";
print STDOUT "\n$kmer_output\n\n";
print STDOUT "With overlapping kmers with different trimming parameters:\n";
print STDOUT "\n$kmer_output2\n\n";
open(OUT,">ssake_kmer_selection.txt") || die("Can't write the kmer output\n");
print OUT "\nThe following were the best kmer/trimming combinations found for use with Multi-SSAKE within VirGA\n";
print OUT "Number of kmer/trimming combinations requested: $number_of_kmers\n";
print OUT "Be aware that more kmers than requested may be reported if numerous kmers report that same quality score\n";
print OUT "Without overlapping kmers with different trimming parameters:\n";
print OUT "\n$kmer_output\n\n";
print OUT "Without overlapping kmers with different trimming parameters:\n";
print OUT "\n$kmer_output2\n\n";
close(OUT);
