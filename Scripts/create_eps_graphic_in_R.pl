#!/usr/bin/perl -w

#  Param 1 = GFF of assembly
#  Param 2 = Genome of assembly
#  Param 3 = Corrected Pileup
#  Param 4 = Low coverage gff
#  Param 5 = No coverage gff

system("cut -f 1-4 $ARGV[2] > cut_pileup.list");
open(PILEUP,"cut_pileup.list") || die("Can't open the cut colums file from the corrected pileup file\n");
while(<PILEUP>)
{
	$_ =~ s/\n//;
	@split_pileup = split(/\t/,$_);
	if($split_pileup[3] == 0)
	{
		$split_pileup[3] = 1;
	}
	$position_total .= "$split_pileup[1], ";
	$depth_total .= "$split_pileup[3], ";
	$min_cutoff_total .= "100, ";
	$med_cutoff_total .= "1000, ";
	$high_cutoff_total .= "10000, ";
}
close(PILEUP);
$position_total =~ s/, $//;
$depth_total =~ s/, $//;
$min_cutoff_total =~ s/, $//;
$med_cutoff_total =~ s/, $//;
$high_cutoff_total =~ s/, $//;

open(GENOME,$ARGV[1]) || die("Can't open the genome file\n");
while(<GENOME>)
{
	if($_ =~ /^>/)
	{
		$genome_ID = $_;
		$genome_ID =~ s/^>//;
	}
	$_ =~ s/\n//;
	$total_genome .= $_;
}
close(GENOME);
$genome_length = length($total_genome);
#print STDERR "Genome length: $genome_length\n";

open(GFF,$ARGV[0]) || die("Can't open the GFF file\n");
while(<GFF>)
{
	$_ =~ s/\n//;
	@split_GFF = split(/\t/,$_);
	$FEATURES{$split_GFF[2]} .= "$split_GFF[3]\t$split_GFF[4]\t$split_GFF[6]\t$split_GFF[8]\n";;
}
close(GFF);

@colors = ("dark green", "dark green", "purple", "black", "grey", "red", "magenta");

open(FILE_OUT,">GFF_visualization_from_$ARGV[0].r") || die("Can't open the outfile for writing");
$location = `pwd`;
$location =~ s/\n//;
while(($key,$value) = each %FEATURES)
{
	print STDERR "Building GFF visualization track for $key...\n";
	@split_into_unique_features = split(/\n/,$value);
	$start = "";
	$stop = "";
	$name = "";
	@split_into_entries = "";
	$start = "1, ";
	$stop = "1, ";
	$name = "genome, ";
	$quick_hold = shift(@colors);
	push(@colors,$quick_hold);
	foreach $LINE (@split_into_unique_features)
	{
		@split_into_entries = split(/\t/,$LINE);
		#print "$split_into_entries[0]\t$split_into_entries[1]\t$split_into_entries[2]\t$split_into_entries[3]\n";
		$start .= "$split_into_entries[0], ";
		$stop .= "$split_into_entries[1], ";
		$name .= "\"".($split_into_entries[3] =~ m/=(.*?)(;|$)/)[0]."\", ";
	}
	$start .= "$genome_length, ";
	$stop .= "$genome_length, ";
	$name .= "genome, ";
	$start =~ s/, $//;
	$stop =~ s/, $//;
	$name =~ s/, $//;

#	print FILE_OUT "setwd(\"$location\")\n";
#	print FILE_OUT "library(ggplot2)\n";
#	print FILE_OUT "library(grid)\n";
#	print FILE_OUT "pdf(file=\"${key}_GFF_feature.pdf\", width=5600, height=70)\n";
#	print FILE_OUT "plot.data <- data.frame(start.points=c($start),end.points=c($stop))\n";
#	print FILE_OUT "p <- ggplot(plot.data)\n";
#	print FILE_OUT "p + geom_rect(aes(xmin=start.points, xmax=end.points, ymin=0, ymax=1), fill=\"$colors[0]\") + scale_x_continuous(expand = c(0, 0)) + theme_bw() + ylab(paste(sprintf(\"\%60s\", \"\"), \"\n${key}\n\")) +\n";
#	print FILE_OUT " theme(axis.title.y = element_text(size = 30, colour = \"black\", angle = 0), axis.text = element_blank(), legend.key = element_blank(), axis.ticks = element_blank(), panel.grid = element_blank(), panel.border = element_blank(), plot.margin = unit(c(0.1, 0.1, 0.1, 0.1), \"lines\"))\n";
#	print FILE_OUT "dev.off()\n";
}


# Writing low coverage track
$start = "1, ";
$stop = "1, ";
open(LOWGFF,$ARGV[3]) || die("Can't open the low coverage gff file\n");
while(<LOWGFF>)
{
	$_ =~ s/\n//;
	@low_split = split(/\t/,$_);
	$start .= "$low_split[3], ";
	$stop .= "$low_split[4], ";
}
close(LOWGFF);
$start .= "$genome_length, ";
$stop .= "$genome_length, ";
$start =~ s/, $//;
$stop =~ s/, $//;
#print FILE_OUT "setwd(\"$location\")\n";
#print FILE_OUT "library(ggplot2)\n";
#print FILE_OUT "pdf(file=\"low_coverage_GFF_feature.pdf\", width=5600, height=160)\n";
#print FILE_OUT "plot.data <- data.frame(start.points=c($start),end.points=c($stop))\n";
#print FILE_OUT "p <- ggplot(plot.data)\n";
#print FILE_OUT "p + geom_rect(aes(xmin=start.points, xmax=end.points, ymin=0, ymax=1), fill=\"orange\") + scale_x_continuous(expand = c(0, 0)) + theme_bw() + ggtitle(\"Depth of coverage figure for assembled genome: $genome_ID\") + ylab(paste(sprintf(\"\%60s\", \"\"), \"\nlow_coverage\n\")) +\n";
#print FILE_OUT " theme(plot.title = element_text(size = 40, colour = \"black\"), axis.title.y = element_text(size = 30, colour = \"black\", angle = 0), axis.text = element_blank(), legend.key = element_blank(), axis.ticks = element_blank(), panel.grid = element_blank(), panel.border = element_blank(), plot.margin = unit(c(0.1, 0.1, 0.1, 0.1), \"lines\"))\n";
#print FILE_OUT "dev.off()\n";

# Writing no coverage track
$start = "1, ";
$stop = "1, ";
open(NOGFF,$ARGV[4]) || die("Can't open the no coverage gff file\n");
while(<NOGFF>)
{
	$_ =~ s/\n//;
        @no_split = split(/\t/,$_);
        $start .= "$no_split[3], ";
        $stop .= "$no_split[4], ";
}
close(NOGFF);
$start .= "$genome_length, ";
$stop .= "$genome_length, ";
$start =~ s/, $//;
$stop =~ s/, $//;
#print FILE_OUT "setwd(\"$location\")\n";
#print FILE_OUT "library(ggplot2)\n";
#print FILE_OUT "pdf(file=\"no_coverage_GFF_feature.pdf\", width=5600, height=70)\n";
#print FILE_OUT "plot.data <- data.frame(start.points=c($start),end.points=c($stop))\n";
#print FILE_OUT "p <- ggplot(plot.data)\n";
#print FILE_OUT "p + geom_rect(aes(xmin=start.points, xmax=end.points, ymin=0, ymax=1), fill=\"red\") + scale_x_continuous(expand = c(0, 0)) + theme_bw() + ylab(paste(sprintf(\"\%60s\", \"\"), \"\nno_coverage\n\")) +\n";
#print FILE_OUT " theme(axis.title.y = element_text(size = 30, colour = \"black\", angle = 0), axis.text = element_blank(), legend.key = element_blank(), axis.ticks = element_blank(), panel.grid = element_blank(), panel.border = element_blank(), plot.margin = unit(c(0.1, 0.1, 0.1, 0.1), \"lines\"))\n";
#print FILE_OUT "dev.off()\n";

print STDERR "Building the depth of coverage track...\n";
print FILE_OUT "setEPS()\n";
print FILE_OUT "postscript(file=\"simple_coverage_graphic.eps\", width=20, height=6)\n";
print FILE_OUT "depth_plot <- c($depth_total)\n";
print FILE_OUT "min_cutoff <- c($min_cutoff_total)\n";
print FILE_OUT "med_cutoff <- c($med_cutoff_total)\n";
print FILE_OUT "high_cutoff <- c($high_cutoff_total)\n";
#print FILE_OUT "par(mar=c(4, 34, 2, 0))\n";
#print FILE_OUT "par(oma=c(1,1,1,1))\n";
#print FILE_OUT "plot(depth_plot, type=\"l\", col=\"blue\", log=\"y\", ylim = c(1, 100000), xaxs=\"i\", yaxs=\"i\", main=\"\", sub=\"\", xlab=\"Genome Position\", ylab=\"log(depth of coverage)\", cex.lab=1.2, cex.axis=0.8, cex.main=0.8, cex.sub=0.3)\n";
print FILE_OUT "plot(depth_plot, type=\"l\", col=\"blue\", log=\"y\", xaxs=\"i\", yaxs=\"i\", main=\"\", sub=\"\", xlab=\"Genome Position\", ylab=\"log(depth of coverage)\", cex.lab=1.2, cex.axis=0.8, cex.main=0.8, cex.sub=0.3)\n";
print FILE_OUT "lines(min_cutoff, col=\"red\")\n";
print FILE_OUT "lines(med_cutoff, col=\"yellow\")\n";
print FILE_OUT "lines(high_cutoff, col=\"green\")\n";
print FILE_OUT "dev.off()\n";

close(FILE_OUT);
system("R --vanilla < GFF_visualization_from_$ARGV[0].r > output_GFF_visualization$ARGV[0]");
system("rm GFF_visualization_from_$ARGV[0].r");
system("rm output_GFF_visualization$ARGV[0]");
system("rm cut_pileup.list");

### Define the list of features that were created and their order for 'montage'
#     low_coverage_GFF_feature.pdf no_coverage_GFF_feature.pdf simple_coverage_graphic.pdf genome_segment_GFF_feature.pdf repeat_region_GFF_feature.pdf gene_GFF_feature.pdf CDS_GFF_feature.pdf miRNA_GFF_feature.pdf
while(($key,$value) = each %FEATURES)
{
	$all_pdfs .= "${key}_GFF_feature.pdf ";
}
$list_of_pdfs = "low_coverage_GFF_feature.pdf no_coverage_GFF_feature.pdf simple_coverage_graphic.pdf ";
if($all_pdfs =~ /(^| )genome_segment_GFF_feature\.pdf /)
{
	$list_of_pdfs .= "genome_segment_GFF_feature.pdf ";
	$all_pdfs =~ s/genome_segment_GFF_feature\.pdf //;
}
if($all_pdfs =~ /(^| )repeat_region_GFF_feature\.pdf /)
{
        $list_of_pdfs .= "repeat_region_GFF_feature.pdf ";
        $all_pdfs =~ s/repeat_region_GFF_feature\.pdf //;
}
if($all_pdfs =~ /(^| )gene_GFF_feature\.pdf /)
{
        $list_of_pdfs .= "gene_GFF_feature.pdf ";
        $all_pdfs =~ s/gene_GFF_feature\.pdf //;
}
if($all_pdfs =~ /(^| )exon_GFF_feature\.pdf /)
{
        $list_of_pdfs .= "exon_GFF_feature.pdf ";
        $all_pdfs =~ s/exon_GFF_feature\.pdf //;
}
if($all_pdfs =~ /(^| )miRNA_GFF_feature\.pdf /)
{
        $list_of_pdfs .= "miRNA_GFF_feature.pdf ";
        $all_pdfs =~ s/miRNA_GFF_feature\.pdf //;
}
$list_of_pdfs .= $all_pdfs;
$list_of_pdfs =~ s/ $//;

#system("montage $list_of_pdfs -mode Concatenate -tile 1x coverage_graphic.pdf");

### Removal of intermediate pdfs
#while(($key,$value) = each %FEATURES)
#{
#	system("rm ${key}_GFF_feature.pdf");
#}
#system("rm low_coverage_GFF_feature.pdf");
#system("rm no_coverage_GFF_feature.pdf");
#system("rm simple_coverage_graphic.pdf");

#Version 1
#plot.data <- data.frame(start.points=c(5, 32),end.points=c(15, 51))
# Plot using ggplot
# library(ggplot2)
# p <- ggplot(plot.data)
# p + geom_rect(aes(xmin=start.points, xmax=end.points, ymin=0, ymax=3), fill="yellow") + theme_bw()

# Version 2
# plot.data <- data.frame(start.points=c(5, 32),end.points=c(15, 51), text.label=c("Sample A", "Sample B"))
# plot.data$text.position <- (plot.data$start.points + plot.data$end.points)/2
# library(ggplot2)
# p <- ggplot(plot.data)
# p + geom_rect(aes(xmin=start.points, xmax=end.points, ymin=0, ymax=3),fill="yellow") + theme_bw() + geom_text(aes(x=text.position, y=1.5, label=text.label)) + labs(x=NULL, y=NULL)
