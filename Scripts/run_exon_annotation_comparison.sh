cat>>STEP_4.sub<<eof
cd $1
mkdir gene_temp_files
cd gene_temp_files

ref=\`ls $3 | grep -v ".fastq" | grep ".fa"\`
ref_gff=\`ls $3 | grep -Pi ".g(f|t)f"\`
cp $4 genome.fa
cp $3/\$ref reference.fa
cp $5 genome.gff
cp $3/\$ref_gff reference.gff

### Extract and align features named exons, report translated AA sequence
cd $1/gene_temp_files
mkdir AA
cp genome.fa AA
cp reference.fa AA
cp genome.gff AA
cp reference.gff AA
cd AA
perl $2/extract_genes_and_transcripts_from_fasta.pl genome.fa genome.gff 2 > genome.faa
perl $2/extract_genes_and_transcripts_from_fasta.pl reference.fa reference.gff 2 > reference.faa
perl $2/translate_fasta_exon_to_protein.pl genome.faa > genome.prot
perl $2/translate_fasta_exon_to_protein.pl reference.faa > reference.prot
perl $2/combine_and_sort_multi_exon_files.pl genome.prot reference.prot
sh $2/create_alignments_from_fastas.sh
perl $2/scanning_exons_for_aberrations.pl feature_names.txt $6
mv feature_report.txt ../../

cd $1/gene_temp_files
mkdir ORF
cp genome.fa ORF
cp reference.fa ORF
cp genome.gff ORF
cp reference.gff ORF
cd ORF
perl $2/extract_genes_and_transcripts_from_fasta.pl genome.fa genome.gff 2 > genome.faa
perl $2/extract_genes_and_transcripts_from_fasta.pl reference.fa reference.gff 2 > reference.faa
perl $2/combine_and_sort_multi_exon_files.pl genome.faa reference.faa
sh $2/create_alignments_from_fastas.sh

cd $1/gene_temp_files
mkdir Gene
cp genome.fa Gene
cp reference.fa Gene
cp genome.gff Gene
cp reference.gff Gene
cd Gene
perl $2/extract_genes_and_transcripts_from_fasta.pl genome.fa genome.gff 1 > genome.faa
perl $2/extract_genes_and_transcripts_from_fasta.pl reference.fa reference.gff 1 > reference.faa
perl $2/combine_and_sort_multi_exon_files.pl genome.faa reference.faa
sh $2/create_alignments_from_fastas.sh

cd $1/gene_temp_files
mkdir Other
cp genome.fa Other
cp reference.fa Other
grep -Pv "\texon\t" genome.gff > Other/genome.gff
grep -Pv "\texon\t" reference.gff > Other/reference.gff
cd Other

while read -r line
do
        sed -ir "/ID=\$line;/d" genome.gff
        sed -ir "/ID=\$line$/d" genome.gff
        sed -ir "/ID=\$line;/d" reference.gff
        sed -ir "/ID=\$line$/d" reference.gff
done < "$1/gene_temp_files/AA/feature_names.txt"
rm genome.gffr
rm reference.gffr

perl -ne '\$_ =~ s/\n//; @splitter = split(/\t/,\$_); if((\$splitter[4] - \$splitter[3]) > 5000){ next; } print "\$_\n";' genome.gff > temp_genome.gff
perl -ne '\$_ =~ s/\n//; @splitter = split(/\t/,\$_); if((\$splitter[4] - \$splitter[3]) > 5000){ next; } print "\$_\n";' reference.gff > temp_reference.gff
mv temp_genome.gff genome.gff
mv temp_reference.gff reference.gff

perl $2/extract_genes_and_transcripts_from_fasta.pl genome.fa genome.gff 3 > genome.faa
perl $2/extract_genes_and_transcripts_from_fasta.pl reference.fa reference.gff 3 > reference.faa
perl $2/combine_and_sort_multi_exon_files.pl genome.faa reference.faa
sh $2/create_alignments_from_fastas.sh

eof
