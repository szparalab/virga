#!/usr/bin/perl -w

open(GENOME,$ARGV[0])||die("Can't open the genome file\n");
while(<GENOME>)
{
	if($_ =~ /^>/)
	{
		$name = $_;
		$name =~ s/>//;
		$name =~ s/\n//;
		next;
	}
	$_ =~ s/\n//;
	$genome .= $_;
}
close(GENOME);

$count = 0;
while($genome =~ /N/)
{
	$Ns = ($genome =~ m/(N+)/)[0];
	$start = ($-[0])+1;
	$end = ($+[0]);
	#print "Ns: $Ns, Start: $-[0], End: $+[0]\n";
	$count++;
	$size = ($end-$start)+1;
	print "$name\t-\tgap\t$start\t$end\t.\t+\t.\tID=Ns-${count}_${size};\n";
	substr($genome,$-[0],length($Ns),"X"x(length($Ns)));
}

#print "$genome\n";
