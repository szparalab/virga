#!/usr/bin/env bash

Red="$(tput setaf 1)"
Green="$(tput setaf 2)"
GreenBG="$(tput setab 2)"
Brown="$(tput setaf 3)"
Blue="$(tput setaf 4)"
NC="$(tput sgr0)" # No Color


# Loading the parameters chosen by the user
. `pwd`/VirGA_parameters.ini
if [ $use_modules == "yes" ]; then
	module load virga
fi

# Checking for FastX Toolkit
if [ $use_modules == "yes" ]; then
	module load $module_fastx 2> /dev/null
fi
fastx_check=`fastx_artifacts_filter -h 2> /dev/null | grep -c "usage: fastx_artifacts_filter"`
if [ $fastx_check -eq 1 ]; then
	echo -ne "FastX Toolkit\t\t=  ${Green}Pass${NC}\n"
fi
if [ $fastx_check -ne 1 ]; then
	echo -ne "FastX Toolkit\t\t=  ${Red}Fail${NC}\n"
fi

# Checking for FastQC
if [ $use_modules == "yes" ]; then
        module load $module_fastqc 2> /dev/null
fi
fastqc_check=`fastqc -h 2> /dev/null | grep -c "FastQC - A high throughput sequence QC analysis tool"`
if [ $fastqc_check -eq 1 ]; then
        echo -ne "FastQC\t\t\t=  ${Green}Pass${NC}\n"
fi
if [ $fastqc_check -ne 1 ]; then
        echo -ne "FastQC\t\t\t=  ${Red}Fail${NC}\n"
fi

# Checking for R
if [ $use_modules == "yes" ]; then
        module load $module_R 2> /dev/null
fi
R_check=`R -h 2> /dev/null | grep -c "R, a system for statistical computation and graphics"`
if [ $R_check -eq 1 ]; then
        echo -ne "R\t\t\t=  ${Green}Pass${NC}\n"
fi
if [ $R_check -ne 1 ]; then
        echo -ne "R\t\t\t=  ${Red}Fail${NC}\n"
fi

# Checking for Bowtie2
if [ $use_modules == "yes" ]; then
        module load $module_bowtie2 2> /dev/null
fi
bowtie2_check=`bowtie2 -h 2> /dev/null | grep -c "bowtie2 [options]*"`
if [ $bowtie2_check -eq 1 ]; then
        echo -ne "Bowtie2\t\t\t=  ${Green}Pass${NC}\n"
fi
if [ $bowtie2_check -ne 1 ]; then
        echo -ne "Bowtie2\t\t\t=  ${Red}Fail${NC}\n"
fi

# Checking for SAMTools
if [ $use_modules == "yes" ]; then
        module load $module_samtools 2> /dev/null
fi
samtools_check=`samtools 2>&1 | tee | grep -c "Tools for alignments in the SAM format"`
if [ $samtools_check -eq 1 ]; then
        echo -ne "SAMTools\t\t=  ${Green}Pass${NC}\n"
fi
if [ $samtools_check -ne 1 ]; then
        echo -ne "SAMTools\t\t=  ${Red}Fail${NC}\n"
fi

# Checking for VirGA tools Maf-Net.py and Compare_genomes.py
if [ $use_modules == "yes" ]; then
        module load $module_virga 2> /dev/null
fi
mafnet_check=`maf_net.py -h 2> /dev/null | grep -ci "Usage: maf_net.py"`
if [ $mafnet_check -eq 1 ]; then
        echo -ne "Maf-net.py\t\t=  ${Green}Pass${NC}\n"
fi
if [ $mafnet_check -ne 1 ]; then
        echo -ne "Maf-net.py\t\t=  ${Red}Fail${NC}\n"
fi
compare_genomes_check=`compare_genomes.py -h 2> /dev/null | grep -ci "Usage: compare_genomes.py"`
if [ $compare_genomes_check -eq 1 ]; then
        echo -ne "Compare_genomes.py\t=  ${Green}Pass${NC}\n"
fi
if [ $compare_genomes_check -ne 1 ]; then
        echo -ne "Compare_genomes.py\t=  ${Red}Fail${NC}\n"
fi

# Checking for Mugsy
if [ $use_modules == "yes" ]; then
        module load $module_mugsy 2> /dev/null
fi
mugsy_check=`mugsy 2>&1 | tee | grep -c "mugsy - a multiple whole genome aligner"`
rm .mugsy.log >/dev/null 2>&1
if [ $mugsy_check -eq 1 ]; then
        echo -ne "Mugsy\t\t\t=  ${Green}Pass${NC}\n"
fi
if [ $mugsy_check -ne 1 ]; then
        echo -ne "Mugsy\t\t\t=  ${Red}Fail${NC}\n"
fi

# Checking for Freebayes
if [ $use_modules == "yes" ]; then
        module load $module_freebayes 2> /dev/null
fi
freebayes_check=`freebayes -h 2> /dev/null | grep -c "Bayesian haplotype-based polymorphism discovery."`
if [ $freebayes_check -eq 1 ]; then
        echo -ne "Freebayes\t\t=  ${Green}Pass${NC}\n"
fi
if [ $freebayes_check -ne 1 ]; then
        echo -ne "Freebayes\t\t=  ${Red}Fail${NC}\n"
fi

# Checking for Celera
if [ $use_modules == "yes" ]; then
        module load $module_celera 2> /dev/null
fi
celera_check=`runCA 2> /dev/null | grep -c "Complete documentation at http://wgs-assembler.sourceforge.net/"`
if [ $celera_check -eq 1 ]; then
        echo -ne "Celera\t\t\t=  ${Green}Pass${NC}\n"
fi
if [ $celera_check -ne 1 ]; then
        echo -ne "Celera\t\t\t=  ${Red}Fail${NC}\n"
fi

# Checking for Gapfiller
if [ $use_modules == "yes" ]; then
        module load $module_gapfiller 2> /dev/null
fi
gapfiller_check=`GapFiller.pl 2> /dev/null | grep -c "Library file containing two paired-read files with insert size, error and orientation indication."`
if [ $gapfiller_check -eq 1 ]; then
        echo -ne "Gapfiller\t\t=  ${Green}Pass${NC}\n"
fi
if [ $gapfiller_check -ne 1 ]; then
        echo -ne "Gapfiller\t\t=  ${Red}Fail${NC}\n"
fi

# Checking for ClustalW2
if [ $use_modules == "yes" ]; then
        module load $module_clustalw2 2> /dev/null
fi
clustalw2_check=`clustalw2 -h 2> /dev/null | grep -c "Multiple Sequence Alignments"`
if [ $clustalw2_check -eq 1 ]; then
        echo -ne "ClustalW2\t\t=  ${Green}Pass${NC}\n"
fi
if [ $clustalw2_check -ne 1 ]; then
        echo -ne "ClustalW2\t\t=  ${Red}Fail${NC}\n"
fi

# Checking for SSAKE
if [ $use_modules == "yes" ]; then
        module load $module_ssake 2> /dev/null
fi
ssake_check=`SSAKE -h 2> /dev/null | grep -c "Minimum number of overlapping bases with the seed/contig during overhang consensus"`
if [ $ssake_check -eq 1 ]; then
        echo -ne "SSAKE\t\t\t=  ${Green}Pass${NC}\n"
fi
if [ $ssake_check -ne 1 ]; then
        echo -ne "SSAKE\t\t\t=  ${Red}Fail${NC}\n"
fi
