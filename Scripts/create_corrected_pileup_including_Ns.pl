#!/usr/bin/perl -w

open(FASTA,$ARGV[0]) || die("Can't open the reference fasta file\n");
while(<FASTA>)
{
	$_ =~ s/\n//;
	if($_ =~ /^>/)
	{
		$_ =~ s/^>//;
		$name = $_;
		next;
	}
	if($_ !~ /^>/)
	{
		$genome .= $_;
		next;
	}
}
close(FASTA);

$total_length = length($genome)+1;

$pos = 0;
open(PILEUP,$ARGV[1]) || die("Can't open the pileup file\n");
while(<PILEUP>)
{
	$pos++;
	$sample_pos = ($_ =~ m/^.*?\t(.*?)\t/)[0];
	while($sample_pos > $pos)
	{
		print "$name\t$pos\tN\t0\t\t\n";
		$pos++;
		next;
	}
	if($sample_pos == $pos)
	{
		print "$_";
		next;
	}
	if($sample_pos < $pos)
	{
		print STDERR "ERROR\n";
		exit 1;
	}
}
close(PILEUP);

$pos++;
while($pos < $total_length)
{
	print "$name\t$pos\tN\t0\t\t\n";
	$pos++;
}
