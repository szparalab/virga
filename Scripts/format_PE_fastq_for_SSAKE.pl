#!/usr/bin/perl -w

$insert_size = $ARGV[2];
open(R1,$ARGV[0]) || die("Can't open the R1 read file\n");
open(R2,$ARGV[1]) || die("Can't open the R2 read file\n");
$count = 0;
$read_name = 0;
while (!eof(R1) and !eof(R2))
{
	my $line1 = <R1>;
	my $line2 = <R2>;
	$count++;
	if($count == 1)
	{
		$read_name++;
		$name1 = ($line1 =~ m/^(\S+)/)[0];
		$name2 = ($line2 =~ m/^(\S+)/)[0];
		if($name1 ne $name2)
		{
			print "The names of the reads (R1: $name1, R2: $name2) do not match. Quitting! \n";
			exit 1;
		}
		print ">$read_name:$insert_size\n";
		next;
	}
	if($count == 2)
	{
		$line1 =~ s/\n//;
		$line2 =~ s/\n//;
		print "$line1:$line2\n";
		next;
	}
	if($count == 3)
	{
		next;
	}
	if($count == 4)
	{
		$count = 0;
		next;
	}
}
