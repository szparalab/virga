#!/usr/bin/perl -w

for($i=0; $i<=$#ARGV; $i++)
{
	open(GENES,$ARGV[$i]) || die("Can't open the gene file number $i\n");
	while(<GENES>)
	{
		if($_ =~ /^>/)
		{
			$ID = ($_ =~ m/>(.*?) /)[0];
			$header = $_;
			$source = ($_ =~ m/Sequence for (.*?),/)[0];
		}
		if($_ !~ /^>/)
		{
			$header =~ s/$ID/${ID}_${source}/;
			$HASH{$ID} .= ${header};
			$HASH{$ID} .= $_;
		}
	}
	close(GENES);
}

open(NAMES,">>feature_names.temp") || die("Can't open the output gene_names.txt for writing");
while(($key,$value) = each %HASH)
{
	print NAMES "$key\n";
	open(OUT,">>${key}.fasta") || die("Can't open output for writing\n");
	print OUT "$value";
	close(OUT);
}

system("sort -V feature_names.temp > feature_names.txt");
system("rm feature_names.temp");
