cat>>UTILITY-reassembly.sub<<eof
cd $1
perl $2/excise_target_genomic_area.pl $3/$4 $3/$5 $3/$6 $3/$7 $8 $9
GapFiller.pl -l libraries.txt -s excised_genome.fa -d 4000 -T ${10} -t 0 -i 20 -r 0.1
cp standard_output/standard_output.gapfilled.final.fa filled_genome_0-1.fa
GapFiller.pl -l libraries.txt -s excised_genome.fa -d 4000 -T ${10} -t 0 -i 20 -r 0.3
cp standard_output/standard_output.gapfilled.final.fa filled_genome_0-3.fa
GapFiller.pl -l libraries.txt -s excised_genome.fa -d 4000 -T ${10} -t 0 -i 20 -r 0.5
cp standard_output/standard_output.gapfilled.final.fa filled_genome_0-5.fa
GapFiller.pl -l libraries.txt -s excised_genome.fa -d 4000 -T ${10} -t 0 -i 20 -r 0.7
cp standard_output/standard_output.gapfilled.final.fa filled_genome_0-7.fa
GapFiller.pl -l libraries.txt -s excised_genome.fa -d 4000 -T ${10} -t 0 -i 20 -r 0.9
cp standard_output/standard_output.gapfilled.final.fa filled_genome_0-9.fa

eof
