#!/bin/usr/perl -w

$count = 0;
$read_name= 0;
open(SINGLETON,$ARGV[0]) || die("Can't open the singletons file\n");
while(<SINGLETON>)
{
	$count++;
	if($count == 1)
	{
		$read_name++;
		next;
	}
	if($count == 2)
	{
		print ">Singleton-$read_name\n$_";
	}
	if($count == 4)
	{
		$count = 0;
	}
}
close(SINGLETON);
