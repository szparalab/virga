#!/usr/bin/perl -w

system("mkdir circos_ref_files");
open(INPUT, $ARGV[0]) || die("Can't open the genome file\n");
while(<INPUT>)
{
	if($_ =~ /^>/)
	{
		$_ =~ s/\n//;
		$genome_name = ($_ =~ m/^>(.*)/)[0];
		next;
	}
	$_ =~ s/\n//;
	$total .= $_;
}
close(INPUT);
$genome_length = length($total);
print "Genome name: $genome_name,\tGenome lenth: $genome_length\n";

open(OUT, ">data.txt") || die("Can't create a new data.txt file\n");
print OUT "chr - chr1 1 0 $genome_length $genome_name\n";
close(OUT);
system("mv data.txt circos_ref_files");

open(OUT, ">genes.txt") || die("Can't open a new genes.txt files\n");
open(INPUT, $ARGV[1]) || die("Can't open the gff file1\n");
while(<INPUT>)
{
	@splitter = split(/\t/,$_);
	if($splitter[2] eq "gene")
	{
		$_ =~ s/\n//;
		$gene_name = ($splitter[8] =~ m/ID=(.*)/)[0];
		print OUT "chr1 $splitter[3] $splitter[4] $gene_name\n";
	}
}
close(INPUT);
close(OUT);
system("mv genes.txt circos_ref_files");

open(OUT, ">exons.txt") || die("Can't open a new genes.txt files\n");
open(INPUT, $ARGV[1]) || die("Can't open the gff file2\n");
while(<INPUT>)
{
        @splitter = split(/\t/,$_);
        if($splitter[2] eq "exon")
        {
                $_ =~ s/\n//;
                $gene_name = ($splitter[8] =~ m/ID=(.*);/)[0];
                print OUT "chr1 $splitter[3] $splitter[4] $gene_name\n";
        }
}
close(INPUT);
close(OUT);
system("mv exons.txt circos_ref_files");

open(OUT, ">others.txt") || die("Can't open a new genes.txt files\n");
open(INPUT, $ARGV[1]) || die("Can't open the gff file3\n");
while(<INPUT>)
{
        @splitter = split(/\t/,$_);
        if($splitter[2] ne "gene" && $splitter[2] ne "exon")
        {
                $_ =~ s/\n//;
                $gene_name = ($splitter[8] =~ m/ID=(.*)/)[0];
		if($gene_name eq "UL" or $gene_name eq "US"){ next; }
                print OUT "chr1 $splitter[3] $splitter[4] $gene_name\n";
        }
}
close(INPUT);
close(OUT);
system("mv others.txt circos_ref_files");

open(OUT, ">low_coverage.txt") || die("Can't open a new genes.txt files\n");
open(INPUT, $ARGV[2]) || die("Can't open the gff file4\n");
while(<INPUT>)
{
        @splitter = split(/\t/,$_);
        $_ =~ s/\n//;
        $gene_name = ($splitter[8] =~ m/ID=(.*)/)[0];
        print OUT "chr1 $splitter[3] $splitter[4] $gene_name\n";
}
close(INPUT);
close(OUT);
system("mv low_coverage.txt circos_ref_files");

open(OUT, ">no_coverage.txt") || die("Can't open a new genes.txt files\n");
open(INPUT, $ARGV[3]) || die("Can't open the gff file5\n");
while(<INPUT>)
{
        @splitter = split(/\t/,$_);
        $_ =~ s/\n//;
        $gene_name = ($splitter[8] =~ m/ID=(.*)/)[0];
        print OUT "chr1 $splitter[3] $splitter[4] $gene_name\n";
}
close(INPUT);
close(OUT);
system("mv no_coverage.txt circos_ref_files");

open(OUT, ">SNPs.txt") || die("Can't open a new genes.txt files\n");
open(INPUT, $ARGV[4]) || die("Can't open the gff file6\n");
while(<INPUT>)
{
	if($_ =~ /======/){ last; next; }
	if($_ =~ /^$/){ last; next; }
        @splitter = split(/\t/,$_);
        $_ =~ s/\n//;
        $gene_name = "SNP";
	$additional = $splitter[1] + 1;
        print OUT "chr1 $splitter[1] $additional $gene_name\n";
}
close(INPUT);
close(OUT);

open(OUT, ">>SNPs.txt") || die("Can't open a new genes.txt files\n");
open(INPUT, $ARGV[5]) || die("Can't open the gff file7\n");
while(<INPUT>)
{
        if($_ =~ /^#/){ next; }
        @splitter = split(/\t/,$_);
        $_ =~ s/\n//;
        $gene_name = "SNP";
        $additional = $splitter[1] + 1;
        print OUT "chr1 $splitter[1] $additional $gene_name\n";
}
close(INPUT);
close(OUT);
system("mv SNPs.txt circos_ref_files");

$read_span_high = 0;
open(INPUT1, $ARGV[6]) || die("Can't open the read_span file\n");
while(<INPUT1>)
{
        $_ =~ s/\n//;
        $number = ($_ =~ m/ (.*)/)[0];
        if($number > $read_span_high)
        {
                $read_span_high = $number;
        }
}
close(INPUT1);

open(INPUT2, $ARGV[7]) || die("Can't open the read_span file\n");
while(<INPUT2>)
{
        $_ =~ s/\n//;
        $number = ($_ =~ m/ (.*)/)[0];
        if($number > $read_span_high)
        {
                $read_span_high = $number;
        }
}
close(INPUT2);

open(OUT1, ">formatted_good_read_spans.txt") || die("Can't write new read_span file\n");
open(INPUT1, $ARGV[6]) || die("Can't open the read_span file\n");
while(<INPUT1>)
{
	$_ =~ s/\n//;
        $number = ($_ =~ m/ (.*)/)[0];
	$start_site = ($_ =~ m/^(.*?) /)[0];
	$divide = $number / $read_span_high;
	print OUT1 "chr1\t$start_site\t$start_site\t$divide\n";
}
close(INPUT1);
close(OUT1);
system("mv formatted_good_read_spans.txt circos_ref_files");

open(OUT2, ">formatted_bad_read_spans.txt") || die("Can't write new read_span file\n");
open(INPUT2, $ARGV[7]) || die("Can't open the read_span file\n");
while(<INPUT2>)
{
	$_ =~ s/\n//;
        $number = ($_ =~ m/ (.*)/)[0];
	$start_site = ($_ =~ m/^(.*?) /)[0];
        $divide = $number / $read_span_high;
        print OUT2 "chr1\t$start_site\t$start_site\t$divide\n";
}
close(INPUT2);
close(OUT2);
system("mv formatted_bad_read_spans.txt circos_ref_files");

#open(OUT, ">>SVDetect.txt") || die("Can't open a new genes.txt files\n");
#open(INPUT, $ARGV[6]) || die("Can't open the gff file\n");
#while(<INPUT>)
#{
#        if($_ !~ /^INTRA/){ next; }
#        @splitter = split(/\t/,$_);
#        $_ =~ s/\n//;
#        $gene_name = $splitter[1];
#        $start = $splitter[4];
#	$start =~ s/-/ /;
#	$end = $splitter[7];
#	$end =~ s/-/ /;
#        print OUT "chr1 $start chr1 $end name=$gene_name\n";
#}
#close(INPUT);
#close(OUT);

#open(OUT, ">>Lumpy.txt") || die("Can't open a new genes.txt files\n");
#open(INPUT, $ARGV[7]) || die("Can't open the gff file\n");
#while(<INPUT>)
#{
#        @splitter = split(/\t/,$_);
#        $_ =~ s/\n//;
#        print OUT "chr1 $splitter[1] $splitter[2] chr1 $splitter[4] $splitter[5] name=$splitter[10]\n";
#}
#close(INPUT);
#close(OUT);
