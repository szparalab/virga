#!/usr/bin/perl -w


$min_depth_allowed = 10;
if(exists $ARGV[1])
{
	$min_depth_allowed = $ARGV[1];
}

$catch_name = 0;
open(PILEUP,$ARGV[0])||die("Can't open the pileup file\n");
while(<PILEUP>)
{
	if($catch_name == 0)
	{
		$genome_name = ($_ =~ m/^(.*?)\t/)[0];
		$catch_name = 1;
	}
	$pos = ($_ =~ m/^.*?\t(.*?)\t/)[0];
	$num = ($_ =~ m/^.*?\t.*?\t.*?\t(.*?)\t/)[0];
	if($num < $min_depth_allowed)
	{
		$HASH{$pos} = $num;
	}
}
close(PILEUP);

foreach $key (sort { $a <=> $b } keys(%HASH))
{
	push(@array,$key);
}

$count = -1;
$within = 0;
$naming = 0;
foreach $LOCUS (@array)
{
	$count++;
	if($count == $#array)
	{
		if($within == 1)
		{
			print "$LOCUS\t.\t+\t.\tID=Depth_less_than_$min_depth_allowed,$naming\n";
			next;
		}
		if($within == 0)
		{
			$naming++;
			print "$genome_name\t-\tlow_coverage\t$LOCUS\t$LOCUS\t.\t+\t.\tID=Depth_less_than_$min_depth_allowed,$naming\n";
			next;
		}
	}
	if(($array[$count+1] == ($LOCUS+1)) and ($within == 0))
	{
		$naming++;
		print "$genome_name\t-\tlow_coverage\t$LOCUS\t";
		$within = 1;
		next;
	}
	if(($array[$count+1] != ($LOCUS+1)) and ($within == 1))
	{
		print "$LOCUS\t.\t+\t.\tID=Depth_less_than_$min_depth_allowed,$naming\n";
		$within = 0;
		next;
	}
	if(($array[$count+1] != ($LOCUS+1)) and ($within == 0))
        {
		$naming++;
                print "$genome_name\t-\tlow_coverage\t$LOCUS\t$LOCUS\t.\t+\t.\tID=Depth_less_than_$min_depth_allowed,$naming\n";
                $within = 0;
                next;
        }
}
