#!/usr/bin/perl -w

$size = 100;
if(exists $ARGV[1])
{
	$size = $ARGV[1];
}

$ID = "Contig";
if(exists $ARGV[2])
{
	$ID = $ARGV[2];
}

open(FASTA,$ARGV[0])||die("Can't open the input fasta file\n");
while(<FASTA>)
{
	$_ =~ s/\n//;
	if($_ =~ /^>/)
	{
		$header = $_;
	}
	if($_ !~ /^>/)
	{
		$HASH{$header} .= $_;
	}
}
close(FASTA);

while(($key,$value) = each %HASH)
{
	$SORT{$value} = length($value);
}

$num=0;
foreach $key (sort { $SORT {$b} <=> $SORT {$a} } keys %SORT )
{
	$num++;
	$value = $key;
	print ">${ID}${num}xLengthx$SORT{$key}\n";
	while($value ne "" and $size != 0)
	{
		if(length($value) >= $size)
		{
			print substr($value,0,$size)."\n";
			substr($value,0,$size) = "";
		}
		else
		{
			print substr($value,0,length($value))."\n";
			substr($value,0,length($value)) = "";
		}
	}
	if($size == 0)
	{
		print "$key\n";
	}
}
