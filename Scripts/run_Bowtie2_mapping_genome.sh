cat>>STEP_4.sub<<eof
cd $1
num_reads=\`ls $3 | grep -c ".fastq"\`
if [ \$num_reads -eq 1 ]; then
	if [ $4 != "SE" ]; then
		echo -ne "ERROR: Only one read found in $3, but your specified read type of $4 isn't SE\n"
		exit 1
	fi
	SE_read=\`ls $3 | grep ".fastq"\`
	bowtie2 -p $5 -x $2 $3/\$SE_read -S ${2}.sam
fi
if [ \$num_reads -gt 1 ]; then
	if [ $4 != "PE" ]; then
		echo -ne "ERROR: Two read files found in $3, but your specified read type of $4 isn't PE\n"
		exit 1
	fi
	find_R1=\`ls $3 | grep -c "1.fastq"\`
	find_R2=\`ls $3 | grep -c "2.fastq"\`
	if [ \$find_R1 -ne 1 ] || [ \$find_R2 -ne 1 ]; then
		echo -ne "ERROR: Cannot find PE-reads using the naming convention of sample_1.fastq sample_2.fastq in $3\n"
		exit 1
	fi
	R1_name=\`ls $3 | grep "1.fastq"\`
	R2_name=\`ls $3 | grep "2.fastq"\`
	bowtie2 -p $5 -x $2 -1 $3/\$R1_name -2 $3/\$R2_name -S ${2}.sam
fi
samtools view -bS -h ${2}.sam -o ${2}.bam
samtools sort ${2}.bam ${2}_sorted
samtools index ${2}_sorted.bam
samtools faidx ${2}.fa
eof
