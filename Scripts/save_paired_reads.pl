#!/usr/bin/perl -w

$pos = 0;
$read1_count = 0;
open(READ1,$ARGV[0]) || die("Can't open the R1 read");
while(<READ1>)
{
	$pos++;
	if($pos == 1)
	{
		$read1_count++;
		$read_ID = ($_ =~ m/^(.*?)(\s|$)/)[0];
		if(not exists $HASH{$read_ID})
		{
			$HASH{$read_ID} = 1;
			next;
		}
		else
		{
			$HASH{$read_ID} += 1;
		}
	}
	if($pos == 4)
	{
		$pos = 0;
	}
}
close(READ1);

$pos2 = 0;
$read2_count = 0;
open(READ2,$ARGV[1]) || die("Can't open the R2 read");
while(<READ2>)
{
        $pos2++;
        if($pos2 == 1)
        {
		$read2_count++;
                $read_ID2 = ($_ =~ m/^(.*?)(\s|$)/)[0];
                if(not exists $HASH{$read_ID2})
                {
                        $HASH{$read_ID2} = 1;
                        next;
                }
                else
                {
                        $HASH{$read_ID2} += 1;
                }
        }
        if($pos2 == 4)
        {
                $pos2 = 0;
        }
}
close(READ2);

$singles = 0;
$doubles = 0;
$others = 0;
$hold = 0;
while(($key,$value) = each %HASH)
{
	$hold = $key;
	if($value == 1){ $singles++; }
	if($value == 2){ $doubles++; }
	if($value!= 1 and $value != 2){ $others++; }
}
if($others != 0)
{
	print STDERR "ERROR: Script detecting reads with non-1 and non-2 occurences\n";
	exit 1;
}
$total_hash_reads = $singles + ($doubles*2);
if($total_hash_reads != ($read1_count + $read2_count))
{
	print STDERR "ERROR: Hash counts don't match the input read counts\n";
	exit 1;
}

print STDERR "R1 read count: $read1_count\n";
print STDERR "R2 read count: $read2_count\n";
print STDERR "Total read count: ".($read1_count + $read2_count)."\n";
print STDERR "Singletons: $singles\n";
print STDERR "Properly paired reads: $doubles\n";

$header1 = "";
$header2 = "";
$name1 = $ARGV[0];
$name2 = $ARGV[1];
if($ARGV[0] =~ /\//)
{
	$ARGV[0] =~ m/(.*\/)(.*)/;
	$header1 = $1;
	$name1 = $2;
}
if($ARGV[1] =~ /\//)
{
	$ARGV[1] =~ m/(.*\/)(.*)/;
	$header2 = $1;
	$name2 = $2;
}

open(OUT1,">>${header1}paired_${name1}") || die("Can't open the read1 output file for writing\n");
open(OUT2,">>${header2}paired_${name2}") || die("Can't open the read2 output file for writing\n");
open(SINGLETONS,">>${ARGV[2]}Singletons.fastq") || die("Can't open the singleton output file for writing\n");
open(READ1,$ARGV[0]) || die("Can't re-open the read1 file for filtering\n");
$last_read1_pos = 0;
while(<READ1>)
{
	$last_read1_pos++;
	$read1_save .= $_;
	if($last_read1_pos == 1)
	{
		$last_read1_ID = ($_ =~ m/^(.*?)(\s|$)/)[0];
	}
	if($last_read1_pos == 4)
	{
		if($HASH{$last_read1_ID} == 2)
		{
			print OUT1 $read1_save;
		}
		if($HASH{$last_read1_ID} == 1)
		{
			print SINGLETONS $read1_save;
		}
		$read1_save = "";
		$last_read1_pos = 0;
	}
}
close(READ1);
close(OUT1);
open(READ2,$ARGV[1]) || die("Can't re-open the read2 file for filtering\n");
$last_read2_pos = 0;
while(<READ2>)
{
        $last_read2_pos++;
        $read2_save .= $_;
        if($last_read2_pos == 1)
        {
                $last_read2_ID = ($_ =~ m/^(.*?)(\s|$)/)[0];
        }
        if($last_read2_pos == 4)
        {
                if($HASH{$last_read2_ID} == 2)
                {
                        print OUT2 $read2_save;
                }
		if($HASH{$last_read2_ID} == 1)
		{
			print SINGLETONS $read2_save;
		}
		$read2_save = "";
		$last_read2_pos = 0;
        }
}
close(READ2);
close(OUT2);
