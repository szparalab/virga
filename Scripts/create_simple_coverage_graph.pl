#!/usr/bin/perl -w

open(PILEUP,$ARGV[0]) || die("Can't open the pileup file\n");
while(<PILEUP>)
{
	$_ =~ s/\n//;
	@splitter = split(/\t/,$_);
	push(@depth,$splitter[3]);
}
close(PILEUP);

$x_location = 0;
foreach $LINE (@depth)
{
	$x_location++;
	$x_total .= "$x_location,";
	$depth .= "$LINE,";
	if($LINE <= $ARGV[1])
	{
		$total_value .= "10,";
	}
	if($LINE > $ARGV[1])
	{
		$total_value .= "0,";
	}
}
$x_total =~ s/,$//;
$total_value =~ s/,$//;
$depth =~ s/,$//;
$location =  `pwd`;
$location =~ s/\n//;

open(FILE_OUT,">>coverage_plot.r") || die("Can't open the outfile for writing");
print FILE_OUT "setwd(\"$location\")\n";
print FILE_OUT "library(ggplot2)\n";
print FILE_OUT "bulk = data.frame(x_pos=c($x_total),low_depth=c($total_value),depth=c($depth))\n";
print FILE_OUT "png(filename=\"simple_coverage_graphic.png\", width=4000, height=800)\n";
print FILE_OUT "ggplot(bulk, aes(x=x_pos), aes(alpha=0.2)) +\n";
print FILE_OUT "  geom_bar(aes(y=low_depth), color=\"red\", stat=\"identity\") +\n";
print FILE_OUT "  geom_line(aes(y=depth), color=\"black\", size=1) +\n";
print FILE_OUT "  scale_y_log10()\n";

#ggplot(bulk, aes(x=x_pos)) + 
#  geom_bar(aes(y=y_bar), stat="identity") +
#  geom_line(aes(y=y_line), color="red", size=2)


print FILE_OUT "dev.off()\n";
close(FILE_OUT);
system("R --vanilla < coverage_plot.r > output_from_R");
system("rm coverage_plot.r");
system("rm output_from_R");

