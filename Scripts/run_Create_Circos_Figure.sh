cat>>STEP_4.sub<<eof
cd $1/Bowtie2
perl $2/circos_conversion.pl $3 $4 $5 $6 $7 $8 $9 ${10}
mv $1/Bowtie2/circos_ref_files $1
mv $1/Bowtie2/histogram_per-base_negative_scaled.txt $1/circos_ref_files
mv $1/Bowtie2/histogram_per-base_positive_scaled.txt $1/circos_ref_files
mv $1/Bowtie2/histogram_per-base_negative.txt $1/circos_ref_files
mv $1/Bowtie2/histogram_per-base_positive.txt $1/circos_ref_files
mv $1/Bowtie2/read_span_bad.txt $1/circos_ref_files
mv $1/Bowtie2/read_span_good.txt $1/circos_ref_files
mv $2/bands.conf $1/circos_ref_files
mv $2/circos_good-qual.conf $1/circos_ref_files
mv $2/circos_badd-qual.conf $1/circos_ref_files
mv $2/ideogram.conf $1/circos_ref_files
mv $2/ideogram.label.conf $1/circos_ref_files
mv $2/ideogram.position.conf $1/circos_ref_files
mv $2/pregap4.config $1/circos_ref_files
mv $2/ticks.conf $1/circos_ref_files
cd $1/circos_ref_files
circos -conf circos_good-qual.conf
mv circos.png circos_good-qual.png
mv circos.svg circos_good-qual.svg
circos -conf circos_bad-qual.conf
mv circos.png circos_bad-qual.png
mv circos.svg circos_bad-qual.svg
eof
