mkdir all_mergedcontigs
cp */*mergedcontigs all_mergedcontigs
cat > ssake_kmers.txt << EOF
Total # of contigs	Average contig length	Longest contig	Shortest contig	N20	N50	N80	Number of contigs >= N50	Total # of bases
EOF
for i in all_mergedcontigs/*; do echo -ne "$i\t" >> ssake_kmers.txt; perl ../../x_scripts/check_ssake_quality_metrics.pl $i >> ssake_kmers.txt; done
sed -i 's/all_mergedcontigs\/ssake.fasta.ssake_//' ssake_kmers.txt
sed -i 's/_w10_e0.75_k4_a0.5_z100_x20_g-Singletons.fasta//' ssake_kmers.txt
sed -i 's/_o2_r0.7_//' ssake_kmers.txt
sed -i 's/_.*mergedcontigs//' ssake_kmers.txt
perl ../../x_scripts/find_ssake_kmers.pl ssake_kmers.txt 8
