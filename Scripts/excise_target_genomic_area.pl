#!/usr/bin/perl -w

############################################
#
# usage: perl excise_target_genomic_area.pl /
#    genome.sam /
#    read_1.fastq /
#    read_2.fastq /
#    genome.fa /
#    excise_start /
#    excise_stop /
#    
############################################


$counter = -1;
$stops = -1;
@inserts = "";
open(INPUT, $ARGV[0]) || die("Can't open the sam file\n");
while(<INPUT>)
{
	if($_ =~ /^\@/){ next; }
	@splitter = split(/\t/,$_);
	$flag = $splitter[1];
	if((field_2($flag) ? "YES" : "NO") eq "NO"){ next; }
	$match = $splitter[8];
	if($match < 0){ $match = $match*-1; }
	$counter++;
	$inserts[$counter] = $match;
        $stops++;
        if($stops == 100000)
        {
                print STDOUT "Processing line: $counter\n";
                $stops = 0;
        }
}
close(INPUT);

$total = 0;
$entries = 0;
foreach $ENTRY (@inserts)
{
	$entries++;
	$total += $ENTRY;
}
$mean = $total / $entries;

$variance = 0;
$squared = 0;
foreach $ENTRY (@inserts)
{
	$squared = $mean - $ENTRY;
#	print "\n$squared\n";
	$squared = $squared * $squared;
	$variance += $squared;
#	print "\n$squared\n";
}

$stddev = $variance / ($entries-1);
$stddev = sqrt($stddev);
print STDOUT "Total:\t$total\nEntries:\t$entries\nMean:\t$mean\nStd Dev:\t$stddev\n";
$int_mean = int($mean);
$int_dev = int($stddev);

open(OUT,">libraries.txt") || die("Can't create the libraries file\n");
print OUT "Reads bwa $ARGV[1] $ARGV[2] $int_mean $int_dev FR\n";
close(OUT);

open(GENOME, $ARGV[3]) || die("Can't open the genome file\n");
while(<GENOME>)
{
	$_ =~ s/\n//;
	if($_ =~ /^>/)
	{
		$genomic_label = $_;
		next;
	}
	$genomic_seq .= $_;
}
close(GENOME);

$excise_start = $ARGV[4];
$excise_start--;
$excise_stop = $ARGV[5];
$excise_length = $excise_stop - $excise_start;
$replacement_Ns = "N"x$excise_length;
print "Excision start:\t$excise_start\n";
print "Excision stop:\t$excise_stop\n";
print "Excusion length:\t$excise_length\n";

substr($genomic_seq, $excise_start, $excise_length, $replacement_Ns);
$genomic_seq =~ s/^N+//;
$genomic_seq =~ s/N+$//;

open(OUT, ">excised_genome.fa") || die("Can't open a new file for writing\n");
print OUT "$genomic_label\n";
print OUT "$genomic_seq\n";
close(OUT);

######################################################################

sub dec2bin {
        my $str = unpack("B32", pack("N", shift));
        $str =~ s/^0+(?=\d)//;   # otherwise you'll get leading zeros
	return $str;
}

sub bin2dec{
	my $query = oct("0b".shift);
        return $query;
}

sub field_1 { $_[0] & 0x0001 } # the read is paired in sequencing
sub field_2 { $_[0] & 0x0002 } # the read is mapped in a proper pair
sub field_3 { $_[0] & 0x0004 } # the query sequence itself is unmapped
sub field_4 { $_[0] & 0x0008 } # the mate is unmapped
sub field_5 { $_[0] & 0x0010 } # strand of the query (0 = forward, 1 = reverse)
sub field_6 { $_[0] & 0x0020 } # strand of the mate (0 - forward, 1 = reverse)
sub field_7 { $_[0] & 0x0040 } # the read is the first read in a pair
sub field_8 { $_[0] & 0x0080 } # the read is the second read in a pair
sub field_9 { $_[0] & 0x0100 } # the alginment is not primary
sub field_10 { $_[0] & 0x0200 } # the read fails platform/vender QC checks
sub field_11 { $_[0] & 0x0400 } # the read is either PCR or optical duplicate
