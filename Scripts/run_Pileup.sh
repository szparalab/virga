cat>>STEP_4.sub<<eof
cd $1
samtools mpileup -d 150000 -f $1/${2}.fa ${2}_sorted.bam > ${2}.pileup
perl $3/create_corrected_pileup_including_Ns.pl $1/${2}.fa ${2}.pileup > corrected.pileup
eof
