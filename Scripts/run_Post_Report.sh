cat>>post_report.sub<<eof
cd $1
if [ $2 == "yes" ]; then
	mv $3/Bowtie2/$4.fa $3
	mv $3/Bowtie2/$4.fa.fai $3
	mv $3/Bowtie2/$4.gff $3
	mv $3/Bowtie2/full-length_$4.fa $3
	mv $3/Bowtie2/full-length_$4.gff $3
	mv $3/Bowtie2/low_coverage.gff $3
	mv $3/Bowtie2/no_coverage.gff $3
	mv $3/Bowtie2/*.png $3
	mv $3/Bowtie2/*.vcf $3
	mv $3/Bowtie2/*.txt $3
	mv $3/temp.vcf $3/Bowtie2
	mv $3/$4.vcf $3/Samtools_SNPs.vcf
	mv $3/freebayes_$4.vcf $3/Freebayes_SNPs.vcf
fi

cd $1
perl $1/x_scripts/generate_report.pl $1/VirGA_parameters.ini $1 $4
eof
