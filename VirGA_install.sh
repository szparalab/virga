#!/usr/bin/env bash

Red="$(tput setaf 1)"
Green="$(tput setaf 2)"
GreenBG="$(tput setab 2)"
Brown="$(tput setaf 3)"
Blue="$(tput setaf 4)"
NC="$(tput sgr0)" # No Color
#PS1='\[$Blue\]\h \W\[$Green\]:\[$Red\][#\#] \[$Brown\] '

current=`pwd`

echo -ne "\n${Brown}  ---     VirGA installation script     ---  \n"
echo -ne "=============================================\n\n"
echo -ne "Installation will be done in the following directory:\n$current\n\n"
echo -ne "Python module will be installed in:\n$HOME/local\n\n"
gcc_detect=`gcc --help | grep -ic "usage"`
if [ $gcc_detect == 0 ]; then
	echo -ne "${Red}gcc compiler not detected, installer exiting.\n"
	exit 1
fi
echo -ne "The pre-requisite gcc compiler has been detected.\n\n"
echo -ne "Creating download directory\t\t......."
mkdir downloads
echo -ne "\t\t${Green}Success!${Brown}\n"
cd downloads

#g++ 4.9.2
#echo -ne "Downloading g++ 4.9.2\t\t\t......."
#wget --quiet wget ftp://gnu.mirror.iweb.com/gcc/gcc-4.9.2/gcc-4.9.2.tar.bz2
#check_gcc=`ls | grep -c "gcc-4.9.2.tar.bz2"`
#if [ $check_gcc != 1 ]; then
#	echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
#fi
#echo -ne "\t\t${Green}Success!${Brown}\n"

#cmake 3.2.0
echo -ne "Downloading cmake 3.2.0\t\t\t......."
wget --quiet wget http://www.cmake.org/files/v3.2/cmake-3.2.0-rc2.tar.gz
check_cmake=`ls | grep -c "cmake-3.2.0-rc2.tar.gz"`
if [ $check_cmake != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#Python 2.7.9
echo -ne "Downloading Python 2.7.9\t\t......."
wget --quiet https://www.python.org/ftp/python/2.7.9/Python-2.7.9.tgz
check_python=`ls | grep -c "Python-2.7.9.tgz"`
if [ $check_python != 1 ]; then
	echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
	exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#R 3.1.1
echo -ne "Downloading R 3.1.1\t\t\t......."
wget --quiet http://cran.cnr.berkeley.edu/src/base/R-3/R-3.1.1.tar.gz
check_R=`ls | grep -c "R-3.1.1.tar.gz"`
if [ $check_R != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#numpy 1.9.2
echo -ne "Downloading numpy 1.9.2\t\t\t......."
wget --quiet http://sourceforge.net/projects/numpy/files/NumPy/1.9.2/numpy-1.9.2.zip/download
check_numpy=`ls | grep -c "download"`
if [ $check_numpy != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
mv download numpy-1.9.2.zip
echo -ne "\t\t${Green}Success!${Brown}\n"

#Biopython 1.65
echo -ne "Downloading Biopython 1.65\t\t......."
wget --quiet http://biopython.org/DIST/biopython-1.65.tar.gz
check_biopython=`ls | grep -c "biopython-1.65.tar.gz"`
if [ $check_biopython != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#Cython 0.21.2
echo -ne "Downloading Cython 0.21.2\t\t......."
wget --quiet http://cython.org/release/Cython-0.21.2.tar.gz
check_cython=`ls | grep -c "Cython-0.21.2.tar.gz"`
if [ $check_cython != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#paired_sequence_utils 0.1
echo -ne "Downloading paired_sequence_utils 0.1\t......."
wget --quiet https://bitbucket.org/lance_parsons/paired_sequence_utils/downloads/paired_sequence_utils-0.1.tar.gz
check_psu=`ls | grep -c "paired_sequence_utils-0.1.tar.gz"`
if [ $check_psu != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#bx-python 0.7.1
echo -ne "Downloading bx-python 0.7.1\t\t......."
wget --quiet https://pypi.python.org/packages/source/b/bx-python/bx-python-0.7.1.tar.gz
check_bxpython=`ls | grep -c "bx-python-0.7.1.tar.gz"`
if [ $check_bxpython != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#pybedtools 0.6
echo -ne "Downloading pybedtools 0.6\t\t......."
wget --quiet https://pypi.python.org/packages/source/p/pybedtools/pybedtools-0.6.tar.gz
check_pybedtools=`ls | grep -c "pybedtools-0.6.tar.gz"`
if [ $check_pybedtools != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#bedtools 2.21.0
echo -ne "Downloading bedtools 2.21.0\t\t......."
wget --quiet https://github.com/arq5x/bedtools2/releases/download/v2.21.0/bedtools-2.21.0.tar.gz
check_bedtools=`ls | grep -c "bedtools-2.21.0.tar.gz"`
if [ $check_bedtools != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#mugsy 1r2.3
echo -ne "Downloading mugsy 1r2.3\t\t\t......."
wget --quiet http://sourceforge.net/projects/mugsy/files/mugsy_x86-64-v1r2.3.tgz/download
check_mugsy=`ls | grep -c "download"`
if [ $check_mugsy != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
mv download mugsy_x86-64-v1r2.3.tgz
echo -ne "\t\t${Green}Success!${Brown}\n"

#FastX-Toolkit 2.6
echo -ne "Downloading FastX-Toolkit 2.6\t\t......."
wget --quiet http://hannonlab.cshl.edu/fastx_toolkit/fastx_toolkit_0.0.13_binaries_Linux_2.6_amd64.tar.bz2
check_fastx=`ls | grep -c "fastx_toolkit_0.0.13_binaries_Linux_2.6_amd64.tar.bz"`
if [ $check_fastx != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#FastQC 0.11.2
echo -ne "Downloading FastQC 0.11.2\t\t......."
wget --quiet http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.2.zip
check_fastqc=`ls | grep -c "fastqc_v0.11.2.zip"`
if [ $check_fastqc != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#Bowtie2 2.2.4
echo -ne "Downloading Bowtie2 2.2.4\t\t......."
wget --quiet http://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.2.4/bowtie2-2.2.4-linux-x86_64.zip
check_bowtie2=`ls | grep -c "bowtie2-2.2.4-linux-x86_64.zip"`
if [ $check_bowtie2 != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#SSAKE 3-8-2
echo -ne "Downloading SSAKE 3-8-2\t\t\t......."
wget --quiet http://www.bcgsc.ca/platform/bioinfo/software/ssake/releases/3.8.2/ssake_v3-8-2.tar.gz
check_ssake=`ls | grep -c "ssake_v3-8-2.tar.gz"`
if [ $check_ssake != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#Zlib 1.2.8
echo -ne "Downloading Zlib 1.2.8\t\t\t......."
wget --quiet http://zlib.net/zlib-1.2.8.tar.gz
check_zlib=`ls | grep -c "zlib-1.2.8.tar.gz"`
if [ $check_zlib != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#Samtools 0.1.19
echo -ne "Downloading Samtools 0.1.19\t\t......."
wget --quiet http://sourceforge.net/projects/samtools/files/samtools/0.1.19/samtools-0.1.19.tar.bz2/download
check_samtools=`ls | grep -c "download"`
if [ $check_samtools != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
mv download samtools-0.1.19.tar.bz2
echo -ne "\t\t${Green}Success!${Brown}\n"

#Freebayes
echo -ne "Downloading Freebayes\t\t\t.......\r"
git clone --recursive git://github.com/ekg/freebayes.git 2>&1 | while read line; do echo -ne "${Brown}\e[KDownloading Freebayes\t\t\t.......\t\t$line\r" | tr -d '\n';done
check_freebayes=`ls | grep -c "freebayes"`
if [ $check_freebayes != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "${Brown}\e[KDownloading Freebayes\t\t\t.......\t\t${Green}Success!${Brown}\n"

#Celera 8.1
echo -ne "Downloading Celera 8.1\t\t\t......."
wget --quiet http://sourceforge.net/projects/wgs-assembler/files/wgs-assembler/wgs-8.1/wgs-8.1-Linux-amd64.tar.bz2/download
check_celera=`ls | grep -c "download"`
if [ $check_celera != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
mv download wgs-8.1-Linux-amd64.tar.bz2
echo -ne "\t\t${Green}Success!${Brown}\n"

#ClustalW 2.1
echo -ne "Downloading ClustalW 2.1\t\t......."
wget --quiet http://www.clustal.org/download/current/clustalw-2.1.tar.gz
check_clustalw=`ls | grep -c "clustalw-2.1.tar.gz"`
if [ $check_clustalw != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

#VAMP 0.9.0
echo -ne "Downloading VAMP 0.9.0\t\t\t......."
wget --quiet https://bitbucket.org/szparalab/vamp/downloads/VAMP-0.9.0.tar.gz
check_vamp=`ls | grep -c "VAMP-0.9.0.tar.gz"`
if [ $check_vamp != 1 ]; then
        echo -ne "\t\t${Red}ERROR - download failed${NC}\n\n\n"
        exit 1
fi
echo -ne "\t\t${Green}Success!${Brown}\n"

echo -ne "\n\n${Brown}Extracting each package into its own dependency directory\n\n${NC}"
cd ../; mkdir dependencies; cd dependencies


#gcc-4.9.2.tar.bz2
#ex_file="gcc-4.9.2.tar.bz2"
#ex_name="gcc"
#echo -ne "${Brown}Extracting ${ex_name}\t\t\t\t.......\t\t"
#mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -jxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t\t.......\t\t$line\r";done
#cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t\t.......\t\t${Green}Success!${NC}\n"

#cmake-3.2.0-rc2.tar.gz
ex_file="cmake-3.2.0-rc2.tar.gz"
ex_name="cmake"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#Python-2.7.9.tgz
ex_file="Python-2.7.9.tgz"
ex_name="Python"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#R-3.1.1.tar.gz
ex_file="R-3.1.1.tar.gz"
ex_name="R-code"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#numpy-1.9.2.zip
ex_file="numpy-1.9.2.zip"
ex_name="numpy"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; unzip ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#biopython-1.65.tar.gz
ex_file="biopython-1.65.tar.gz"
ex_name="Biopython"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#Cython-0.21.2.tar.gz
ex_file="Cython-0.21.2.tar.gz"
ex_name="Cython"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#paired_sequence_utils-0.1.tar.gz
ex_file="paired_sequence_utils-0.1.tar.gz"
ex_name="paired_sequence_utils"
echo -ne "${Brown}Extracting ${ex_name}\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t.......\t\t${Green}Success!${NC}\n"

#bx-python-0.7.1.tar.gz
ex_file="bx-python-0.7.1.tar.gz"
ex_name="bx-python"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#pybedtools-0.6.tar.gz
ex_file="pybedtools-0.6.tar.gz"
ex_name="Pybedtools"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#bedtools-2.21.0.tar.gz
ex_file="bedtools-2.21.0.tar.gz"
ex_name="Bedtools"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#mugsy_x86-64-v1r2.3.tgz
ex_file="mugsy_x86-64-v1r2.3.tgz"
ex_name="Mugsy"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#fastx_toolkit_0.0.13_binaries_Linux_2.6_amd64.tar.bz2
ex_file="fastx_toolkit_0.0.13_binaries_Linux_2.6_amd64.tar.bz2"
ex_name="FastX-Toolkit"
echo -ne "${Brown}Extracting ${ex_name}\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -jxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t.......\t\t${Green}Success!${NC}\n"

#fastqc_v0.11.2.zip
ex_file="fastqc_v0.11.2.zip"
ex_name="FastQC"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; unzip ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#bowtie2-2.2.4-linux-x86_64.zip
ex_file="bowtie2-2.2.4-linux-x86_64.zip"
ex_name="Bowtie2"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; unzip ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#ssake_v3-8-2.tar.gz
ex_file="ssake_v3-8-2.tar.gz"
ex_name="SSAKE"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#zlib.net/zlib-1.2.8.tar.gz
ex_file="zlib-1.2.8.tar.gz"
ex_name="Zlib"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t\t.......\t\t${Green}Success!${NC}\n"

#samtools-0.1.19.tar.bz2
ex_file="samtools-0.1.19.tar.bz2"
ex_name="Samtools"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -jxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#Freebayes.zip
ex_file="freebayes"
ex_name="Freebayes"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t\r"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#wgs-8.1-Linux-amd64.tar.bz2
ex_file="wgs-8.1-Linux-amd64.tar.bz2"
ex_name="Celera"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -jxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#clustalw-2.1.tar.gz
ex_file="clustalw-2.1.tar.gz"
ex_name="ClustalW2"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#VAMP-0.9.0.tar.gz
ex_file="VAMP-0.9.0.tar.gz"
ex_name="VAMP"
echo -ne "${Brown}Extracting ${ex_name}\t\t\t\t.......\t\t"
mkdir ${ex_name}; cd ${ex_name}; mv ../../downloads/${ex_file} .; tar -zxvf ${ex_file} | while read line; do echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t\t.......\t\t$line\r";done
cd ../; echo -ne "${Brown}\e[KExtracting ${ex_name}\t\t\t\t.......\t\t${Green}Success!${NC}\n"

cd ../
empty_downloads=`ls downloads | wc -l`
if [ $empty_downloads != 0 ]; then
        echo -ne "${Red}\nERROR -- Installation failed, download directory not empty\n"
fi
rmdir downloads

#mkdir -p local/lib/python2.7/site-packages
#echo "export PYTHONPATH=\$PYTHONPATH:$current/local/lib/python2.7/site-packages" >> PATH_additions.txt
#export PYTHONPATH=$PYTHONPATH:$current/local/lib/python2.7/site-packages

echo -ne "${Brown}\nBeginning the installation process of each dependency${NC}\n"
cd $current


#gcc-4.9.2.tar.bz2
#install_file="gcc-4.9.2"
#install_name="gcc"
#wait=""
#for count in {6..1}; do
#        wait="${wait}."
#        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t\t[$wait]\r"
#        sleep 0.3
#done
#echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t\t.......${Blue}\n"
#cd $current/dependencies/${install_name}/${install_file}; ./configure; make
#cd $current
#echo "export PATH=\$PATH:$current/dependencies/Zlib/zlib-1.2.8" >> PATH_additions.txt
#export PATH=$PATH:$current/dependencies/Zlib/zlib-1.2.8
#echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t\t.......\t\t${Green}Success!${NC}\n"

#cmake-3.2.0-rc2.tar.gz
install_file="cmake-3.2.0-rc2"
install_name="cmake"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; ./configure; gmake
cd $current
echo "export PATH=\$PATH:$current/dependencies/cmake/cmake-3.2.0-rc2/bin" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/cmake/cmake-3.2.0-rc2/bin
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#Python-2.7.9.tgz
install_file="Python-2.7.9"
install_name="Python"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; ./configure; make
cd $current
echo "export PATH=\$PATH:$current/dependencies/Python/Python-2.7.9" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/Python/Python-2.7.9
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#R-3.1.1.tar.gz
install_file="R-3.1.1"
install_name="R-code"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; ./configure; make
cd $current
echo "export PATH=\$PATH:$current/dependencies/R-code/R-3.1.1/bin" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/R-code/R-3.1.1/bin
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#numpy-1.9.2.zip
install_file="numpy-1.9.2"
install_name="numpy"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; python setup.py install --user
cd $current
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#biopython-1.65.tar.gz
install_file="biopython-1.65"
install_name="Biopython"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; python setup.py install --user
cd $current
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#Cython-0.21.2.tar.gz
install_file="Cython-0.21.2"
install_name="Cython"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; python setup.py install --user
cd $current
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#paired_sequence_utils-0.1.tar.gz
install_file="paired_sequence_utils-0.1"
install_name="paired_sequence_utils"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; python setup.py install --user
cd $current
echo -ne "${Brown}\e[KInstalling ${install_name}\t.......\t\t${Green}Success!${NC}\n"

#bx-python-0.7.1.tar.gz
install_file="bx-python-0.7.1"
install_name="bx-python"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; python setup.py install --user
cd $current
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#pybedtools-0.6.tar.gz
install_file="pybedtools-0.6"
install_name="Pybedtools"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; python setup.py install --user
cd $current
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#bedtools-2.21.0.tar.gz
install_file="bedtools2"
install_name="Bedtools"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; make
cd $current
echo "export PATH=\$PATH:$current/dependencies/bedtools2/bin" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/bedtools2/bin
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#mugsy_x86-64-v1r2.3.tgz
install_file="mugsy_x86-64-v1r2.3"
install_name="Mugsy"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo "export PATH=\$PATH:$current/dependencies/Mugsy/mugsy_x86-64-v1r2.3" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/Mugsy/mugsy_x86-64-v1r2.3
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#fastx_toolkit_0.0.13_binaries_Linux_2.6_amd64.tar.bz2
install_file="bin"
install_name="FastX-Toolkit"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t[$wait]\r"
        sleep 0.3
done
echo "export PATH=\$PATH:$current/dependencies/FastX-Toolkit/bin" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/FastX-Toolkit/bin
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t.......\t\t${Green}Success!${NC}\n"

#fastqc_v0.11.2.zip
install_file="FastQC"
install_name="FastQC"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
cd $current/dependencies/${install_name}/${install_file}; chmod 775 fastqc
cd $current
echo "export PATH=\$PATH:$current/dependencies/FastQC/FastQC" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/FastQC/FastQC
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#bowtie2-2.2.4-linux-x86_64.zip
install_file="bowtie2-2.2.4-linux-x86_64"
install_name="Bowtie2"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo "export PATH=\$PATH:$current/dependencies/Bowtie2/bowtie2-2.2.4" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/Bowtie2/bowtie2-2.2.4
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#ssake_v3-8-2.tar.gz
install_file="ssake_v3-8-2"
install_name="SSAKE"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo "export PATH=\$PATH:$current/dependencies/SSAKE/ssake_v3-8-2" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/SSAKE/ssake_v3-8-2
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#zlib.net/zlib-1.2.8.tar.gz
install_file="zlib-1.2.8"
install_name="Zlib"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; ./configure; make
cd $current
echo "export PATH=\$PATH:$current/dependencies/Zlib/zlib-1.2.8" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/Zlib/zlib-1.2.8
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t\t.......\t\t${Green}Success!${NC}\n"

#samtools-0.1.19.tar.bz2
install_file="samtools-0.1.19"
install_name="Samtools"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; ./configure; make
cd $current
echo "export PATH=\$PATH:$current/dependencies/Samtools/samtools-0.1.19/bcftools" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/Samtools/samtools-0.1.19/bcftools
echo "export PATH=\$PATH:$current/dependencies/Samtools/samtools-0.1.19" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/Samtools/samtools-0.1.19
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#Freebayes.zip
install_file="freebayes"
install_name="Freebayes"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; make
cd $current
echo "export PATH=\$PATH:$current/dependencies/Freebayes/freebayes/bin" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/Freebayes/freebayes/bin
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#wgs-8.1-Linux-amd64.tar.bz2
install_file="wgs-8.1-Linux-amd64"
install_name="Celera"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo "export PATH=\$PATH:$current/dependencies/Celera/wgs-8.1/Linux-amd64/bin" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/Celera/wgs-8.1/Linux-amd64/bin
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#clustalw-2.1.tar.gz
install_file="clustalw-2.1"
install_name="ClustalW2"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; ./configure; make
cd $current
echo "export PATH=\$PATH:$current/dependencies/ClustalW2/clustalw-2.1/src" >> PATH_additions.txt
export PATH=$PATH:$current/dependencies/ClustalW2/clustalw-2.1/src
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t.......\t\t${Green}Success!${NC}\n"

#VAMP-0.9.0.tar.gz
install_file="VAMP-0.9.0"
install_name="VAMP"
wait=""
for count in {6..1}; do
        wait="${wait}."
        echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t\t[$wait]\r"
        sleep 0.3
done
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t\t.......${Blue}\n"
cd $current/dependencies/${install_name}/${install_file}; python setup.py install --user
cd $current
echo "export PATH=\$PATH:$current/dependencies/VAMP/VAMP-0.9.0/bin" >> PATH_additions.txt
export PATH=\$PATH:$current/dependencies/VAMP/VAMP-0.9.0/bin
echo -ne "${Brown}\e[KInstalling ${install_name}\t\t\t\t.......\t\t${Green}Success!${NC}\n"

